#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xce24a5d, __VMLINUX_SYMBOL_STR(ethtool_op_get_link) },
	{ 0x1358f7b9, __VMLINUX_SYMBOL_STR(eth_validate_addr) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xf7f25d3f, __VMLINUX_SYMBOL_STR(netif_device_attach) },
	{ 0x91f45392, __VMLINUX_SYMBOL_STR(eth_commit_mac_addr_change) },
	{ 0xce1951e9, __VMLINUX_SYMBOL_STR(eth_prepare_mac_addr_change) },
	{ 0x6e720ff2, __VMLINUX_SYMBOL_STR(rtnl_unlock) },
	{ 0xc7a4fbed, __VMLINUX_SYMBOL_STR(rtnl_lock) },
	{ 0x5e62b588, __VMLINUX_SYMBOL_STR(netdev_notify_peers) },
	{ 0x78699eb7, __VMLINUX_SYMBOL_STR(virtqueue_add_sgs) },
	{ 0xfff2f85, __VMLINUX_SYMBOL_STR(register_virtio_driver) },
	{ 0xf2249a05, __VMLINUX_SYMBOL_STR(netif_carrier_on) },
	{ 0x53a4040b, __VMLINUX_SYMBOL_STR(netif_carrier_off) },
	{ 0x8b43159b, __VMLINUX_SYMBOL_STR(register_cpu_notifier) },
	{ 0x6877b859, __VMLINUX_SYMBOL_STR(register_netdev) },
	{ 0xedcd9166, __VMLINUX_SYMBOL_STR(netif_set_real_num_rx_queues) },
	{ 0x73ae698d, __VMLINUX_SYMBOL_STR(netif_set_real_num_tx_queues) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x949f7342, __VMLINUX_SYMBOL_STR(__alloc_percpu) },
	{ 0x79aa04a2, __VMLINUX_SYMBOL_STR(get_random_bytes) },
	{ 0xf71a9fee, __VMLINUX_SYMBOL_STR(alloc_etherdev_mqs) },
	{ 0x64e16b64, __VMLINUX_SYMBOL_STR(virtqueue_enable_cb_delayed) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x2a18c74, __VMLINUX_SYMBOL_STR(nf_conntrack_destroy) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x87e352bb, __VMLINUX_SYMBOL_STR(virtqueue_add_outbuf) },
	{ 0x3b134b3e, __VMLINUX_SYMBOL_STR(dev_kfree_skb_any) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x7e31674f, __VMLINUX_SYMBOL_STR(virtqueue_get_vring_size) },
	{ 0x236a3f8c, __VMLINUX_SYMBOL_STR(free_netdev) },
	{ 0xc9ec4e21, __VMLINUX_SYMBOL_STR(free_percpu) },
	{ 0x1c60d5c9, __VMLINUX_SYMBOL_STR(unregister_netdev) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x8487a2b6, __VMLINUX_SYMBOL_STR(flush_work) },
	{ 0x4ad14efa, __VMLINUX_SYMBOL_STR(netif_napi_del) },
	{ 0xbe82da40, __VMLINUX_SYMBOL_STR(netif_device_detach) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x1fe9f800, __VMLINUX_SYMBOL_STR(unregister_cpu_notifier) },
	{ 0xbb41e0b7, __VMLINUX_SYMBOL_STR(__free_pages) },
	{ 0x13074b57, __VMLINUX_SYMBOL_STR(virtqueue_detach_unused_buf) },
	{ 0x5528d259, __VMLINUX_SYMBOL_STR(cancel_delayed_work_sync) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xa3acd520, __VMLINUX_SYMBOL_STR(virtqueue_poll) },
	{ 0x903300e5, __VMLINUX_SYMBOL_STR(napi_complete) },
	{ 0x121337f7, __VMLINUX_SYMBOL_STR(virtqueue_enable_cb_prepare) },
	{ 0x53f01b6b, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0xbe6c060b, __VMLINUX_SYMBOL_STR(netif_receive_skb) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x5d6e6cc0, __VMLINUX_SYMBOL_STR(eth_type_trans) },
	{ 0x6e85d414, __VMLINUX_SYMBOL_STR(skb_partial_csum_set) },
	{ 0xffb126f4, __VMLINUX_SYMBOL_STR(virtqueue_get_buf) },
	{ 0xf6ebc03b, __VMLINUX_SYMBOL_STR(net_ratelimit) },
	{ 0xf00f8562, __VMLINUX_SYMBOL_STR(skb_trim) },
	{ 0x8b3ee142, __VMLINUX_SYMBOL_STR(virtqueue_kick) },
	{ 0x33d53d62, __VMLINUX_SYMBOL_STR(consume_skb) },
	{ 0xbb435892, __VMLINUX_SYMBOL_STR(skb_to_sgvec) },
	{ 0x45f4292a, __VMLINUX_SYMBOL_STR(skb_put) },
	{ 0x2ef407d0, __VMLINUX_SYMBOL_STR(__netdev_alloc_skb) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0x171c1a3b, __VMLINUX_SYMBOL_STR(virtqueue_add_inbuf) },
	{ 0xb6244511, __VMLINUX_SYMBOL_STR(sg_init_one) },
	{ 0xfb994d5a, __VMLINUX_SYMBOL_STR(alloc_pages_current) },
	{ 0x799aca4, __VMLINUX_SYMBOL_STR(local_bh_enable) },
	{ 0x3ff62317, __VMLINUX_SYMBOL_STR(local_bh_disable) },
	{ 0x76a495c1, __VMLINUX_SYMBOL_STR(cpu_possible_mask) },
	{ 0xd7d79132, __VMLINUX_SYMBOL_STR(put_online_cpus) },
	{ 0x3efb35c9, __VMLINUX_SYMBOL_STR(get_online_cpus) },
	{ 0x865b9977, __VMLINUX_SYMBOL_STR(virtio_check_driver_offered_feature) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xc897c382, __VMLINUX_SYMBOL_STR(sg_init_table) },
	{ 0x832c198b, __VMLINUX_SYMBOL_STR(netif_napi_add) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x4cbbd171, __VMLINUX_SYMBOL_STR(__bitmap_weight) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xc0a3d105, __VMLINUX_SYMBOL_STR(find_next_bit) },
	{ 0xfe7c4287, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0x32047ad5, __VMLINUX_SYMBOL_STR(__per_cpu_offset) },
	{ 0x950ffff2, __VMLINUX_SYMBOL_STR(cpu_online_mask) },
	{ 0x5d7a5723, __VMLINUX_SYMBOL_STR(__netif_schedule) },
	{ 0x1902adf, __VMLINUX_SYMBOL_STR(netpoll_trap) },
	{ 0x3290ba2f, __VMLINUX_SYMBOL_STR(__napi_schedule) },
	{ 0x698f3c25, __VMLINUX_SYMBOL_STR(virtqueue_disable_cb) },
	{ 0x7628f3c7, __VMLINUX_SYMBOL_STR(this_cpu_off) },
	{ 0x817a930e, __VMLINUX_SYMBOL_STR(unregister_virtio_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=virtio_ring,virtio";

MODULE_ALIAS("virtio:d00000001v*");

MODULE_INFO(srcversion, "BEDAFFDC0E36D2B6DDCF5A0");

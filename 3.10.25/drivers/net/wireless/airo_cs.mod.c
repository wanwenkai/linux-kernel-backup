#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xc988e6a, __VMLINUX_SYMBOL_STR(pcmcia_register_driver) },
	{ 0xa3baba49, __VMLINUX_SYMBOL_STR(pcmcia_request_io) },
	{ 0x6595b6e6, __VMLINUX_SYMBOL_STR(init_airo_card) },
	{ 0x681ef8d, __VMLINUX_SYMBOL_STR(pcmcia_enable_device) },
	{ 0x4c9581a4, __VMLINUX_SYMBOL_STR(pcmcia_loop_config) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xcbf64c64, __VMLINUX_SYMBOL_STR(stop_airo_card) },
	{ 0x78b40382, __VMLINUX_SYMBOL_STR(pcmcia_disable_device) },
	{ 0xbe82da40, __VMLINUX_SYMBOL_STR(netif_device_detach) },
	{ 0xf7f25d3f, __VMLINUX_SYMBOL_STR(netif_device_attach) },
	{ 0x8065d30c, __VMLINUX_SYMBOL_STR(reset_airo_card) },
	{ 0xf88db0f2, __VMLINUX_SYMBOL_STR(pcmcia_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=airo";

MODULE_ALIAS("pcmcia:m015Fc000Af*fn*pfn*pa*pb*pc*pd*");
MODULE_ALIAS("pcmcia:m015Fc0005f*fn*pfn*pa*pb*pc*pd*");
MODULE_ALIAS("pcmcia:m015Fc0007f*fn*pfn*pa*pb*pc*pd*");
MODULE_ALIAS("pcmcia:m0105c0007f*fn*pfn*pa*pb*pc*pd*");

MODULE_INFO(srcversion, "12273428B1AFAC06E0A6F34");

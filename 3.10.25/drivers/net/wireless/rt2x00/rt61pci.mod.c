#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x1b7f0549, __VMLINUX_SYMBOL_STR(rt2x00mac_tx_frames_pending) },
	{ 0xa70d4438, __VMLINUX_SYMBOL_STR(rt2x00mac_get_ringparam) },
	{ 0x9da0490c, __VMLINUX_SYMBOL_STR(rt2x00mac_get_antenna) },
	{ 0x7743033d, __VMLINUX_SYMBOL_STR(rt2x00mac_set_antenna) },
	{ 0xb23d4a80, __VMLINUX_SYMBOL_STR(rt2x00mac_flush) },
	{ 0x93ed78df, __VMLINUX_SYMBOL_STR(rt2x00mac_rfkill_poll) },
	{ 0xcfa9bda3, __VMLINUX_SYMBOL_STR(rt2x00mac_get_stats) },
	{ 0xc0e0d180, __VMLINUX_SYMBOL_STR(rt2x00mac_sw_scan_complete) },
	{ 0x19b89f70, __VMLINUX_SYMBOL_STR(rt2x00mac_sw_scan_start) },
	{ 0x1bb2697e, __VMLINUX_SYMBOL_STR(rt2x00mac_set_key) },
	{ 0x8c592a6f, __VMLINUX_SYMBOL_STR(rt2x00mac_configure_filter) },
	{ 0x79cf3922, __VMLINUX_SYMBOL_STR(rt2x00mac_bss_info_changed) },
	{ 0xca8743bd, __VMLINUX_SYMBOL_STR(rt2x00mac_config) },
	{ 0x9952514e, __VMLINUX_SYMBOL_STR(rt2x00mac_remove_interface) },
	{ 0x62a85d7b, __VMLINUX_SYMBOL_STR(rt2x00mac_add_interface) },
	{ 0xecb72f54, __VMLINUX_SYMBOL_STR(rt2x00mac_stop) },
	{ 0xbcc4d972, __VMLINUX_SYMBOL_STR(rt2x00mac_start) },
	{ 0x402e60f0, __VMLINUX_SYMBOL_STR(rt2x00mac_tx) },
	{ 0xf2ab5871, __VMLINUX_SYMBOL_STR(rt2x00mmio_flush_queue) },
	{ 0x724018e0, __VMLINUX_SYMBOL_STR(rt2x00mmio_uninitialize) },
	{ 0xcc67dbed, __VMLINUX_SYMBOL_STR(rt2x00mmio_initialize) },
	{ 0xed934897, __VMLINUX_SYMBOL_STR(rt2x00pci_resume) },
	{ 0x1fc2328c, __VMLINUX_SYMBOL_STR(rt2x00pci_suspend) },
	{ 0x5692d586, __VMLINUX_SYMBOL_STR(rt2x00pci_remove) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x47939e0d, __VMLINUX_SYMBOL_STR(__tasklet_hi_schedule) },
	{ 0xa5873874, __VMLINUX_SYMBOL_STR(rt2x00lib_txdone) },
	{ 0x4cfcd605, __VMLINUX_SYMBOL_STR(rt2x00queue_get_entry) },
	{ 0xbf79a950, __VMLINUX_SYMBOL_STR(rt2x00lib_txdone_noinfo) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x5806075c, __VMLINUX_SYMBOL_STR(rt2x00lib_beacondone) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0xfaef0ed, __VMLINUX_SYMBOL_STR(__tasklet_schedule) },
	{ 0x894883af, __VMLINUX_SYMBOL_STR(rt2x00mmio_rxdone) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x79aa04a2, __VMLINUX_SYMBOL_STR(get_random_bytes) },
	{ 0x27cb133b, __VMLINUX_SYMBOL_STR(eeprom_93cx6_multiread) },
	{ 0x952c5e83, __VMLINUX_SYMBOL_STR(pci_bus_read_config_word) },
	{ 0xd29b009f, __VMLINUX_SYMBOL_STR(crc_itu_t_table) },
	{ 0x6d356209, __VMLINUX_SYMBOL_STR(crc_itu_t) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x82072614, __VMLINUX_SYMBOL_STR(tasklet_kill) },
	{ 0x3b134b3e, __VMLINUX_SYMBOL_STR(dev_kfree_skb_any) },
	{ 0x65dcba97, __VMLINUX_SYMBOL_STR(skb_pad) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x2dba276a, __VMLINUX_SYMBOL_STR(__iowrite32_copy) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xd833d269, __VMLINUX_SYMBOL_STR(rt2x00mmio_regbusy_read) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x31a6c6f5, __VMLINUX_SYMBOL_STR(rt2x00mac_conf_tx) },
	{ 0xf7f0d5fd, __VMLINUX_SYMBOL_STR(rt2x00pci_probe) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=rt2x00lib,rt2x00mmio,rt2x00pci,eeprom_93cx6,crc-itu-t";

MODULE_ALIAS("pci:v00001814d00000301sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001814d00000302sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001814d00000401sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "6A351D6FE881FCD0886B4C9");

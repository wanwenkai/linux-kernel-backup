#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x109aeb3f, __VMLINUX_SYMBOL_STR(usbnet_disconnect) },
	{ 0xabfb708f, __VMLINUX_SYMBOL_STR(usbnet_probe) },
	{ 0xce24a5d, __VMLINUX_SYMBOL_STR(ethtool_op_get_link) },
	{ 0x35ed9f6e, __VMLINUX_SYMBOL_STR(usbnet_nway_reset) },
	{ 0xda66aefd, __VMLINUX_SYMBOL_STR(usbnet_set_msglevel) },
	{ 0x4646b5b3, __VMLINUX_SYMBOL_STR(usbnet_get_msglevel) },
	{ 0xd653a9f7, __VMLINUX_SYMBOL_STR(usbnet_tx_timeout) },
	{ 0x1358f7b9, __VMLINUX_SYMBOL_STR(eth_validate_addr) },
	{ 0xaff94333, __VMLINUX_SYMBOL_STR(usbnet_start_xmit) },
	{ 0x6d9f680, __VMLINUX_SYMBOL_STR(usbnet_stop) },
	{ 0xc06f6c44, __VMLINUX_SYMBOL_STR(usbnet_open) },
	{ 0xfaf98462, __VMLINUX_SYMBOL_STR(bitrev32) },
	{ 0x802d0e93, __VMLINUX_SYMBOL_STR(crc32_le) },
	{ 0xed88cff1, __VMLINUX_SYMBOL_STR(usbnet_write_cmd_async) },
	{ 0xb3d01079, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0x5005215, __VMLINUX_SYMBOL_STR(usbnet_suspend) },
	{ 0xb7b980d1, __VMLINUX_SYMBOL_STR(usbnet_resume) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xdf01d54, __VMLINUX_SYMBOL_STR(usb_deregister) },
	{ 0x67a773e9, __VMLINUX_SYMBOL_STR(generic_mii_ioctl) },
	{ 0xfd49696, __VMLINUX_SYMBOL_STR(mii_ethtool_gset) },
	{ 0x26819c5f, __VMLINUX_SYMBOL_STR(mii_ethtool_sset) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xdca237cf, __VMLINUX_SYMBOL_STR(usbnet_get_endpoints) },
	{ 0x713dcff7, __VMLINUX_SYMBOL_STR(mii_nway_restart) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x7e823052, __VMLINUX_SYMBOL_STR(netdev_info) },
	{ 0x25a85511, __VMLINUX_SYMBOL_STR(usbnet_link_change) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xf2249a05, __VMLINUX_SYMBOL_STR(netif_carrier_on) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x9cb57a9b, __VMLINUX_SYMBOL_STR(usbnet_write_cmd_nopm) },
	{ 0x8139bc74, __VMLINUX_SYMBOL_STR(usbnet_write_cmd) },
	{ 0x3c39aced, __VMLINUX_SYMBOL_STR(netdev_warn) },
	{ 0xa38361fd, __VMLINUX_SYMBOL_STR(usbnet_read_cmd_nopm) },
	{ 0x8c755968, __VMLINUX_SYMBOL_STR(usbnet_read_cmd) },
	{ 0xf18903fc, __VMLINUX_SYMBOL_STR(usbnet_skb_return) },
	{ 0xc1b4c309, __VMLINUX_SYMBOL_STR(skb_clone) },
	{ 0x1ce107cb, __VMLINUX_SYMBOL_STR(skb_pull) },
	{ 0xf00f8562, __VMLINUX_SYMBOL_STR(skb_trim) },
	{ 0xe6961bf3, __VMLINUX_SYMBOL_STR(skb_push) },
	{ 0x3b134b3e, __VMLINUX_SYMBOL_STR(dev_kfree_skb_any) },
	{ 0x86a62306, __VMLINUX_SYMBOL_STR(skb_copy_expand) },
	{ 0xb0e602eb, __VMLINUX_SYMBOL_STR(memmove) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=usbnet,mii";

MODULE_ALIAS("usb:v0B95p1790d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0B95p178Ad*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0DF6p0072d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "BB5EBD8420096EF8CC572C6");

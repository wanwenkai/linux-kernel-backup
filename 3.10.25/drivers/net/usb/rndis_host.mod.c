#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xb7b980d1, __VMLINUX_SYMBOL_STR(usbnet_resume) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0xabfb708f, __VMLINUX_SYMBOL_STR(usbnet_probe) },
	{ 0x8b40ef3a, __VMLINUX_SYMBOL_STR(usbnet_cdc_unbind) },
	{ 0x109aeb3f, __VMLINUX_SYMBOL_STR(usbnet_disconnect) },
	{ 0xc1b4c309, __VMLINUX_SYMBOL_STR(skb_clone) },
	{ 0x1765057c, __VMLINUX_SYMBOL_STR(usbnet_generic_cdc_bind) },
	{ 0x6d9f680, __VMLINUX_SYMBOL_STR(usbnet_stop) },
	{ 0xf00f8562, __VMLINUX_SYMBOL_STR(skb_trim) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xdf01d54, __VMLINUX_SYMBOL_STR(usb_deregister) },
	{ 0xf0119601, __VMLINUX_SYMBOL_STR(usb_control_msg) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xaff94333, __VMLINUX_SYMBOL_STR(usbnet_start_xmit) },
	{ 0x5005215, __VMLINUX_SYMBOL_STR(usbnet_suspend) },
	{ 0x1ce107cb, __VMLINUX_SYMBOL_STR(skb_pull) },
	{ 0x3b134b3e, __VMLINUX_SYMBOL_STR(dev_kfree_skb_any) },
	{ 0x86a62306, __VMLINUX_SYMBOL_STR(skb_copy_expand) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0xd653a9f7, __VMLINUX_SYMBOL_STR(usbnet_tx_timeout) },
	{ 0xf18903fc, __VMLINUX_SYMBOL_STR(usbnet_skb_return) },
	{ 0xacb424ee, __VMLINUX_SYMBOL_STR(usb_driver_release_interface) },
	{ 0xc06f6c44, __VMLINUX_SYMBOL_STR(usbnet_open) },
	{ 0x1c2f8949, __VMLINUX_SYMBOL_STR(usb_interrupt_msg) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x1358f7b9, __VMLINUX_SYMBOL_STR(eth_validate_addr) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xb3d01079, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0xb0e602eb, __VMLINUX_SYMBOL_STR(memmove) },
	{ 0xb06acc77, __VMLINUX_SYMBOL_STR(eth_mac_addr) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=usbnet,cdc_ether";

MODULE_ALIAS("usb:v1630p0042d*dc*dsc*dp*ic02isc02ipFFin*");
MODULE_ALIAS("usb:v*p*d*dc*dsc*dp*ic02isc02ipFFin*");
MODULE_ALIAS("usb:v*p*d*dc*dsc*dp*icEFisc01ip01in*");
MODULE_ALIAS("usb:v*p*d*dc*dsc*dp*icE0isc01ip03in*");

MODULE_INFO(srcversion, "A14B6FF99741835DBB4A7A8");

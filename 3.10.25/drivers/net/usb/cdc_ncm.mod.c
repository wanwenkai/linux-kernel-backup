#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf39a165f, __VMLINUX_SYMBOL_STR(usb_altnum_to_altsetting) },
	{ 0xb7b980d1, __VMLINUX_SYMBOL_STR(usbnet_resume) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0xabfb708f, __VMLINUX_SYMBOL_STR(usbnet_probe) },
	{ 0xbb382529, __VMLINUX_SYMBOL_STR(usbnet_set_settings) },
	{ 0x25a85511, __VMLINUX_SYMBOL_STR(usbnet_link_change) },
	{ 0xc4936d39, __VMLINUX_SYMBOL_STR(hrtimer_cancel) },
	{ 0x109aeb3f, __VMLINUX_SYMBOL_STR(usbnet_disconnect) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0xc1b4c309, __VMLINUX_SYMBOL_STR(skb_clone) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0x54efb5d6, __VMLINUX_SYMBOL_STR(cpu_number) },
	{ 0x35ed9f6e, __VMLINUX_SYMBOL_STR(usbnet_nway_reset) },
	{ 0xdf01d54, __VMLINUX_SYMBOL_STR(usb_deregister) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x5958b46b, __VMLINUX_SYMBOL_STR(usb_set_interface) },
	{ 0xfaef0ed, __VMLINUX_SYMBOL_STR(__tasklet_schedule) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x7dbeff38, __VMLINUX_SYMBOL_STR(usb_driver_claim_interface) },
	{ 0xaff94333, __VMLINUX_SYMBOL_STR(usbnet_start_xmit) },
	{ 0x5005215, __VMLINUX_SYMBOL_STR(usbnet_suspend) },
	{ 0x2cc1155d, __VMLINUX_SYMBOL_STR(usbnet_get_link) },
	{ 0x3b134b3e, __VMLINUX_SYMBOL_STR(dev_kfree_skb_any) },
	{ 0x82072614, __VMLINUX_SYMBOL_STR(tasklet_kill) },
	{ 0x3c0a5219, __VMLINUX_SYMBOL_STR(usbnet_get_settings) },
	{ 0x3ff62317, __VMLINUX_SYMBOL_STR(local_bh_disable) },
	{ 0x8c755968, __VMLINUX_SYMBOL_STR(usbnet_read_cmd) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x5d2dc510, __VMLINUX_SYMBOL_STR(__alloc_skb) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0x671aa585, __VMLINUX_SYMBOL_STR(usbnet_get_ethernet_addr) },
	{ 0xf18903fc, __VMLINUX_SYMBOL_STR(usbnet_skb_return) },
	{ 0xab9dc6c8, __VMLINUX_SYMBOL_STR(hrtimer_start) },
	{ 0xacb424ee, __VMLINUX_SYMBOL_STR(usb_driver_release_interface) },
	{ 0x799aca4, __VMLINUX_SYMBOL_STR(local_bh_enable) },
	{ 0x4646b5b3, __VMLINUX_SYMBOL_STR(usbnet_get_msglevel) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x22a4778a, __VMLINUX_SYMBOL_STR(hrtimer_init) },
	{ 0xb3d01079, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0xabe924d9, __VMLINUX_SYMBOL_STR(usb_ifnum_to_if) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x5d7a5723, __VMLINUX_SYMBOL_STR(__netif_schedule) },
	{ 0x45f4292a, __VMLINUX_SYMBOL_STR(skb_put) },
	{ 0x1e416050, __VMLINUX_SYMBOL_STR(usbnet_manage_power) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x8139bc74, __VMLINUX_SYMBOL_STR(usbnet_write_cmd) },
	{ 0xda66aefd, __VMLINUX_SYMBOL_STR(usbnet_set_msglevel) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=usbnet";

MODULE_ALIAS("usb:v0BDBp*d*dc*dsc*dp*ic02isc0Dip00in*");
MODULE_ALIAS("usb:v413Cp*d*dc*dsc*dp*ic02isc0Dip00in*");
MODULE_ALIAS("usb:v0930p*d*dc*dsc*dp*ic02isc0Dip00in*");
MODULE_ALIAS("usb:v12D1p*d*dc*dsc*dp*ic02isc0Dip00in*");
MODULE_ALIAS("usb:v12D1p*d*dc*dsc*dp*icFFisc02ip16in*");
MODULE_ALIAS("usb:v12D1p*d*dc*dsc*dp*icFFisc02ip46in*");
MODULE_ALIAS("usb:v12D1p*d*dc*dsc*dp*icFFisc02ip76in*");
MODULE_ALIAS("usb:v1519p0443d*dc*dsc*dp*ic02isc0Dip00in*");
MODULE_ALIAS("usb:v*p*d*dc*dsc*dp*ic02isc0Dip00in*");

MODULE_INFO(srcversion, "27456F3774ED6604CEACE91");

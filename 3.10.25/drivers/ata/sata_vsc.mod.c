#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x116eeefd, __VMLINUX_SYMBOL_STR(ata_bmdma_port_ops) },
	{ 0x906ed660, __VMLINUX_SYMBOL_STR(ata_common_sdev_attrs) },
	{ 0x63205de1, __VMLINUX_SYMBOL_STR(ata_scsi_unlock_native_capacity) },
	{ 0x32d436c7, __VMLINUX_SYMBOL_STR(ata_std_bios_param) },
	{ 0x94a68723, __VMLINUX_SYMBOL_STR(ata_scsi_slave_destroy) },
	{ 0x69e9a2cb, __VMLINUX_SYMBOL_STR(ata_scsi_slave_config) },
	{ 0xcc94722f, __VMLINUX_SYMBOL_STR(ata_scsi_queuecmd) },
	{ 0x39a0cbce, __VMLINUX_SYMBOL_STR(ata_scsi_ioctl) },
	{ 0x5f1d5630, __VMLINUX_SYMBOL_STR(ata_pci_remove_one) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x7e7ed358, __VMLINUX_SYMBOL_STR(ata_bmdma_port_intr) },
	{ 0x11b4af98, __VMLINUX_SYMBOL_STR(ata_port_abort) },
	{ 0x255686e9, __VMLINUX_SYMBOL_STR(ata_port_freeze) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x33374b1f, __VMLINUX_SYMBOL_STR(ata_host_activate) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0x21ca769b, __VMLINUX_SYMBOL_STR(pci_bus_write_config_dword) },
	{ 0xc09a6f21, __VMLINUX_SYMBOL_STR(pci_intx) },
	{ 0x98616114, __VMLINUX_SYMBOL_STR(pci_enable_msi_block) },
	{ 0x87f70505, __VMLINUX_SYMBOL_STR(pci_bus_write_config_byte) },
	{ 0xe722ff65, __VMLINUX_SYMBOL_STR(pci_bus_read_config_byte) },
	{ 0xf65f7fc7, __VMLINUX_SYMBOL_STR(dma_supported) },
	{ 0x4a8cbbae, __VMLINUX_SYMBOL_STR(dma_set_mask) },
	{ 0x77f230ad, __VMLINUX_SYMBOL_STR(ata_port_pbar_desc) },
	{ 0xffde746c, __VMLINUX_SYMBOL_STR(pcim_iomap_table) },
	{ 0xa6cec0d, __VMLINUX_SYMBOL_STR(pcim_pin_device) },
	{ 0x5eb4963f, __VMLINUX_SYMBOL_STR(pcim_iomap_regions) },
	{ 0xeb8d93d2, __VMLINUX_SYMBOL_STR(pcim_enable_device) },
	{ 0xe982fc75, __VMLINUX_SYMBOL_STR(ata_host_alloc_pinfo) },
	{ 0x7f2a05ca, __VMLINUX_SYMBOL_STR(ata_print_version) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xb00ae387, __VMLINUX_SYMBOL_STR(ata_sff_check_status) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libata";

MODULE_ALIAS("pci:v00001725d00007174sv*sd*bc01sc06i00*");
MODULE_ALIAS("pci:v00008086d00003200sv*sd*bc01sc06i00*");

MODULE_INFO(srcversion, "002C9C366840834CB264E1B");

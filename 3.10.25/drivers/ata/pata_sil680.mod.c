#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xae348bc1, __VMLINUX_SYMBOL_STR(ata_bmdma32_port_ops) },
	{ 0x906ed660, __VMLINUX_SYMBOL_STR(ata_common_sdev_attrs) },
	{ 0x63205de1, __VMLINUX_SYMBOL_STR(ata_scsi_unlock_native_capacity) },
	{ 0x32d436c7, __VMLINUX_SYMBOL_STR(ata_std_bios_param) },
	{ 0x94a68723, __VMLINUX_SYMBOL_STR(ata_scsi_slave_destroy) },
	{ 0x69e9a2cb, __VMLINUX_SYMBOL_STR(ata_scsi_slave_config) },
	{ 0xcc94722f, __VMLINUX_SYMBOL_STR(ata_scsi_queuecmd) },
	{ 0x39a0cbce, __VMLINUX_SYMBOL_STR(ata_scsi_ioctl) },
	{ 0x4393d5d4, __VMLINUX_SYMBOL_STR(ata_pci_device_suspend) },
	{ 0x5f1d5630, __VMLINUX_SYMBOL_STR(ata_pci_remove_one) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0xd62951f2, __VMLINUX_SYMBOL_STR(ata_pci_bmdma_init_one) },
	{ 0x33374b1f, __VMLINUX_SYMBOL_STR(ata_host_activate) },
	{ 0xe818b32b, __VMLINUX_SYMBOL_STR(ata_bmdma_interrupt) },
	{ 0x73a48b4a, __VMLINUX_SYMBOL_STR(ata_sff_std_ports) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0xf65f7fc7, __VMLINUX_SYMBOL_STR(dma_supported) },
	{ 0x4a8cbbae, __VMLINUX_SYMBOL_STR(dma_set_mask) },
	{ 0xffde746c, __VMLINUX_SYMBOL_STR(pcim_iomap_table) },
	{ 0xe982fc75, __VMLINUX_SYMBOL_STR(ata_host_alloc_pinfo) },
	{ 0x5eb4963f, __VMLINUX_SYMBOL_STR(pcim_iomap_regions) },
	{ 0xeb8d93d2, __VMLINUX_SYMBOL_STR(pcim_enable_device) },
	{ 0x7f2a05ca, __VMLINUX_SYMBOL_STR(ata_print_version) },
	{ 0x7c17658d, __VMLINUX_SYMBOL_STR(ata_pio_need_iordy) },
	{ 0xae47e1be, __VMLINUX_SYMBOL_STR(ata_dev_pair) },
	{ 0x952c5e83, __VMLINUX_SYMBOL_STR(pci_bus_read_config_word) },
	{ 0xf10de535, __VMLINUX_SYMBOL_STR(ioread8) },
	{ 0x727c4f3, __VMLINUX_SYMBOL_STR(iowrite8) },
	{ 0x96affe9b, __VMLINUX_SYMBOL_STR(ata_host_resume) },
	{ 0xb2d449ca, __VMLINUX_SYMBOL_STR(ata_pci_device_do_resume) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x21ca769b, __VMLINUX_SYMBOL_STR(pci_bus_write_config_dword) },
	{ 0x24939c32, __VMLINUX_SYMBOL_STR(pci_bus_write_config_word) },
	{ 0x87f70505, __VMLINUX_SYMBOL_STR(pci_bus_write_config_byte) },
	{ 0xe722ff65, __VMLINUX_SYMBOL_STR(pci_bus_read_config_byte) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libata";

MODULE_ALIAS("pci:v00001095d00000680sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "DBC7AE07CC33A77DEC6A037");

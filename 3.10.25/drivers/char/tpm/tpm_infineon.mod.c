#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x9e91ce13, __VMLINUX_SYMBOL_STR(tpm_store_cancel) },
	{ 0xea6452ba, __VMLINUX_SYMBOL_STR(tpm_show_caps) },
	{ 0x8d021dc, __VMLINUX_SYMBOL_STR(tpm_show_pcrs) },
	{ 0xf38c9761, __VMLINUX_SYMBOL_STR(tpm_show_pubek) },
	{ 0xa22f4941, __VMLINUX_SYMBOL_STR(tpm_release) },
	{ 0xf76edb99, __VMLINUX_SYMBOL_STR(tpm_open) },
	{ 0x565fea31, __VMLINUX_SYMBOL_STR(tpm_write) },
	{ 0xa6dc04d5, __VMLINUX_SYMBOL_STR(tpm_read) },
	{ 0x12a4e61c, __VMLINUX_SYMBOL_STR(no_llseek) },
	{ 0x421a79de, __VMLINUX_SYMBOL_STR(pnp_register_driver) },
	{ 0xfb3043c1, __VMLINUX_SYMBOL_STR(tpm_register_hardware) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x42c8de35, __VMLINUX_SYMBOL_STR(ioremap_nocache) },
	{ 0x1fedf0f4, __VMLINUX_SYMBOL_STR(__request_region) },
	{ 0x526444db, __VMLINUX_SYMBOL_STR(pnp_get_resource) },
	{ 0xadec1761, __VMLINUX_SYMBOL_STR(tpm_remove_hardware) },
	{ 0xf2fb7965, __VMLINUX_SYMBOL_STR(tpm_dev_vendor_release) },
	{ 0x69a358a6, __VMLINUX_SYMBOL_STR(iomem_resource) },
	{ 0xedc03953, __VMLINUX_SYMBOL_STR(iounmap) },
	{ 0x7c61340c, __VMLINUX_SYMBOL_STR(__release_region) },
	{ 0xff7559e4, __VMLINUX_SYMBOL_STR(ioport_resource) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x2355ce79, __VMLINUX_SYMBOL_STR(tpm_pm_resume) },
	{ 0xffac5935, __VMLINUX_SYMBOL_STR(pnp_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=tpm";

MODULE_ALIAS("pnp:dIFX0101*");
MODULE_ALIAS("acpi*:IFX0101:*");
MODULE_ALIAS("pnp:dIFX0102*");
MODULE_ALIAS("acpi*:IFX0102:*");

MODULE_INFO(srcversion, "779916FC9C5FADDE71595E9");

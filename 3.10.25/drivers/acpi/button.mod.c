#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xe54b721f, __VMLINUX_SYMBOL_STR(acpi_bus_register_driver) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xaa8b4be9, __VMLINUX_SYMBOL_STR(single_open) },
	{ 0x691001b5, __VMLINUX_SYMBOL_STR(acpi_evaluate_integer) },
	{ 0xc1eb0c37, __VMLINUX_SYMBOL_STR(single_release) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0x90ce81d5, __VMLINUX_SYMBOL_STR(remove_proc_entry) },
	{ 0x53e0a3b, __VMLINUX_SYMBOL_STR(acpi_bus_unregister_driver) },
	{ 0x9331b8fa, __VMLINUX_SYMBOL_STR(blocking_notifier_chain_unregister) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x9c39470d, __VMLINUX_SYMBOL_STR(seq_read) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0xf4efcb02, __VMLINUX_SYMBOL_STR(input_event) },
	{ 0x7932f22c, __VMLINUX_SYMBOL_STR(PDE_DATA) },
	{ 0x435e0b50, __VMLINUX_SYMBOL_STR(proc_mkdir) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x10f90df1, __VMLINUX_SYMBOL_STR(acpi_bus_generate_proc_event) },
	{ 0x77456e0a, __VMLINUX_SYMBOL_STR(acpi_root_dir) },
	{ 0x63b626e8, __VMLINUX_SYMBOL_STR(input_register_device) },
	{ 0x168f5114, __VMLINUX_SYMBOL_STR(blocking_notifier_call_chain) },
	{ 0x8b11d42d, __VMLINUX_SYMBOL_STR(input_free_device) },
	{ 0x8b110124, __VMLINUX_SYMBOL_STR(pm_wakeup_event) },
	{ 0x6ffd847, __VMLINUX_SYMBOL_STR(blocking_notifier_chain_register) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x7f76e4a5, __VMLINUX_SYMBOL_STR(proc_create_data) },
	{ 0xd2bcdb9a, __VMLINUX_SYMBOL_STR(seq_lseek) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x16aa29dd, __VMLINUX_SYMBOL_STR(input_unregister_device) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x6ad85887, __VMLINUX_SYMBOL_STR(acpi_enable_gpe) },
	{ 0x77acb41d, __VMLINUX_SYMBOL_STR(acpi_device_hid) },
	{ 0x9e363b6b, __VMLINUX_SYMBOL_STR(acpi_disable_gpe) },
	{ 0xf412c760, __VMLINUX_SYMBOL_STR(device_set_wakeup_enable) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
	{ 0xf5a1fb40, __VMLINUX_SYMBOL_STR(input_allocate_device) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("acpi*:PNP0C0D:*");
MODULE_ALIAS("acpi*:PNP0C0E:*");
MODULE_ALIAS("acpi*:LNXSLPBN:*");
MODULE_ALIAS("acpi*:PNP0C0C:*");
MODULE_ALIAS("acpi*:LNXPWRBN:*");

MODULE_INFO(srcversion, "C071743328E3FDD5A223D2A");

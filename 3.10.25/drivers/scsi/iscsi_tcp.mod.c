#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x6942caeb, __VMLINUX_SYMBOL_STR(iscsi_change_queue_depth) },
	{ 0xb11f2182, __VMLINUX_SYMBOL_STR(iscsi_target_alloc) },
	{ 0xafdce62b, __VMLINUX_SYMBOL_STR(iscsi_eh_recover_target) },
	{ 0xb5107020, __VMLINUX_SYMBOL_STR(iscsi_eh_device_reset) },
	{ 0x5e85ce11, __VMLINUX_SYMBOL_STR(iscsi_eh_abort) },
	{ 0xb08e5d8c, __VMLINUX_SYMBOL_STR(iscsi_queuecommand) },
	{ 0x65432038, __VMLINUX_SYMBOL_STR(iscsi_session_recovery_timedout) },
	{ 0x64d711cb, __VMLINUX_SYMBOL_STR(iscsi_tcp_cleanup_task) },
	{ 0x9fded913, __VMLINUX_SYMBOL_STR(iscsi_tcp_task_xmit) },
	{ 0x79b42460, __VMLINUX_SYMBOL_STR(iscsi_tcp_task_init) },
	{ 0x517ccd47, __VMLINUX_SYMBOL_STR(iscsi_conn_send_pdu) },
	{ 0x48daadd2, __VMLINUX_SYMBOL_STR(iscsi_host_set_param) },
	{ 0xf4b4b747, __VMLINUX_SYMBOL_STR(iscsi_session_get_param) },
	{ 0x1440fa2f, __VMLINUX_SYMBOL_STR(iscsi_conn_start) },
	{ 0x6d044c26, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0xf9887e96, __VMLINUX_SYMBOL_STR(iscsi_register_transport) },
	{ 0xd663751c, __VMLINUX_SYMBOL_STR(blk_queue_dma_alignment) },
	{ 0x664d950c, __VMLINUX_SYMBOL_STR(blk_queue_bounce_limit) },
	{ 0x1934eac3, __VMLINUX_SYMBOL_STR(iscsi_tcp_r2tpool_alloc) },
	{ 0x16684f4d, __VMLINUX_SYMBOL_STR(iscsi_session_setup) },
	{ 0xe3d8183c, __VMLINUX_SYMBOL_STR(iscsi_host_add) },
	{ 0xc5bc531f, __VMLINUX_SYMBOL_STR(iscsi_host_alloc) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x585a3794, __VMLINUX_SYMBOL_STR(iscsi_host_free) },
	{ 0x6bcddf32, __VMLINUX_SYMBOL_STR(iscsi_host_remove) },
	{ 0x79c6a5d3, __VMLINUX_SYMBOL_STR(iscsi_session_teardown) },
	{ 0xb5cfa930, __VMLINUX_SYMBOL_STR(iscsi_tcp_r2tpool_free) },
	{ 0x36caf629, __VMLINUX_SYMBOL_STR(scsi_is_host_device) },
	{ 0x8b449442, __VMLINUX_SYMBOL_STR(crypto_alloc_base) },
	{ 0x2e22b8e6, __VMLINUX_SYMBOL_STR(iscsi_tcp_conn_setup) },
	{ 0x13798496, __VMLINUX_SYMBOL_STR(iscsi_tcp_recv_skb) },
	{ 0x476ccd62, __VMLINUX_SYMBOL_STR(tcp_read_sock) },
	{ 0x9b0c8220, __VMLINUX_SYMBOL_STR(_raw_read_lock) },
	{ 0x2bbb3ca9, __VMLINUX_SYMBOL_STR(iscsi_conn_queue_work) },
	{ 0xbded85d0, __VMLINUX_SYMBOL_STR(_raw_read_unlock_bh) },
	{ 0xa5a99b49, __VMLINUX_SYMBOL_STR(_raw_read_lock_bh) },
	{ 0x6920f587, __VMLINUX_SYMBOL_STR(iscsi_tcp_hdr_recv_prep) },
	{ 0xf1af252f, __VMLINUX_SYMBOL_STR(sk_set_memalloc) },
	{ 0xb43d7f3c, __VMLINUX_SYMBOL_STR(iscsi_conn_bind) },
	{ 0xc60dcf91, __VMLINUX_SYMBOL_STR(sockfd_lookup) },
	{ 0x173eb2d3, __VMLINUX_SYMBOL_STR(iscsi_conn_stop) },
	{ 0xaa4c7d47, __VMLINUX_SYMBOL_STR(iscsi_suspend_tx) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xfd1ada6e, __VMLINUX_SYMBOL_STR(iscsi_tcp_conn_teardown) },
	{ 0x9e6f20de, __VMLINUX_SYMBOL_STR(crypto_destroy_tfm) },
	{ 0x32f55c5e, __VMLINUX_SYMBOL_STR(fput) },
	{ 0x9025f7fc, __VMLINUX_SYMBOL_STR(sk_free) },
	{ 0x5bd6c1ee, __VMLINUX_SYMBOL_STR(_raw_write_unlock_bh) },
	{ 0xd5dac574, __VMLINUX_SYMBOL_STR(_raw_write_lock_bh) },
	{ 0x437fe60a, __VMLINUX_SYMBOL_STR(iscsi_tcp_set_max_r2t) },
	{ 0xce0ff185, __VMLINUX_SYMBOL_STR(sock_no_sendpage) },
	{ 0x39aa93b, __VMLINUX_SYMBOL_STR(iscsi_set_param) },
	{ 0x74fac18a, __VMLINUX_SYMBOL_STR(iscsi_conn_get_param) },
	{ 0x72301010, __VMLINUX_SYMBOL_STR(kernel_getpeername) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x98fdb25f, __VMLINUX_SYMBOL_STR(iscsi_host_get_param) },
	{ 0xc0cb02f4, __VMLINUX_SYMBOL_STR(iscsi_conn_get_addr_param) },
	{ 0xe8f50022, __VMLINUX_SYMBOL_STR(kernel_getsockname) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0xd0baf2, __VMLINUX_SYMBOL_STR(iscsi_tcp_conn_get_stats) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
	{ 0xe41b67b0, __VMLINUX_SYMBOL_STR(iscsi_conn_failure) },
	{ 0x13a3272d, __VMLINUX_SYMBOL_STR(iscsi_tcp_segment_done) },
	{ 0xcb1f8397, __VMLINUX_SYMBOL_STR(iscsi_tcp_segment_unmap) },
	{ 0xf1474452, __VMLINUX_SYMBOL_STR(kernel_sendmsg) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x53a8c0a9, __VMLINUX_SYMBOL_STR(iscsi_segment_seek_sg) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x5f65762d, __VMLINUX_SYMBOL_STR(iscsi_segment_init_linear) },
	{ 0x46ac8f80, __VMLINUX_SYMBOL_STR(iscsi_tcp_dgst_header) },
	{ 0x2162100b, __VMLINUX_SYMBOL_STR(dev_printk) },
	{ 0x6b7d7f16, __VMLINUX_SYMBOL_STR(iscsi_unregister_transport) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libiscsi,libiscsi_tcp,scsi_transport_iscsi,scsi_mod";


MODULE_INFO(srcversion, "ACA792D58FACCF019198DAA");

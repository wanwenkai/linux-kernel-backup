#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x399b261a, __VMLINUX_SYMBOL_STR(__kfree_skb) },
	{ 0x4c4fef19, __VMLINUX_SYMBOL_STR(kernel_stack) },
	{ 0x6bcddf32, __VMLINUX_SYMBOL_STR(iscsi_host_remove) },
	{ 0xd6ee688f, __VMLINUX_SYMBOL_STR(vmalloc) },
	{ 0x437fe60a, __VMLINUX_SYMBOL_STR(iscsi_tcp_set_max_r2t) },
	{ 0x2162100b, __VMLINUX_SYMBOL_STR(dev_printk) },
	{ 0x1b6314fd, __VMLINUX_SYMBOL_STR(in_aton) },
	{ 0x13798496, __VMLINUX_SYMBOL_STR(iscsi_tcp_recv_skb) },
	{ 0x56121368, __VMLINUX_SYMBOL_STR(dst_release) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0xcfb261a9, __VMLINUX_SYMBOL_STR(pci_dev_get) },
	{ 0x3fec048f, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0x6b7d7f16, __VMLINUX_SYMBOL_STR(iscsi_unregister_transport) },
	{ 0xc0cb02f4, __VMLINUX_SYMBOL_STR(iscsi_conn_get_addr_param) },
	{ 0x79c6a5d3, __VMLINUX_SYMBOL_STR(iscsi_session_teardown) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x999e8297, __VMLINUX_SYMBOL_STR(vfree) },
	{ 0x89da0b07, __VMLINUX_SYMBOL_STR(neigh_destroy) },
	{ 0xe3d8183c, __VMLINUX_SYMBOL_STR(iscsi_host_add) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xc5bc531f, __VMLINUX_SYMBOL_STR(iscsi_host_alloc) },
	{ 0xdd263c03, __VMLINUX_SYMBOL_STR(iscsi_lookup_endpoint) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0xb5cfa930, __VMLINUX_SYMBOL_STR(iscsi_tcp_r2tpool_free) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0x1934eac3, __VMLINUX_SYMBOL_STR(iscsi_tcp_r2tpool_alloc) },
	{ 0x48daadd2, __VMLINUX_SYMBOL_STR(iscsi_host_set_param) },
	{ 0x585a3794, __VMLINUX_SYMBOL_STR(iscsi_host_free) },
	{ 0xaa4c7d47, __VMLINUX_SYMBOL_STR(iscsi_suspend_tx) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x6d886024, __VMLINUX_SYMBOL_STR(iscsi_destroy_endpoint) },
	{ 0x28fd2cd3, __VMLINUX_SYMBOL_STR(scsi_host_put) },
	{ 0x88dc702c, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0x81533963, __VMLINUX_SYMBOL_STR(sysfs_format_mac) },
	{ 0x2e22b8e6, __VMLINUX_SYMBOL_STR(iscsi_tcp_conn_setup) },
	{ 0x8d48a0fe, __VMLINUX_SYMBOL_STR(__get_page_tail) },
	{ 0x155fb1c8, __VMLINUX_SYMBOL_STR(vlan_dev_real_dev) },
	{ 0x39aa93b, __VMLINUX_SYMBOL_STR(iscsi_set_param) },
	{ 0x5d2dc510, __VMLINUX_SYMBOL_STR(__alloc_skb) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0xf9887e96, __VMLINUX_SYMBOL_STR(iscsi_register_transport) },
	{ 0x64d711cb, __VMLINUX_SYMBOL_STR(iscsi_tcp_cleanup_task) },
	{ 0x2bbb3ca9, __VMLINUX_SYMBOL_STR(iscsi_conn_queue_work) },
	{ 0x6920f587, __VMLINUX_SYMBOL_STR(iscsi_tcp_hdr_recv_prep) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xe41b67b0, __VMLINUX_SYMBOL_STR(iscsi_conn_failure) },
	{ 0x18c9d7f7, __VMLINUX_SYMBOL_STR(ip_route_output_flow) },
	{ 0x5bd6c1ee, __VMLINUX_SYMBOL_STR(_raw_write_unlock_bh) },
	{ 0xa5a99b49, __VMLINUX_SYMBOL_STR(_raw_read_lock_bh) },
	{ 0xbded85d0, __VMLINUX_SYMBOL_STR(_raw_read_unlock_bh) },
	{ 0x5d3ecd4d, __VMLINUX_SYMBOL_STR(iscsi_create_endpoint) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x1a4ac71, __VMLINUX_SYMBOL_STR(__ip_dev_find) },
	{ 0xd5dac574, __VMLINUX_SYMBOL_STR(_raw_write_lock_bh) },
	{ 0x16684f4d, __VMLINUX_SYMBOL_STR(iscsi_session_setup) },
	{ 0x98fdb25f, __VMLINUX_SYMBOL_STR(iscsi_host_get_param) },
	{ 0xb43d7f3c, __VMLINUX_SYMBOL_STR(iscsi_conn_bind) },
	{ 0x12121db8, __VMLINUX_SYMBOL_STR(pci_dev_put) },
	{ 0x79086eea, __VMLINUX_SYMBOL_STR(iscsi_tcp_recv_segment_is_hdr) },
	{ 0x45f4292a, __VMLINUX_SYMBOL_STR(skb_put) },
	{ 0x6d044c26, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libiscsi,libiscsi_tcp,scsi_transport_iscsi,scsi_mod";


MODULE_INFO(srcversion, "D11AF26C2201D4103DFE694");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x128d3bc0, __VMLINUX_SYMBOL_STR(scsi_set_medium_removal) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xe381912f, __VMLINUX_SYMBOL_STR(blk_queue_rq_timeout) },
	{ 0xb027f24b, __VMLINUX_SYMBOL_STR(scsi_prep_return) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xbf06b565, __VMLINUX_SYMBOL_STR(alloc_disk) },
	{ 0x4c4fef19, __VMLINUX_SYMBOL_STR(kernel_stack) },
	{ 0x92387997, __VMLINUX_SYMBOL_STR(blk_queue_prep_rq) },
	{ 0xc65e73c, __VMLINUX_SYMBOL_STR(scsi_normalize_sense) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0xad3fc77e, __VMLINUX_SYMBOL_STR(cdrom_number_of_slots) },
	{ 0xfef96e23, __VMLINUX_SYMBOL_STR(__scsi_print_command) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0x2162100b, __VMLINUX_SYMBOL_STR(dev_printk) },
	{ 0x3a785795, __VMLINUX_SYMBOL_STR(scsi_execute_req_flags) },
	{ 0xc6f33fc, __VMLINUX_SYMBOL_STR(unregister_cdrom) },
	{ 0x3fec048f, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xd209497, __VMLINUX_SYMBOL_STR(cdrom_open) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x1750b1b1, __VMLINUX_SYMBOL_STR(scsi_execute) },
	{ 0x5b7de463, __VMLINUX_SYMBOL_STR(scsi_autopm_get_device) },
	{ 0x3b7032f6, __VMLINUX_SYMBOL_STR(scsi_setup_fs_cmnd) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x39e2ac34, __VMLINUX_SYMBOL_STR(cdrom_release) },
	{ 0x777d1ee3, __VMLINUX_SYMBOL_STR(del_gendisk) },
	{ 0xaf3dd7dc, __VMLINUX_SYMBOL_STR(scsi_logging_level) },
	{ 0x1e845db5, __VMLINUX_SYMBOL_STR(driver_unregister) },
	{ 0x5a921311, __VMLINUX_SYMBOL_STR(strncmp) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xbf29fac9, __VMLINUX_SYMBOL_STR(cdrom_get_media_event) },
	{ 0x71a50dbc, __VMLINUX_SYMBOL_STR(register_blkdev) },
	{ 0x120e011c, __VMLINUX_SYMBOL_STR(scsi_prep_fn) },
	{ 0x33cdcd12, __VMLINUX_SYMBOL_STR(cdrom_check_events) },
	{ 0xf11543ff, __VMLINUX_SYMBOL_STR(find_first_zero_bit) },
	{ 0xb6936ffe, __VMLINUX_SYMBOL_STR(_bcd2bin) },
	{ 0x55bdd887, __VMLINUX_SYMBOL_STR(kmalloc_dma_caches) },
	{ 0xb5a459dc, __VMLINUX_SYMBOL_STR(unregister_blkdev) },
	{ 0x7814f57e, __VMLINUX_SYMBOL_STR(cdrom_get_last_written) },
	{ 0x83338307, __VMLINUX_SYMBOL_STR(scsi_device_put) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x39633301, __VMLINUX_SYMBOL_STR(scsi_nonblockable_ioctl) },
	{ 0x210e3df5, __VMLINUX_SYMBOL_STR(scsi_ioctl) },
	{ 0x430a89d7, __VMLINUX_SYMBOL_STR(put_disk) },
	{ 0x534130cf, __VMLINUX_SYMBOL_STR(scsi_register_driver) },
	{ 0x657e090b, __VMLINUX_SYMBOL_STR(scsi_block_when_processing_errors) },
	{ 0x743e72a, __VMLINUX_SYMBOL_STR(scsi_test_unit_ready) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xc813450, __VMLINUX_SYMBOL_STR(add_disk) },
	{ 0xbcd2162f, __VMLINUX_SYMBOL_STR(cdrom_ioctl) },
	{ 0x1952ee46, __VMLINUX_SYMBOL_STR(scsi_setup_blk_pc_cmnd) },
	{ 0x221cd5ea, __VMLINUX_SYMBOL_STR(register_cdrom) },
	{ 0xe63389e7, __VMLINUX_SYMBOL_STR(scsi_device_get) },
	{ 0x5bcdc14f, __VMLINUX_SYMBOL_STR(scsi_autopm_put_device) },
	{ 0x5ce6d1ea, __VMLINUX_SYMBOL_STR(blk_queue_logical_block_size) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xb3303502, __VMLINUX_SYMBOL_STR(scsi_mode_sense) },
	{ 0xca5dbc50, __VMLINUX_SYMBOL_STR(scsi_print_sense_hdr) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=scsi_mod,cdrom";


MODULE_INFO(srcversion, "470B5DC3D45179652DED294");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x8803b914, __VMLINUX_SYMBOL_STR(cpufreq_cpu_get) },
	{ 0x950ffff2, __VMLINUX_SYMBOL_STR(cpu_online_mask) },
	{ 0x76b48223, __VMLINUX_SYMBOL_STR(cpufreq_register_governor) },
	{ 0x5826598b, __VMLINUX_SYMBOL_STR(dbs_check_cpu) },
	{ 0xc715d9e0, __VMLINUX_SYMBOL_STR(boot_cpu_data) },
	{ 0xb54533f7, __VMLINUX_SYMBOL_STR(usecs_to_jiffies) },
	{ 0xc0a3d105, __VMLINUX_SYMBOL_STR(find_next_bit) },
	{ 0x5528d259, __VMLINUX_SYMBOL_STR(cancel_delayed_work_sync) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x54efb5d6, __VMLINUX_SYMBOL_STR(cpu_number) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x53614269, __VMLINUX_SYMBOL_STR(get_cpu_idle_time_us) },
	{ 0x60944f48, __VMLINUX_SYMBOL_STR(__cpufreq_driver_target) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xfe7c4287, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0x9f2bdaac, __VMLINUX_SYMBOL_STR(__bitmap_or) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x20c55ae0, __VMLINUX_SYMBOL_STR(sscanf) },
	{ 0x52f86ab4, __VMLINUX_SYMBOL_STR(cpufreq_unregister_governor) },
	{ 0x773369c0, __VMLINUX_SYMBOL_STR(need_load_eval) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xab2703ff, __VMLINUX_SYMBOL_STR(cpufreq_governor_dbs) },
	{ 0x54740eb7, __VMLINUX_SYMBOL_STR(get_cpu_idle_time) },
	{ 0xd7d79132, __VMLINUX_SYMBOL_STR(put_online_cpus) },
	{ 0x2f47d8c7, __VMLINUX_SYMBOL_STR(cpufreq_frequency_get_table) },
	{ 0x7f24de73, __VMLINUX_SYMBOL_STR(jiffies_to_usecs) },
	{ 0x3efb35c9, __VMLINUX_SYMBOL_STR(get_online_cpus) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x38592536, __VMLINUX_SYMBOL_STR(cpufreq_cpu_put) },
	{ 0x32047ad5, __VMLINUX_SYMBOL_STR(__per_cpu_offset) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x4cbbd171, __VMLINUX_SYMBOL_STR(__bitmap_weight) },
	{ 0xe42fe2d8, __VMLINUX_SYMBOL_STR(gov_queue_work) },
	{ 0xf16add4, __VMLINUX_SYMBOL_STR(cpufreq_frequency_table_target) },
	{ 0x5567c227, __VMLINUX_SYMBOL_STR(kernel_cpustat) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=freq_table";


MODULE_INFO(srcversion, "03C6CDC33228FAE0C0570B1");

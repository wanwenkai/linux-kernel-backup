#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x50c542f1, __VMLINUX_SYMBOL_STR(ide_prep_sense) },
	{ 0xaa1dcc14, __VMLINUX_SYMBOL_STR(ide_dma_unmap_sg) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xd9cc2acc, __VMLINUX_SYMBOL_STR(blk_delay_queue) },
	{ 0xbf06b565, __VMLINUX_SYMBOL_STR(alloc_disk) },
	{ 0x68cd8268, __VMLINUX_SYMBOL_STR(driver_register) },
	{ 0x92387997, __VMLINUX_SYMBOL_STR(blk_queue_prep_rq) },
	{ 0x18e70634, __VMLINUX_SYMBOL_STR(ide_read_error) },
	{ 0xaa8b4be9, __VMLINUX_SYMBOL_STR(single_open) },
	{ 0x4f476e96, __VMLINUX_SYMBOL_STR(init_cdrom_command) },
	{ 0xad3fc77e, __VMLINUX_SYMBOL_STR(cdrom_number_of_slots) },
	{ 0x1d4d84db, __VMLINUX_SYMBOL_STR(ide_read_bcount_and_ireason) },
	{ 0x21c56f52, __VMLINUX_SYMBOL_STR(ide_error) },
	{ 0xc1eb0c37, __VMLINUX_SYMBOL_STR(single_release) },
	{ 0xf776d804, __VMLINUX_SYMBOL_STR(blk_put_request) },
	{ 0xc6f33fc, __VMLINUX_SYMBOL_STR(unregister_cdrom) },
	{ 0xc37ee324, __VMLINUX_SYMBOL_STR(ide_dump_status) },
	{ 0x3912b95d, __VMLINUX_SYMBOL_STR(ide_complete_rq) },
	{ 0x163405ae, __VMLINUX_SYMBOL_STR(blk_dump_rq_flags) },
	{ 0x27606b02, __VMLINUX_SYMBOL_STR(ide_pio_bytes) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0x73dd9fa7, __VMLINUX_SYMBOL_STR(generic_ide_ioctl) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xd209497, __VMLINUX_SYMBOL_STR(cdrom_open) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x9c39470d, __VMLINUX_SYMBOL_STR(seq_read) },
	{ 0x48ba524a, __VMLINUX_SYMBOL_STR(ide_map_sg) },
	{ 0x65ab94c4, __VMLINUX_SYMBOL_STR(ide_set_handler) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0xadd58aad, __VMLINUX_SYMBOL_STR(ide_queue_sense_rq) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x7932f22c, __VMLINUX_SYMBOL_STR(PDE_DATA) },
	{ 0x424758a3, __VMLINUX_SYMBOL_STR(ide_end_rq) },
	{ 0x620ec38d, __VMLINUX_SYMBOL_STR(device_del) },
	{ 0xc61c7f2e, __VMLINUX_SYMBOL_STR(device_register) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x42411fc5, __VMLINUX_SYMBOL_STR(ide_cd_expiry) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x39e2ac34, __VMLINUX_SYMBOL_STR(cdrom_release) },
	{ 0x777d1ee3, __VMLINUX_SYMBOL_STR(del_gendisk) },
	{ 0xddd1aced, __VMLINUX_SYMBOL_STR(blk_queue_update_dma_pad) },
	{ 0x1e845db5, __VMLINUX_SYMBOL_STR(driver_unregister) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xbf29fac9, __VMLINUX_SYMBOL_STR(cdrom_get_media_event) },
	{ 0x1e6d26a8, __VMLINUX_SYMBOL_STR(strstr) },
	{ 0xff25906a, __VMLINUX_SYMBOL_STR(ide_init_disk) },
	{ 0x9a1a4c98, __VMLINUX_SYMBOL_STR(ide_proc_unregister_driver) },
	{ 0x95aef99c, __VMLINUX_SYMBOL_STR(ide_device_get) },
	{ 0x2bf3dd36, __VMLINUX_SYMBOL_STR(cdrom_mode_sense) },
	{ 0x33cdcd12, __VMLINUX_SYMBOL_STR(cdrom_check_events) },
	{ 0xb6936ffe, __VMLINUX_SYMBOL_STR(_bcd2bin) },
	{ 0x7814f57e, __VMLINUX_SYMBOL_STR(cdrom_get_last_written) },
	{ 0x3db50da3, __VMLINUX_SYMBOL_STR(ide_issue_pc) },
	{ 0xabfbe59e, __VMLINUX_SYMBOL_STR(ide_bus_type) },
	{ 0xd253c123, __VMLINUX_SYMBOL_STR(blk_rq_map_kern) },
	{ 0x3490eae9, __VMLINUX_SYMBOL_STR(ide_dma_off) },
	{ 0x17a54f1c, __VMLINUX_SYMBOL_STR(put_device) },
	{ 0xdc68d7e5, __VMLINUX_SYMBOL_STR(blk_execute_rq) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x430a89d7, __VMLINUX_SYMBOL_STR(put_disk) },
	{ 0x2c120543, __VMLINUX_SYMBOL_STR(ide_init_sg_cmd) },
	{ 0x6cf49ce7, __VMLINUX_SYMBOL_STR(cdrom_mode_select) },
	{ 0x5a67ba68, __VMLINUX_SYMBOL_STR(ide_pad_transfer) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xb0415876, __VMLINUX_SYMBOL_STR(get_device) },
	{ 0xd2bcdb9a, __VMLINUX_SYMBOL_STR(seq_lseek) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x226ec602, __VMLINUX_SYMBOL_STR(ide_proc_register_driver) },
	{ 0xd663751c, __VMLINUX_SYMBOL_STR(blk_queue_dma_alignment) },
	{ 0xc813450, __VMLINUX_SYMBOL_STR(add_disk) },
	{ 0x3ddcdebf, __VMLINUX_SYMBOL_STR(blk_end_request_all) },
	{ 0xbcd2162f, __VMLINUX_SYMBOL_STR(cdrom_ioctl) },
	{ 0x221cd5ea, __VMLINUX_SYMBOL_STR(register_cdrom) },
	{ 0xe0583868, __VMLINUX_SYMBOL_STR(ide_device_put) },
	{ 0x6cb08e95, __VMLINUX_SYMBOL_STR(ide_check_ireason) },
	{ 0x291f89b7, __VMLINUX_SYMBOL_STR(dev_set_name) },
	{ 0x5ce6d1ea, __VMLINUX_SYMBOL_STR(blk_queue_logical_block_size) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0xe5d95985, __VMLINUX_SYMBOL_STR(param_ops_ulong) },
	{ 0x80e3b5d1, __VMLINUX_SYMBOL_STR(blk_get_request) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=cdrom";


MODULE_INFO(srcversion, "633FDDFF938E4669529A684");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xfff2f85, __VMLINUX_SYMBOL_STR(register_virtio_driver) },
	{ 0x71a50dbc, __VMLINUX_SYMBOL_STR(register_blkdev) },
	{ 0x43a53735, __VMLINUX_SYMBOL_STR(__alloc_workqueue_key) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x1af82fce, __VMLINUX_SYMBOL_STR(kobject_uevent_env) },
	{ 0x39944dcb, __VMLINUX_SYMBOL_STR(dev_notice) },
	{ 0x3c9d1211, __VMLINUX_SYMBOL_STR(string_get_size) },
	{ 0xe94b430c, __VMLINUX_SYMBOL_STR(blk_start_request) },
	{ 0xfa801eb, __VMLINUX_SYMBOL_STR(blk_peek_request) },
	{ 0x51e8a6e0, __VMLINUX_SYMBOL_STR(blk_rq_map_sg) },
	{ 0xe9dff136, __VMLINUX_SYMBOL_STR(mempool_alloc) },
	{ 0x222e7ce2, __VMLINUX_SYMBOL_STR(sysfs_streq) },
	{ 0x7e08f3f1, __VMLINUX_SYMBOL_STR(device_create_file) },
	{ 0xc813450, __VMLINUX_SYMBOL_STR(add_disk) },
	{ 0xb9eb8335, __VMLINUX_SYMBOL_STR(blk_queue_io_opt) },
	{ 0x6e0fef1c, __VMLINUX_SYMBOL_STR(blk_queue_io_min) },
	{ 0x367960da, __VMLINUX_SYMBOL_STR(blk_queue_alignment_offset) },
	{ 0x5e32451b, __VMLINUX_SYMBOL_STR(blk_queue_physical_block_size) },
	{ 0x5ce6d1ea, __VMLINUX_SYMBOL_STR(blk_queue_logical_block_size) },
	{ 0x98e091a1, __VMLINUX_SYMBOL_STR(blk_queue_max_segment_size) },
	{ 0xd8f9f68c, __VMLINUX_SYMBOL_STR(blk_queue_max_hw_sectors) },
	{ 0x664d950c, __VMLINUX_SYMBOL_STR(blk_queue_bounce_limit) },
	{ 0x6a6feb36, __VMLINUX_SYMBOL_STR(blk_queue_max_segments) },
	{ 0x75a876b7, __VMLINUX_SYMBOL_STR(set_disk_ro) },
	{ 0xb0e602eb, __VMLINUX_SYMBOL_STR(memmove) },
	{ 0xf476116b, __VMLINUX_SYMBOL_STR(blk_queue_make_request) },
	{ 0x62d06521, __VMLINUX_SYMBOL_STR(blk_init_queue) },
	{ 0xbf06b565, __VMLINUX_SYMBOL_STR(alloc_disk) },
	{ 0x3f71faa1, __VMLINUX_SYMBOL_STR(mempool_create) },
	{ 0xa05c03df, __VMLINUX_SYMBOL_STR(mempool_kmalloc) },
	{ 0x6a037cf1, __VMLINUX_SYMBOL_STR(mempool_kfree) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xc897c382, __VMLINUX_SYMBOL_STR(sg_init_table) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x44e6ecc8, __VMLINUX_SYMBOL_STR(ida_simple_get) },
	{ 0x121f115c, __VMLINUX_SYMBOL_STR(revalidate_disk) },
	{ 0x757cb9db, __VMLINUX_SYMBOL_STR(blk_queue_flush) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0xc4148678, __VMLINUX_SYMBOL_STR(scsi_cmd_blk_ioctl) },
	{ 0x865b9977, __VMLINUX_SYMBOL_STR(virtio_check_driver_offered_feature) },
	{ 0xa9d8f55e, __VMLINUX_SYMBOL_STR(ida_simple_remove) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xefba93e1, __VMLINUX_SYMBOL_STR(mempool_destroy) },
	{ 0x430a89d7, __VMLINUX_SYMBOL_STR(put_disk) },
	{ 0x2e133e3e, __VMLINUX_SYMBOL_STR(blk_cleanup_queue) },
	{ 0x777d1ee3, __VMLINUX_SYMBOL_STR(del_gendisk) },
	{ 0xdadcff4e, __VMLINUX_SYMBOL_STR(blk_sync_queue) },
	{ 0xa2cd8a7a, __VMLINUX_SYMBOL_STR(blk_stop_queue) },
	{ 0x8487a2b6, __VMLINUX_SYMBOL_STR(flush_work) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xd32fc3b, __VMLINUX_SYMBOL_STR(blk_bio_map_sg) },
	{ 0x8b3ee142, __VMLINUX_SYMBOL_STR(virtqueue_kick) },
	{ 0xfa66f77c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x93a6e0b2, __VMLINUX_SYMBOL_STR(io_schedule) },
	{ 0xf8983de7, __VMLINUX_SYMBOL_STR(prepare_to_wait_exclusive) },
	{ 0xc8b57c27, __VMLINUX_SYMBOL_STR(autoremove_wake_function) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0xbf899fdb, __VMLINUX_SYMBOL_STR(blk_start_queue) },
	{ 0xfed6bf54, __VMLINUX_SYMBOL_STR(virtqueue_enable_cb) },
	{ 0xffb126f4, __VMLINUX_SYMBOL_STR(virtqueue_get_buf) },
	{ 0x92c962b, __VMLINUX_SYMBOL_STR(__blk_end_request_all) },
	{ 0x4aad52d7, __VMLINUX_SYMBOL_STR(mempool_free) },
	{ 0xe0794ec4, __VMLINUX_SYMBOL_STR(bio_endio) },
	{ 0x698f3c25, __VMLINUX_SYMBOL_STR(virtqueue_disable_cb) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x78699eb7, __VMLINUX_SYMBOL_STR(virtqueue_add_sgs) },
	{ 0xb6244511, __VMLINUX_SYMBOL_STR(sg_init_one) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x8c03d20c, __VMLINUX_SYMBOL_STR(destroy_workqueue) },
	{ 0x817a930e, __VMLINUX_SYMBOL_STR(unregister_virtio_driver) },
	{ 0xb5a459dc, __VMLINUX_SYMBOL_STR(unregister_blkdev) },
	{ 0xf776d804, __VMLINUX_SYMBOL_STR(blk_put_request) },
	{ 0xdc68d7e5, __VMLINUX_SYMBOL_STR(blk_execute_rq) },
	{ 0x403fbba, __VMLINUX_SYMBOL_STR(bio_put) },
	{ 0x3596605c, __VMLINUX_SYMBOL_STR(blk_make_request) },
	{ 0x125e618f, __VMLINUX_SYMBOL_STR(bio_map_kern) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=virtio,virtio_ring";

MODULE_ALIAS("virtio:d00000002v*");

MODULE_INFO(srcversion, "CE17230704D652E94EDC914");

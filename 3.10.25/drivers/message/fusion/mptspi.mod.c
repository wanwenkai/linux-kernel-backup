#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x46884346, __VMLINUX_SYMBOL_STR(mptscsih_host_attrs) },
	{ 0xe7cebeac, __VMLINUX_SYMBOL_STR(mptscsih_show_info) },
	{ 0x5183dff5, __VMLINUX_SYMBOL_STR(mptscsih_bios_param) },
	{ 0xeb17aa3b, __VMLINUX_SYMBOL_STR(mptscsih_change_queue_depth) },
	{ 0xd68e9809, __VMLINUX_SYMBOL_STR(mptscsih_host_reset) },
	{ 0xa4e54add, __VMLINUX_SYMBOL_STR(mptscsih_bus_reset) },
	{ 0x660e1105, __VMLINUX_SYMBOL_STR(mptscsih_dev_reset) },
	{ 0xf92d9b83, __VMLINUX_SYMBOL_STR(mptscsih_abort) },
	{ 0xc9407759, __VMLINUX_SYMBOL_STR(mptscsih_info) },
	{ 0x4d5d663d, __VMLINUX_SYMBOL_STR(mptscsih_shutdown) },
	{ 0x694c50e2, __VMLINUX_SYMBOL_STR(mptscsih_suspend) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x810f7ea6, __VMLINUX_SYMBOL_STR(scsi_scan_target) },
	{ 0xe744bf51, __VMLINUX_SYMBOL_STR(mpt_findImVolumes) },
	{ 0xbc78810, __VMLINUX_SYMBOL_STR(mptscsih_slave_configure) },
	{ 0x9bffd2dd, __VMLINUX_SYMBOL_STR(__scsi_iterate_devices) },
	{ 0x82173bec, __VMLINUX_SYMBOL_STR(spi_display_xfer_agreement) },
	{ 0x1db1a478, __VMLINUX_SYMBOL_STR(spi_dv_device) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xd1960438, __VMLINUX_SYMBOL_STR(mpt_free_msg_frame) },
	{ 0xd33d061c, __VMLINUX_SYMBOL_STR(mpt_HardResetHandler) },
	{ 0x53f6ffbc, __VMLINUX_SYMBOL_STR(wait_for_completion_timeout) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x9c8a18c9, __VMLINUX_SYMBOL_STR(mptscsih_event_process) },
	{ 0xb3889d24, __VMLINUX_SYMBOL_STR(mptscsih_ioc_reset) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0xb088e9e8, __VMLINUX_SYMBOL_STR(mpt_reset_register) },
	{ 0xdac9968f, __VMLINUX_SYMBOL_STR(mpt_event_register) },
	{ 0x24958def, __VMLINUX_SYMBOL_STR(mptscsih_scandv_complete) },
	{ 0x51cbe003, __VMLINUX_SYMBOL_STR(mptscsih_taskmgmt_complete) },
	{ 0x83f7cc6c, __VMLINUX_SYMBOL_STR(mpt_register) },
	{ 0x81c06abd, __VMLINUX_SYMBOL_STR(mptscsih_io_done) },
	{ 0x5aec6cd9, __VMLINUX_SYMBOL_STR(spi_attach_transport) },
	{ 0x7ab80e45, __VMLINUX_SYMBOL_STR(mptscsih_remove) },
	{ 0x67365a5c, __VMLINUX_SYMBOL_STR(scsi_scan_host) },
	{ 0x7cc5931a, __VMLINUX_SYMBOL_STR(mptscsih_IssueTaskMgmt) },
	{ 0x51973243, __VMLINUX_SYMBOL_STR(scsi_add_host_with_dma) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xaddb8893, __VMLINUX_SYMBOL_STR(scsi_host_alloc) },
	{ 0x3de027e4, __VMLINUX_SYMBOL_STR(mpt_attach) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9cbeb511, __VMLINUX_SYMBOL_STR(mptscsih_qcmd) },
	{ 0x491e6a54, __VMLINUX_SYMBOL_STR(scsi_print_command) },
	{ 0x6c137fc1, __VMLINUX_SYMBOL_STR(scsi_cmd_get_serial) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xb791135, __VMLINUX_SYMBOL_STR(mpt_put_msg_frame) },
	{ 0x93937fa4, __VMLINUX_SYMBOL_STR(mpt_get_msg_frame) },
	{ 0x448cc567, __VMLINUX_SYMBOL_STR(mptscsih_slave_destroy) },
	{ 0xc563212b, __VMLINUX_SYMBOL_STR(mptscsih_raid_id_to_num) },
	{ 0xb5a35d57, __VMLINUX_SYMBOL_STR(mptscsih_is_phys_disk) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xc83e34bd, __VMLINUX_SYMBOL_STR(mpt_config) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x6d553aa8, __VMLINUX_SYMBOL_STR(scsi_device_lookup_by_target) },
	{ 0x2162100b, __VMLINUX_SYMBOL_STR(dev_printk) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0x36caf629, __VMLINUX_SYMBOL_STR(scsi_is_host_device) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x30394567, __VMLINUX_SYMBOL_STR(mptscsih_resume) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x54ae2f09, __VMLINUX_SYMBOL_STR(spi_release_transport) },
	{ 0xc47c22e8, __VMLINUX_SYMBOL_STR(mpt_deregister) },
	{ 0x4526289b, __VMLINUX_SYMBOL_STR(mpt_event_deregister) },
	{ 0xd9a92a75, __VMLINUX_SYMBOL_STR(mpt_reset_deregister) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mptscsih,scsi_mod,mptbase,scsi_transport_spi";

MODULE_ALIAS("pci:v00001000d00000030sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v0000117Cd00000030sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000040sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "C704B01EA98284063D3E1F6");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x12a4e61c, __VMLINUX_SYMBOL_STR(no_llseek) },
	{ 0xd33d061c, __VMLINUX_SYMBOL_STR(mpt_HardResetHandler) },
	{ 0xc43edc56, __VMLINUX_SYMBOL_STR(mpt_alloc_fw_memory) },
	{ 0xde8b1fa1, __VMLINUX_SYMBOL_STR(mpt_free_fw_memory) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x4b06d2e7, __VMLINUX_SYMBOL_STR(complete) },
	{ 0xf86f84ba, __VMLINUX_SYMBOL_STR(kill_fasync) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xdac9968f, __VMLINUX_SYMBOL_STR(mpt_event_register) },
	{ 0xb088e9e8, __VMLINUX_SYMBOL_STR(mpt_reset_register) },
	{ 0x83f7cc6c, __VMLINUX_SYMBOL_STR(mpt_register) },
	{ 0x638ef410, __VMLINUX_SYMBOL_STR(misc_register) },
	{ 0x6e52e086, __VMLINUX_SYMBOL_STR(mpt_device_driver_register) },
	{ 0xc83e34bd, __VMLINUX_SYMBOL_STR(mpt_config) },
	{ 0x8ba98207, __VMLINUX_SYMBOL_STR(mutex_lock_interruptible) },
	{ 0x50a58e8e, __VMLINUX_SYMBOL_STR(mutex_trylock) },
	{ 0x24a4a6bd, __VMLINUX_SYMBOL_STR(mpt_GetIocState) },
	{ 0xb791135, __VMLINUX_SYMBOL_STR(mpt_put_msg_frame) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0x9bffd2dd, __VMLINUX_SYMBOL_STR(__scsi_iterate_devices) },
	{ 0x816d6df, __VMLINUX_SYMBOL_STR(mpt_Soft_Hard_ResetHandler) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xba2dc38b, __VMLINUX_SYMBOL_STR(mpt_halt_firmware) },
	{ 0x502108d0, __VMLINUX_SYMBOL_STR(mpt_fwfault_debug) },
	{ 0x53f6ffbc, __VMLINUX_SYMBOL_STR(wait_for_completion_timeout) },
	{ 0xd1960438, __VMLINUX_SYMBOL_STR(mpt_free_msg_frame) },
	{ 0xc841ddb7, __VMLINUX_SYMBOL_STR(mpt_send_handshake_request) },
	{ 0x8e6c0401, __VMLINUX_SYMBOL_STR(mpt_put_msg_frame_hi_pri) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x1b85919e, __VMLINUX_SYMBOL_STR(mpt_clear_taskmgmt_in_progress_flag) },
	{ 0x93937fa4, __VMLINUX_SYMBOL_STR(mpt_get_msg_frame) },
	{ 0x9b0c3b48, __VMLINUX_SYMBOL_STR(mpt_set_taskmgmt_in_progress_flag) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x9166fada, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0x86f8d9fe, __VMLINUX_SYMBOL_STR(mpt_verify_adapter) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xc5244b8a, __VMLINUX_SYMBOL_STR(fasync_helper) },
	{ 0xdd805159, __VMLINUX_SYMBOL_STR(ioc_list) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xc0e69f82, __VMLINUX_SYMBOL_STR(mpt_device_driver_deregister) },
	{ 0xc47c22e8, __VMLINUX_SYMBOL_STR(mpt_deregister) },
	{ 0xd9a92a75, __VMLINUX_SYMBOL_STR(mpt_reset_deregister) },
	{ 0x4526289b, __VMLINUX_SYMBOL_STR(mpt_event_deregister) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x28638b77, __VMLINUX_SYMBOL_STR(misc_deregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mptbase,scsi_mod";


MODULE_INFO(srcversion, "CEB5390A668A825609C05A9");

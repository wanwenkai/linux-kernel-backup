#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3b01e6ea, __VMLINUX_SYMBOL_STR(drm_release) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x54930663, __VMLINUX_SYMBOL_STR(drm_compat_ioctl) },
	{ 0x4c4fef19, __VMLINUX_SYMBOL_STR(kernel_stack) },
	{ 0x765a81ae, __VMLINUX_SYMBOL_STR(drm_pci_exit) },
	{ 0xf8daa1d7, __VMLINUX_SYMBOL_STR(drm_ati_pcigart_cleanup) },
	{ 0x3074f033, __VMLINUX_SYMBOL_STR(drm_order) },
	{ 0xfce4fd07, __VMLINUX_SYMBOL_STR(drm_mmap) },
	{ 0x7d3abc8d, __VMLINUX_SYMBOL_STR(drm_core_ioremap_wc) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xa7092745, __VMLINUX_SYMBOL_STR(drm_vblank_init) },
	{ 0x6b43df53, __VMLINUX_SYMBOL_STR(platform_device_register_full) },
	{ 0x72f0aa39, __VMLINUX_SYMBOL_STR(drm_ati_pcigart_init) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0xa85e3d28, __VMLINUX_SYMBOL_STR(drm_handle_vblank) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xfcf65499, __VMLINUX_SYMBOL_STR(noop_llseek) },
	{ 0xa013ba3d, __VMLINUX_SYMBOL_STR(platform_device_unregister) },
	{ 0x9745d638, __VMLINUX_SYMBOL_STR(drm_err) },
	{ 0x5e8aa95b, __VMLINUX_SYMBOL_STR(drm_core_ioremapfree) },
	{ 0xaf72c522, __VMLINUX_SYMBOL_STR(drm_getsarea) },
	{ 0xcebb348f, __VMLINUX_SYMBOL_STR(drm_ioctl) },
	{ 0x41f480f2, __VMLINUX_SYMBOL_STR(drm_irq_uninstall) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xbffde8ec, __VMLINUX_SYMBOL_STR(compat_alloc_user_space) },
	{ 0x2e526f67, __VMLINUX_SYMBOL_STR(request_firmware) },
	{ 0x94a951de, __VMLINUX_SYMBOL_STR(drm_pci_init) },
	{ 0x498dcb5e, __VMLINUX_SYMBOL_STR(drm_ut_debug_printk) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0x75c3f17b, __VMLINUX_SYMBOL_STR(release_firmware) },
	{ 0x4450ffe3, __VMLINUX_SYMBOL_STR(drm_poll) },
	{ 0xd1278c9, __VMLINUX_SYMBOL_STR(drm_fasync) },
	{ 0xd5de7b88, __VMLINUX_SYMBOL_STR(drm_open) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=drm";


MODULE_INFO(srcversion, "26E4A8889D4BD35BDAF11D4");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3b01e6ea, __VMLINUX_SYMBOL_STR(drm_release) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x54930663, __VMLINUX_SYMBOL_STR(drm_compat_ioctl) },
	{ 0x765a81ae, __VMLINUX_SYMBOL_STR(drm_pci_exit) },
	{ 0xfce4fd07, __VMLINUX_SYMBOL_STR(drm_mmap) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x5666bbfc, __VMLINUX_SYMBOL_STR(drm_core_ioremap) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xa4fb9a07, __VMLINUX_SYMBOL_STR(drm_idlelock_release) },
	{ 0xfe7c4287, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xfcf65499, __VMLINUX_SYMBOL_STR(noop_llseek) },
	{ 0x9745d638, __VMLINUX_SYMBOL_STR(drm_err) },
	{ 0x5e8aa95b, __VMLINUX_SYMBOL_STR(drm_core_ioremapfree) },
	{ 0xcebb348f, __VMLINUX_SYMBOL_STR(drm_ioctl) },
	{ 0x76a495c1, __VMLINUX_SYMBOL_STR(cpu_possible_mask) },
	{ 0x41f480f2, __VMLINUX_SYMBOL_STR(drm_irq_uninstall) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd392a04f, __VMLINUX_SYMBOL_STR(vm_mmap) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x24005d9d, __VMLINUX_SYMBOL_STR(remap_pfn_range) },
	{ 0x4cbbd171, __VMLINUX_SYMBOL_STR(__bitmap_weight) },
	{ 0x5b56860c, __VMLINUX_SYMBOL_STR(vm_munmap) },
	{ 0x5da8137a, __VMLINUX_SYMBOL_STR(drm_idlelock_take) },
	{ 0x94a951de, __VMLINUX_SYMBOL_STR(drm_pci_init) },
	{ 0x498dcb5e, __VMLINUX_SYMBOL_STR(drm_ut_debug_printk) },
	{ 0x4450ffe3, __VMLINUX_SYMBOL_STR(drm_poll) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0xd1278c9, __VMLINUX_SYMBOL_STR(drm_fasync) },
	{ 0xd5de7b88, __VMLINUX_SYMBOL_STR(drm_open) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=drm";


MODULE_INFO(srcversion, "EF9508B30D5BE0458E4F9F2");

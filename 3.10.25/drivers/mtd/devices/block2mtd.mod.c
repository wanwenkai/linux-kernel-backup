#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x7c890f67, __VMLINUX_SYMBOL_STR(mtd_device_unregister) },
	{ 0xd65d2ade, __VMLINUX_SYMBOL_STR(mtd_erase_callback) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x38baa76b, __VMLINUX_SYMBOL_STR(balance_dirty_pages_ratelimited) },
	{ 0x67f43102, __VMLINUX_SYMBOL_STR(unlock_page) },
	{ 0xdb795fb5, __VMLINUX_SYMBOL_STR(set_page_dirty) },
	{ 0x86cc3e99, __VMLINUX_SYMBOL_STR(__lock_page) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xc2aee12b, __VMLINUX_SYMBOL_STR(sync_blockdev) },
	{ 0x2e710893, __VMLINUX_SYMBOL_STR(put_page) },
	{ 0xb8b21bf1, __VMLINUX_SYMBOL_STR(read_cache_page) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x2dc0ab23, __VMLINUX_SYMBOL_STR(mtd_device_parse_register) },
	{ 0x733c3b54, __VMLINUX_SYMBOL_STR(kasprintf) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x89c89565, __VMLINUX_SYMBOL_STR(blkdev_get_by_path) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x20000329, __VMLINUX_SYMBOL_STR(simple_strtoul) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x85df9b6c, __VMLINUX_SYMBOL_STR(strsep) },
	{ 0x9f984513, __VMLINUX_SYMBOL_STR(strrchr) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
	{ 0xa916b694, __VMLINUX_SYMBOL_STR(strnlen) },
	{ 0xf1c1e695, __VMLINUX_SYMBOL_STR(blkdev_put) },
	{ 0xec087399, __VMLINUX_SYMBOL_STR(invalidate_mapping_pages) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mtd";


MODULE_INFO(srcversion, "178EFD48F2F9659E3A481D0");

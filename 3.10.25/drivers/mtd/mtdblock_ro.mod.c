#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x14685016, __VMLINUX_SYMBOL_STR(register_mtd_blktrans) },
	{ 0xea5a901f, __VMLINUX_SYMBOL_STR(mtd_read) },
	{ 0xa6227f4e, __VMLINUX_SYMBOL_STR(mtd_write) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x65d20e1, __VMLINUX_SYMBOL_STR(add_mtd_blktrans_dev) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xa753ac39, __VMLINUX_SYMBOL_STR(del_mtd_blktrans_dev) },
	{ 0x6e0c3cea, __VMLINUX_SYMBOL_STR(deregister_mtd_blktrans) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mtd_blkdevs,mtd";


MODULE_INFO(srcversion, "E18BD9F152BD41A461742D8");

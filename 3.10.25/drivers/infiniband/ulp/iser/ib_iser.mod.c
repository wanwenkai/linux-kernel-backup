#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xb294dd6, __VMLINUX_SYMBOL_STR(kmem_cache_destroy) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x4c4fef19, __VMLINUX_SYMBOL_STR(kernel_stack) },
	{ 0x6bcddf32, __VMLINUX_SYMBOL_STR(iscsi_host_remove) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x48b8bb5e, __VMLINUX_SYMBOL_STR(ib_fmr_pool_map_phys) },
	{ 0x3bfe8d2c, __VMLINUX_SYMBOL_STR(ib_dealloc_pd) },
	{ 0xc8b57c27, __VMLINUX_SYMBOL_STR(autoremove_wake_function) },
	{ 0x6942caeb, __VMLINUX_SYMBOL_STR(iscsi_change_queue_depth) },
	{ 0xb08e5d8c, __VMLINUX_SYMBOL_STR(iscsi_queuecommand) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0x173eb2d3, __VMLINUX_SYMBOL_STR(iscsi_conn_stop) },
	{ 0x3fec048f, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0xafdce62b, __VMLINUX_SYMBOL_STR(iscsi_eh_recover_target) },
	{ 0x6b7d7f16, __VMLINUX_SYMBOL_STR(iscsi_unregister_transport) },
	{ 0xc0cb02f4, __VMLINUX_SYMBOL_STR(iscsi_conn_get_addr_param) },
	{ 0x79c6a5d3, __VMLINUX_SYMBOL_STR(iscsi_session_teardown) },
	{ 0x6ddbd44, __VMLINUX_SYMBOL_STR(rdma_destroy_id) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xe3d8183c, __VMLINUX_SYMBOL_STR(iscsi_host_add) },
	{ 0xc32b59ff, __VMLINUX_SYMBOL_STR(ib_destroy_fmr_pool) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xc5bc531f, __VMLINUX_SYMBOL_STR(iscsi_host_alloc) },
	{ 0xdd263c03, __VMLINUX_SYMBOL_STR(iscsi_lookup_endpoint) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0xe6abe22d, __VMLINUX_SYMBOL_STR(rdma_connect) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x36caf629, __VMLINUX_SYMBOL_STR(scsi_is_host_device) },
	{ 0x4a2fc29b, __VMLINUX_SYMBOL_STR(ib_alloc_pd) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0xa3db70d, __VMLINUX_SYMBOL_STR(ib_get_dma_mr) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x1440fa2f, __VMLINUX_SYMBOL_STR(iscsi_conn_start) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x20c55ae0, __VMLINUX_SYMBOL_STR(sscanf) },
	{ 0xe222595e, __VMLINUX_SYMBOL_STR(rdma_destroy_qp) },
	{ 0x517ccd47, __VMLINUX_SYMBOL_STR(iscsi_conn_send_pdu) },
	{ 0xf4b4b747, __VMLINUX_SYMBOL_STR(iscsi_session_get_param) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0x48daadd2, __VMLINUX_SYMBOL_STR(iscsi_host_set_param) },
	{ 0x3d4bb27f, __VMLINUX_SYMBOL_STR(iscsi_conn_teardown) },
	{ 0xfaef0ed, __VMLINUX_SYMBOL_STR(__tasklet_schedule) },
	{ 0xcc831c6a, __VMLINUX_SYMBOL_STR(iscsi_put_task) },
	{ 0x585a3794, __VMLINUX_SYMBOL_STR(iscsi_host_free) },
	{ 0xaa4c7d47, __VMLINUX_SYMBOL_STR(iscsi_suspend_tx) },
	{ 0x74fac18a, __VMLINUX_SYMBOL_STR(iscsi_conn_get_param) },
	{ 0xc9409b91, __VMLINUX_SYMBOL_STR(kmem_cache_free) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x6d886024, __VMLINUX_SYMBOL_STR(iscsi_destroy_endpoint) },
	{ 0x9545af6d, __VMLINUX_SYMBOL_STR(tasklet_init) },
	{ 0x67eca25a, __VMLINUX_SYMBOL_STR(rdma_create_id) },
	{ 0xfbc89c16, __VMLINUX_SYMBOL_STR(ib_destroy_cq) },
	{ 0x82072614, __VMLINUX_SYMBOL_STR(tasklet_kill) },
	{ 0xb971585e, __VMLINUX_SYMBOL_STR(rdma_create_qp) },
	{ 0x39aa93b, __VMLINUX_SYMBOL_STR(iscsi_set_param) },
	{ 0x91c0f7cf, __VMLINUX_SYMBOL_STR(ib_unregister_event_handler) },
	{ 0xd086d3c8, __VMLINUX_SYMBOL_STR(rdma_resolve_route) },
	{ 0x51bc3a46, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xb6ccaa58, __VMLINUX_SYMBOL_STR(ib_register_event_handler) },
	{ 0x93fca811, __VMLINUX_SYMBOL_STR(__get_free_pages) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0xd62c833f, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0xf9887e96, __VMLINUX_SYMBOL_STR(iscsi_register_transport) },
	{ 0xb5107020, __VMLINUX_SYMBOL_STR(iscsi_eh_device_reset) },
	{ 0x15ae9aeb, __VMLINUX_SYMBOL_STR(iscsi_prep_data_out_pdu) },
	{ 0x578ccefb, __VMLINUX_SYMBOL_STR(rdma_disconnect) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x361f099f, __VMLINUX_SYMBOL_STR(iscsi_complete_pdu) },
	{ 0xe41b67b0, __VMLINUX_SYMBOL_STR(iscsi_conn_failure) },
	{ 0x67cc86e8, __VMLINUX_SYMBOL_STR(kmem_cache_create) },
	{ 0xad5377f9, __VMLINUX_SYMBOL_STR(ib_dereg_mr) },
	{ 0x4302d0eb, __VMLINUX_SYMBOL_STR(free_pages) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xb6244511, __VMLINUX_SYMBOL_STR(sg_init_one) },
	{ 0x5d3ecd4d, __VMLINUX_SYMBOL_STR(iscsi_create_endpoint) },
	{ 0x5e85ce11, __VMLINUX_SYMBOL_STR(iscsi_eh_abort) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x5c8b5ce8, __VMLINUX_SYMBOL_STR(prepare_to_wait) },
	{ 0xbd5152bc, __VMLINUX_SYMBOL_STR(ib_create_fmr_pool) },
	{ 0x55dc18a1, __VMLINUX_SYMBOL_STR(rdma_resolve_addr) },
	{ 0x16684f4d, __VMLINUX_SYMBOL_STR(iscsi_session_setup) },
	{ 0xfa66f77c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x98fdb25f, __VMLINUX_SYMBOL_STR(iscsi_host_get_param) },
	{ 0xb43d7f3c, __VMLINUX_SYMBOL_STR(iscsi_conn_bind) },
	{ 0xb11f2182, __VMLINUX_SYMBOL_STR(iscsi_target_alloc) },
	{ 0x87dcd430, __VMLINUX_SYMBOL_STR(ib_create_cq) },
	{ 0x67d238dd, __VMLINUX_SYMBOL_STR(ib_fmr_pool_unmap) },
	{ 0x65432038, __VMLINUX_SYMBOL_STR(iscsi_session_recovery_timedout) },
	{ 0xa4817be, __VMLINUX_SYMBOL_STR(iscsi_conn_setup) },
	{ 0x6d044c26, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libiscsi,ib_core,scsi_transport_iscsi,rdma_cm,scsi_mod";


MODULE_INFO(srcversion, "E26CB39DCE2700A65C74F37");

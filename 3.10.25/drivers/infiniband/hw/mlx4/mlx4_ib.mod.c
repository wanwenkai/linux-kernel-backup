#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xcc79a16b, __VMLINUX_SYMBOL_STR(mlx4_get_parav_qkey) },
	{ 0x54554948, __VMLINUX_SYMBOL_STR(kobject_put) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0xdda27acf, __VMLINUX_SYMBOL_STR(mlx4_mr_enable) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x75b1f1fb, __VMLINUX_SYMBOL_STR(kobject_get) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x28319db8, __VMLINUX_SYMBOL_STR(mlx4_counter_alloc) },
	{ 0xa4caa594, __VMLINUX_SYMBOL_STR(mlx4_pd_free) },
	{ 0x19639f10, __VMLINUX_SYMBOL_STR(mlx4_srq_alloc) },
	{ 0x16948512, __VMLINUX_SYMBOL_STR(mlx4_qp_reserve_range) },
	{ 0x42957f54, __VMLINUX_SYMBOL_STR(mlx4_srq_free) },
	{ 0xb3e31a34, __VMLINUX_SYMBOL_STR(mlx4_qp_alloc) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x4b71906a, __VMLINUX_SYMBOL_STR(mlx4_INIT_PORT) },
	{ 0x3bfe8d2c, __VMLINUX_SYMBOL_STR(ib_dealloc_pd) },
	{ 0x43a53735, __VMLINUX_SYMBOL_STR(__alloc_workqueue_key) },
	{ 0xa27db657, __VMLINUX_SYMBOL_STR(mlx4_sync_pkey_table) },
	{ 0x952860b8, __VMLINUX_SYMBOL_STR(rdma_port_get_link_layer) },
	{ 0x33d79ab0, __VMLINUX_SYMBOL_STR(mlx4_srq_lookup) },
	{ 0x47079364, __VMLINUX_SYMBOL_STR(mlx4_fmr_free) },
	{ 0xe201c5b7, __VMLINUX_SYMBOL_STR(vlan_dev_vlan_id) },
	{ 0x3aa97aff, __VMLINUX_SYMBOL_STR(ib_register_mad_agent) },
	{ 0xd2669cf5, __VMLINUX_SYMBOL_STR(mlx4_register_interface) },
	{ 0xc715d9e0, __VMLINUX_SYMBOL_STR(boot_cpu_data) },
	{ 0xc7a4fbed, __VMLINUX_SYMBOL_STR(rtnl_lock) },
	{ 0xa72c5e19, __VMLINUX_SYMBOL_STR(ib_destroy_qp) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0xd2da1048, __VMLINUX_SYMBOL_STR(register_netdevice_notifier) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0xf42e9206, __VMLINUX_SYMBOL_STR(mlx4_db_alloc) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x5528d259, __VMLINUX_SYMBOL_STR(cancel_delayed_work_sync) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x6dcb79f4, __VMLINUX_SYMBOL_STR(mlx4_mw_free) },
	{ 0x460da262, __VMLINUX_SYMBOL_STR(mlx4_gen_pkey_eqe) },
	{ 0xaa526e2a, __VMLINUX_SYMBOL_STR(mlx4_cq_modify) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x92fb4b07, __VMLINUX_SYMBOL_STR(mlx4_buf_free) },
	{ 0x331fc2a9, __VMLINUX_SYMBOL_STR(sysfs_remove_group) },
	{ 0xc176acce, __VMLINUX_SYMBOL_STR(mlx4_cq_resize) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x9d0d6206, __VMLINUX_SYMBOL_STR(unregister_netdevice_notifier) },
	{ 0x6414d180, __VMLINUX_SYMBOL_STR(ib_get_cached_gid) },
	{ 0x7fe32873, __VMLINUX_SYMBOL_STR(rb_replace_node) },
	{ 0x91db6a6b, __VMLINUX_SYMBOL_STR(ib_destroy_ah) },
	{ 0xf036d1f6, __VMLINUX_SYMBOL_STR(ib_modify_qp) },
	{ 0xf6f71534, __VMLINUX_SYMBOL_STR(mlx4_qp_free) },
	{ 0xc10ca2e1, __VMLINUX_SYMBOL_STR(kobject_create_and_add) },
	{ 0xb2af5bec, __VMLINUX_SYMBOL_STR(ib_alloc_device) },
	{ 0x35faaf43, __VMLINUX_SYMBOL_STR(ib_free_send_mad) },
	{ 0xbafe4b10, __VMLINUX_SYMBOL_STR(mlx4_srq_arm) },
	{ 0xaf4ba0be, __VMLINUX_SYMBOL_STR(ib_dealloc_device) },
	{ 0x78a04bae, __VMLINUX_SYMBOL_STR(mlx4_qp_remove) },
	{ 0x35a57fb2, __VMLINUX_SYMBOL_STR(dev_mc_add) },
	{ 0xece784c2, __VMLINUX_SYMBOL_STR(rb_first) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xb09e8dad, __VMLINUX_SYMBOL_STR(ib_create_qp) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0xf9304350, __VMLINUX_SYMBOL_STR(mlx4_mw_enable) },
	{ 0xc7e5985c, __VMLINUX_SYMBOL_STR(mlx4_mtt_init) },
	{ 0x6d0aba34, __VMLINUX_SYMBOL_STR(wait_for_completion) },
	{ 0xabbc7d94, __VMLINUX_SYMBOL_STR(mlx4_mw_alloc) },
	{ 0x50d1f870, __VMLINUX_SYMBOL_STR(pgprot_writecombine) },
	{ 0x4a2fc29b, __VMLINUX_SYMBOL_STR(ib_alloc_pd) },
	{ 0x416f25b6, __VMLINUX_SYMBOL_STR(cancel_delayed_work) },
	{ 0xf52d0111, __VMLINUX_SYMBOL_STR(mlx4_assign_eq) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0xc770d15c, __VMLINUX_SYMBOL_STR(idr_alloc_cyclic) },
	{ 0xa3db70d, __VMLINUX_SYMBOL_STR(ib_get_dma_mr) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x20c55ae0, __VMLINUX_SYMBOL_STR(sscanf) },
	{ 0xe31c05, __VMLINUX_SYMBOL_STR(mlx4_qp_modify) },
	{ 0x14fafc17, __VMLINUX_SYMBOL_STR(sysfs_create_group) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xa2118a69, __VMLINUX_SYMBOL_STR(kobject_init_and_add) },
	{ 0x959fd487, __VMLINUX_SYMBOL_STR(mlx4_buf_alloc) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0x2b048fb3, __VMLINUX_SYMBOL_STR(mlx4_fmr_alloc) },
	{ 0xc45705d9, __VMLINUX_SYMBOL_STR(mlx4_xrcd_alloc) },
	{ 0x8fd7313, __VMLINUX_SYMBOL_STR(mlx4_mr_free) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x4d9b652b, __VMLINUX_SYMBOL_STR(rb_erase) },
	{ 0x9166fada, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0xe34a3bb1, __VMLINUX_SYMBOL_STR(dev_mc_del) },
	{ 0xddbe6055, __VMLINUX_SYMBOL_STR(mlx4_gen_guid_change_eqe) },
	{ 0x7ab31ee2, __VMLINUX_SYMBOL_STR(ib_query_port) },
	{ 0xea9130a4, __VMLINUX_SYMBOL_STR(mlx4_uar_alloc) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x8c03d20c, __VMLINUX_SYMBOL_STR(destroy_workqueue) },
	{ 0x84ffea8b, __VMLINUX_SYMBOL_STR(idr_preload) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xc845f161, __VMLINUX_SYMBOL_STR(ib_umem_get) },
	{ 0x472fa97a, __VMLINUX_SYMBOL_STR(mlx4_map_phys_fmr) },
	{ 0x87cc1504, __VMLINUX_SYMBOL_STR(mlx4_gen_port_state_change_eqe) },
	{ 0x40d0964f, __VMLINUX_SYMBOL_STR(mlx4_counter_free) },
	{ 0xa735db59, __VMLINUX_SYMBOL_STR(prandom_u32) },
	{ 0x9b772d90, __VMLINUX_SYMBOL_STR(mlx4_alloc_cmd_mailbox) },
	{ 0x222b98ad, __VMLINUX_SYMBOL_STR(ib_get_cached_pkey) },
	{ 0x88dc702c, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0x16126c05, __VMLINUX_SYMBOL_STR(mlx4_CLOSE_PORT) },
	{ 0x42160169, __VMLINUX_SYMBOL_STR(flush_workqueue) },
	{ 0x8897a675, __VMLINUX_SYMBOL_STR(mlx4_cq_free) },
	{ 0xfc150956, __VMLINUX_SYMBOL_STR(ib_ud_header_init) },
	{ 0x2fd0e42f, __VMLINUX_SYMBOL_STR(ib_sa_unregister_client) },
	{ 0xfbc89c16, __VMLINUX_SYMBOL_STR(ib_destroy_cq) },
	{ 0x155fb1c8, __VMLINUX_SYMBOL_STR(vlan_dev_real_dev) },
	{ 0xb9e0d7b0, __VMLINUX_SYMBOL_STR(mlx4_is_slave_active) },
	{ 0x67ecda6a, __VMLINUX_SYMBOL_STR(ib_dispatch_event) },
	{ 0x4d19d532, __VMLINUX_SYMBOL_STR(ib_create_send_mad) },
	{ 0x9068e5da, __VMLINUX_SYMBOL_STR(ib_post_send_mad) },
	{ 0xcaef1b77, __VMLINUX_SYMBOL_STR(idr_remove) },
	{ 0x7e08f3f1, __VMLINUX_SYMBOL_STR(device_create_file) },
	{ 0x2d42803b, __VMLINUX_SYMBOL_STR(ib_unregister_mad_agent) },
	{ 0xda0d50ec, __VMLINUX_SYMBOL_STR(ib_sa_cancel_query) },
	{ 0xde3463f1, __VMLINUX_SYMBOL_STR(ib_create_ah) },
	{ 0x68d6c9e6, __VMLINUX_SYMBOL_STR(ib_sa_guid_info_rec_query) },
	{ 0xac5623c3, __VMLINUX_SYMBOL_STR(set_and_calc_slave_port_state) },
	{ 0x1f7014dc, __VMLINUX_SYMBOL_STR(mlx4_multicast_attach) },
	{ 0x87343e08, __VMLINUX_SYMBOL_STR(mlx4_mr_alloc) },
	{ 0x6f36c782, __VMLINUX_SYMBOL_STR(idr_find_slowpath) },
	{ 0xc6f3cf36, __VMLINUX_SYMBOL_STR(ib_umem_page_count) },
	{ 0x42c8de35, __VMLINUX_SYMBOL_STR(ioremap_nocache) },
	{ 0x51f1dcbb, __VMLINUX_SYMBOL_STR(mlx4_get_slave_port_state) },
	{ 0xb2777dba, __VMLINUX_SYMBOL_STR(mlx4_db_free) },
	{ 0x7bce451a, __VMLINUX_SYMBOL_STR(__mlx4_cmd) },
	{ 0x93fca811, __VMLINUX_SYMBOL_STR(__get_free_pages) },
	{ 0x53f01b6b, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x5ba6337, __VMLINUX_SYMBOL_STR(mlx4_qp_query) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x87832d17, __VMLINUX_SYMBOL_STR(ib_register_device) },
	{ 0x394f39f2, __VMLINUX_SYMBOL_STR(mlx4_xrcd_free) },
	{ 0xf8504814, __VMLINUX_SYMBOL_STR(mlx4_uar_free) },
	{ 0xb8e90dd5, __VMLINUX_SYMBOL_STR(mlx4_mtt_cleanup) },
	{ 0x96b29254, __VMLINUX_SYMBOL_STR(strncasecmp) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0xdcdb0270, __VMLINUX_SYMBOL_STR(mlx4_free_cmd_mailbox) },
	{ 0x4ad13443, __VMLINUX_SYMBOL_STR(ib_find_cached_pkey) },
	{ 0xc6a31ab3, __VMLINUX_SYMBOL_STR(ib_unregister_device) },
	{ 0x88081ac7, __VMLINUX_SYMBOL_STR(mlx4_SYNC_TPT) },
	{ 0xc9d1852c, __VMLINUX_SYMBOL_STR(mlx4_get_protocol_dev) },
	{ 0x80892842, __VMLINUX_SYMBOL_STR(mlx4_cq_alloc) },
	{ 0x111bc7a0, __VMLINUX_SYMBOL_STR(sysfs_create_file) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x429bf74a, __VMLINUX_SYMBOL_STR(mlx4_qp_release_range) },
	{ 0xa5526619, __VMLINUX_SYMBOL_STR(rb_insert_color) },
	{ 0x29a1b49b, __VMLINUX_SYMBOL_STR(mlx4_release_eq) },
	{ 0xad5377f9, __VMLINUX_SYMBOL_STR(ib_dereg_mr) },
	{ 0x49926a7c, __VMLINUX_SYMBOL_STR(mlx4_srq_query) },
	{ 0x4302d0eb, __VMLINUX_SYMBOL_STR(free_pages) },
	{ 0x8838e541, __VMLINUX_SYMBOL_STR(mlx4_put_slave_node_guid) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x24005d9d, __VMLINUX_SYMBOL_STR(remap_pfn_range) },
	{ 0x801678, __VMLINUX_SYMBOL_STR(flush_scheduled_work) },
	{ 0xc1955a3f, __VMLINUX_SYMBOL_STR(ib_sa_register_client) },
	{ 0xedc03953, __VMLINUX_SYMBOL_STR(iounmap) },
	{ 0xb40fe947, __VMLINUX_SYMBOL_STR(ib_modify_qp_is_ok) },
	{ 0x52b1c573, __VMLINUX_SYMBOL_STR(sysfs_remove_file) },
	{ 0x5818125b, __VMLINUX_SYMBOL_STR(mlx4_fmr_enable) },
	{ 0x870bf928, __VMLINUX_SYMBOL_STR(radix_tree_lookup) },
	{ 0xca9360b5, __VMLINUX_SYMBOL_STR(rb_next) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x2ed6e386, __VMLINUX_SYMBOL_STR(ib_ud_header_pack) },
	{ 0x87dcd430, __VMLINUX_SYMBOL_STR(ib_create_cq) },
	{ 0x848932e8, __VMLINUX_SYMBOL_STR(mlx4_multicast_detach) },
	{ 0xb8c43546, __VMLINUX_SYMBOL_STR(ib_query_ah) },
	{ 0x922b93c9, __VMLINUX_SYMBOL_STR(mlx4_pd_alloc) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x53c895fe, __VMLINUX_SYMBOL_STR(mlx4_gen_slaves_port_mgt_ev) },
	{ 0x4b06d2e7, __VMLINUX_SYMBOL_STR(complete) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x32f5a49d, __VMLINUX_SYMBOL_STR(mlx4_write_mtt) },
	{ 0xcf74c42a, __VMLINUX_SYMBOL_STR(mlx4_unregister_interface) },
	{ 0xf26422d, __VMLINUX_SYMBOL_STR(mlx4_fmr_unmap) },
	{ 0xb1b42470, __VMLINUX_SYMBOL_STR(idr_init) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0x6e720ff2, __VMLINUX_SYMBOL_STR(rtnl_unlock) },
	{ 0x2752d211, __VMLINUX_SYMBOL_STR(ib_umem_release) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0xc630208, __VMLINUX_SYMBOL_STR(mlx4_buf_write_mtt) },
	{ 0xe90070bb, __VMLINUX_SYMBOL_STR(mlx4_find_cached_vlan) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mlx4_core,ib_core,ib_mad,ib_sa";


MODULE_INFO(srcversion, "F45B0A960D1C3CEAE0ED164");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xbd8fa773, __VMLINUX_SYMBOL_STR(usb_stor_bulk_transfer_buf) },
	{ 0xc8ba7611, __VMLINUX_SYMBOL_STR(usb_stor_post_reset) },
	{ 0x28dfc342, __VMLINUX_SYMBOL_STR(usb_stor_control_msg) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xe21d5ea8, __VMLINUX_SYMBOL_STR(usb_stor_disconnect) },
	{ 0xdf01d54, __VMLINUX_SYMBOL_STR(usb_deregister) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x802fe80a, __VMLINUX_SYMBOL_STR(usb_stor_probe2) },
	{ 0x7ba045c7, __VMLINUX_SYMBOL_STR(usb_stor_pre_reset) },
	{ 0xa861e116, __VMLINUX_SYMBOL_STR(usb_stor_reset_resume) },
	{ 0x8236dd51, __VMLINUX_SYMBOL_STR(usb_stor_suspend) },
	{ 0xefd625d5, __VMLINUX_SYMBOL_STR(usb_stor_probe1) },
	{ 0x69c3285f, __VMLINUX_SYMBOL_STR(usb_stor_resume) },
	{ 0xb3d01079, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0xcf0438d1, __VMLINUX_SYMBOL_STR(usb_stor_bulk_srb) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=usb-storage";

MODULE_ALIAS("usb:v07ABpFC01d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "B29F1BB20C3EBD002A92940");

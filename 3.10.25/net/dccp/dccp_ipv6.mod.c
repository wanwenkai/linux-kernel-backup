#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xf3017e21, __VMLINUX_SYMBOL_STR(dccp_statistics) },
	{ 0x349de413, __VMLINUX_SYMBOL_STR(dccp_init_sock) },
	{ 0x399b261a, __VMLINUX_SYMBOL_STR(__kfree_skb) },
	{ 0x62ef850e, __VMLINUX_SYMBOL_STR(dccp_v4_do_rcv) },
	{ 0x615bcc6b, __VMLINUX_SYMBOL_STR(inet6_csk_update_pmtu) },
	{ 0xf0044f7d, __VMLINUX_SYMBOL_STR(sk_filter) },
	{ 0x8588e086, __VMLINUX_SYMBOL_STR(dccp_set_state) },
	{ 0x9449e476, __VMLINUX_SYMBOL_STR(inet6_csk_bind_conflict) },
	{ 0x29e7ffe3, __VMLINUX_SYMBOL_STR(ipv6_getsockopt) },
	{ 0xaf25f65, __VMLINUX_SYMBOL_STR(inet_unhash) },
	{ 0x9f554caa, __VMLINUX_SYMBOL_STR(inet_twsk_put) },
	{ 0xb8a409ea, __VMLINUX_SYMBOL_STR(dccp_hashinfo) },
	{ 0x68dcfdc1, __VMLINUX_SYMBOL_STR(inet6_add_protocol) },
	{ 0x5dbb1bee, __VMLINUX_SYMBOL_STR(inet6_lookup_listener) },
	{ 0x82680ebf, __VMLINUX_SYMBOL_STR(dccp_sendmsg) },
	{ 0xe27ec86a, __VMLINUX_SYMBOL_STR(ipv6_opt_accepted) },
	{ 0xabc6e9ad, __VMLINUX_SYMBOL_STR(compat_ipv6_getsockopt) },
	{ 0xcfcd57bd, __VMLINUX_SYMBOL_STR(sk_setup_caps) },
	{ 0xbf9c1f3a, __VMLINUX_SYMBOL_STR(dccp_ctl_make_reset) },
	{ 0x1b7d3989, __VMLINUX_SYMBOL_STR(dccp_destroy_sock) },
	{ 0xb18a407a, __VMLINUX_SYMBOL_STR(dccp_make_response) },
	{ 0x56121368, __VMLINUX_SYMBOL_STR(dst_release) },
	{ 0xc1b4c309, __VMLINUX_SYMBOL_STR(skb_clone) },
	{ 0xba764bcc, __VMLINUX_SYMBOL_STR(dccp_rcv_established) },
	{ 0x88d140b0, __VMLINUX_SYMBOL_STR(inet6_csk_search_req) },
	{ 0xd78e6f89, __VMLINUX_SYMBOL_STR(security_skb_classify_flow) },
	{ 0x774e24ca, __VMLINUX_SYMBOL_STR(dccp_v4_conn_request) },
	{ 0x86498a30, __VMLINUX_SYMBOL_STR(inet_csk_prepare_forced_close) },
	{ 0x76e54d3e, __VMLINUX_SYMBOL_STR(dccp_create_openreq_child) },
	{ 0xdb1a2323, __VMLINUX_SYMBOL_STR(ipv6_dup_options) },
	{ 0x304282e2, __VMLINUX_SYMBOL_STR(dccp_check_req) },
	{ 0x8ec4e5c2, __VMLINUX_SYMBOL_STR(inet_sendmsg) },
	{ 0x3d8d6283, __VMLINUX_SYMBOL_STR(inet6_release) },
	{ 0xb0d888b, __VMLINUX_SYMBOL_STR(icmpv6_err_convert) },
	{ 0x15b6d574, __VMLINUX_SYMBOL_STR(inet6_register_protosw) },
	{ 0xefd0efea, __VMLINUX_SYMBOL_STR(dccp_recvmsg) },
	{ 0x3e2158db, __VMLINUX_SYMBOL_STR(ip6_xmit) },
	{ 0xe31a769f, __VMLINUX_SYMBOL_STR(inet_sk_rebuild_header) },
	{ 0xddbb9980, __VMLINUX_SYMBOL_STR(inet6_getname) },
	{ 0x27447b34, __VMLINUX_SYMBOL_STR(sock_rfree) },
	{ 0xc8702faa, __VMLINUX_SYMBOL_STR(inet6_csk_xmit) },
	{ 0x558b7ff2, __VMLINUX_SYMBOL_STR(__inet6_hash) },
	{ 0xf87108b6, __VMLINUX_SYMBOL_STR(dccp_ioctl) },
	{ 0x66bbdc53, __VMLINUX_SYMBOL_STR(compat_sock_common_setsockopt) },
	{ 0x5b7e9607, __VMLINUX_SYMBOL_STR(dccp_setsockopt) },
	{ 0xacd1b906, __VMLINUX_SYMBOL_STR(sock_common_recvmsg) },
	{ 0x27c33efe, __VMLINUX_SYMBOL_STR(csum_ipv6_magic) },
	{ 0xce0ff185, __VMLINUX_SYMBOL_STR(sock_no_sendpage) },
	{ 0x477b0545, __VMLINUX_SYMBOL_STR(dccp_child_process) },
	{ 0xa2187a22, __VMLINUX_SYMBOL_STR(sock_no_mmap) },
	{ 0x24723a56, __VMLINUX_SYMBOL_STR(compat_sock_common_getsockopt) },
	{ 0x7b0d0e2d, __VMLINUX_SYMBOL_STR(sock_no_socketpair) },
	{ 0x86bd8e8a, __VMLINUX_SYMBOL_STR(skb_checksum) },
	{ 0x53635565, __VMLINUX_SYMBOL_STR(ip6_dst_lookup_flow) },
	{ 0xaef6cd12, __VMLINUX_SYMBOL_STR(unregister_pernet_subsys) },
	{ 0x66edb96d, __VMLINUX_SYMBOL_STR(__inet6_lookup_established) },
	{ 0x1e8216ad, __VMLINUX_SYMBOL_STR(dccp_v4_request_recv_sock) },
	{ 0xa4749798, __VMLINUX_SYMBOL_STR(dccp_parse_options) },
	{ 0x81864531, __VMLINUX_SYMBOL_STR(compat_dccp_setsockopt) },
	{ 0x3fc624d5, __VMLINUX_SYMBOL_STR(dccp_getsockopt) },
	{ 0x3c88d1b3, __VMLINUX_SYMBOL_STR(inet6_csk_reqsk_queue_hash_add) },
	{ 0x2deda4a1, __VMLINUX_SYMBOL_STR(security_sk_classify_flow) },
	{ 0x4897eb1a, __VMLINUX_SYMBOL_STR(inet6_ioctl) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xd9086320, __VMLINUX_SYMBOL_STR(inet_csk_delete_keepalive_timer) },
	{ 0xebd64ac4, __VMLINUX_SYMBOL_STR(fl6_sock_lookup) },
	{ 0xd8525ea7, __VMLINUX_SYMBOL_STR(fl6_update_dst) },
	{ 0xc9409b91, __VMLINUX_SYMBOL_STR(kmem_cache_free) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x13a1a61a, __VMLINUX_SYMBOL_STR(_raw_write_lock) },
	{ 0x11df6fd, __VMLINUX_SYMBOL_STR(dccp_reqsk_init) },
	{ 0x353acc02, __VMLINUX_SYMBOL_STR(dccp_close) },
	{ 0x9025f7fc, __VMLINUX_SYMBOL_STR(sk_free) },
	{ 0xdf18a5f, __VMLINUX_SYMBOL_STR(inet_shutdown) },
	{ 0xe3691b9d, __VMLINUX_SYMBOL_STR(dccp_orphan_count) },
	{ 0xdc373912, __VMLINUX_SYMBOL_STR(dccp_sync_mss) },
	{ 0xa20ce1b8, __VMLINUX_SYMBOL_STR(net_msg_warn) },
	{ 0xfcbe8ca8, __VMLINUX_SYMBOL_STR(inet6_hash_connect) },
	{ 0x3f037795, __VMLINUX_SYMBOL_STR(inet_accept) },
	{ 0xa8253f85, __VMLINUX_SYMBOL_STR(inet_csk_get_port) },
	{ 0x269c9e35, __VMLINUX_SYMBOL_STR(dccp_death_row) },
	{ 0x84151dc6, __VMLINUX_SYMBOL_STR(inet_ctl_sock_create) },
	{ 0x563075a9, __VMLINUX_SYMBOL_STR(dccp_poll) },
	{ 0x70b1621f, __VMLINUX_SYMBOL_STR(inet_hash) },
	{ 0x3ff62317, __VMLINUX_SYMBOL_STR(local_bh_disable) },
	{ 0x7654dafb, __VMLINUX_SYMBOL_STR(proto_register) },
	{ 0x48d41f2, __VMLINUX_SYMBOL_STR(dccp_disconnect) },
	{ 0x51bc3a46, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x51a59413, __VMLINUX_SYMBOL_STR(__sk_dst_check) },
	{ 0x2a9dbeb8, __VMLINUX_SYMBOL_STR(dccp_done) },
	{ 0x250cc09, __VMLINUX_SYMBOL_STR(sk_release_kernel) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x2166ff5, __VMLINUX_SYMBOL_STR(dccp_v4_send_check) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0xeefce16c, __VMLINUX_SYMBOL_STR(proto_unregister) },
	{ 0xc13c8ead, __VMLINUX_SYMBOL_STR(inet_stream_connect) },
	{ 0x772fa86d, __VMLINUX_SYMBOL_STR(dccp_invalid_packet) },
	{ 0x799aca4, __VMLINUX_SYMBOL_STR(local_bh_enable) },
	{ 0x9b4181a3, __VMLINUX_SYMBOL_STR(inet6_del_protocol) },
	{ 0xad4fe501, __VMLINUX_SYMBOL_STR(register_pernet_subsys) },
	{ 0xdb3a1d53, __VMLINUX_SYMBOL_STR(inet6_lookup) },
	{ 0x4cdd391d, __VMLINUX_SYMBOL_STR(dccp_feat_list_purge) },
	{ 0x23bf7b9d, __VMLINUX_SYMBOL_STR(dccp_syn_ack_timeout) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x11abb17c, __VMLINUX_SYMBOL_STR(sk_receive_skb) },
	{ 0xa5844b97, __VMLINUX_SYMBOL_STR(compat_dccp_getsockopt) },
	{ 0x91f8be2c, __VMLINUX_SYMBOL_STR(ipv6_setsockopt) },
	{ 0xf6ebc03b, __VMLINUX_SYMBOL_STR(net_ratelimit) },
	{ 0xe5fa471b, __VMLINUX_SYMBOL_STR(sock_common_setsockopt) },
	{ 0x6dc6057e, __VMLINUX_SYMBOL_STR(inet6_sk_rebuild_header) },
	{ 0x5401e02, __VMLINUX_SYMBOL_STR(__xfrm_policy_check) },
	{ 0xae729e59, __VMLINUX_SYMBOL_STR(security_req_classify_flow) },
	{ 0xe1761617, __VMLINUX_SYMBOL_STR(security_inet_conn_request) },
	{ 0xb266917b, __VMLINUX_SYMBOL_STR(inet_dccp_listen) },
	{ 0xad79435d, __VMLINUX_SYMBOL_STR(__inet_inherit_port) },
	{ 0xa32fd35d, __VMLINUX_SYMBOL_STR(sock_common_getsockopt) },
	{ 0xe200f872, __VMLINUX_SYMBOL_STR(secure_dccpv6_sequence_number) },
	{ 0x7002c3ed, __VMLINUX_SYMBOL_STR(inet6_unregister_protosw) },
	{ 0x929ff41b, __VMLINUX_SYMBOL_STR(inet_csk_accept) },
	{ 0x3292a80d, __VMLINUX_SYMBOL_STR(inet6_bind) },
	{ 0x95e5c714, __VMLINUX_SYMBOL_STR(dccp_reqsk_send_ack) },
	{ 0x6508a516, __VMLINUX_SYMBOL_STR(dccp_rcv_state_process) },
	{ 0x33d53d62, __VMLINUX_SYMBOL_STR(consume_skb) },
	{ 0x97ee6d68, __VMLINUX_SYMBOL_STR(dccp_connect) },
	{ 0xb92fb5c3, __VMLINUX_SYMBOL_STR(dccp_shutdown) },
	{ 0x71a3f3bb, __VMLINUX_SYMBOL_STR(ip_queue_xmit) },
	{ 0xa914b8c7, __VMLINUX_SYMBOL_STR(dccp_v4_connect) },
	{ 0xd542439, __VMLINUX_SYMBOL_STR(__ipv6_addr_type) },
	{ 0xa0cc3195, __VMLINUX_SYMBOL_STR(inet6_csk_addr2sockaddr) },
	{ 0xebb2298a, __VMLINUX_SYMBOL_STR(compat_ipv6_setsockopt) },
	{ 0xc42e2ad9, __VMLINUX_SYMBOL_STR(inet6_destroy_sock) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=dccp,dccp_ipv4,ipv6";


MODULE_INFO(srcversion, "3370E40C47C5EC1AEB684C1");

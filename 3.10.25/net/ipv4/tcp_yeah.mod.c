#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x5f3bd1b9, __VMLINUX_SYMBOL_STR(tcp_vegas_get_info) },
	{ 0x8e93050d, __VMLINUX_SYMBOL_STR(tcp_vegas_cwnd_event) },
	{ 0x81622768, __VMLINUX_SYMBOL_STR(tcp_vegas_state) },
	{ 0xbfe880c0, __VMLINUX_SYMBOL_STR(tcp_reno_min_cwnd) },
	{ 0xdb65719e, __VMLINUX_SYMBOL_STR(tcp_register_congestion_control) },
	{ 0xb4d41aed, __VMLINUX_SYMBOL_STR(tcp_vegas_init) },
	{ 0xe37d5f6e, __VMLINUX_SYMBOL_STR(tcp_cong_avoid_ai) },
	{ 0xc93a0456, __VMLINUX_SYMBOL_STR(tcp_slow_start) },
	{ 0xaf965fa7, __VMLINUX_SYMBOL_STR(tcp_is_cwnd_limited) },
	{ 0x4f6ac607, __VMLINUX_SYMBOL_STR(tcp_vegas_pkts_acked) },
	{ 0x7884831e, __VMLINUX_SYMBOL_STR(tcp_unregister_congestion_control) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=tcp_vegas";


MODULE_INFO(srcversion, "5614C869E58C5A944B682A4");

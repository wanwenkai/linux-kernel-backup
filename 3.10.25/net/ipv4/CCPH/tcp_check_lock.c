/*
 * tcpchklock - Check whether the sock is locked or not when 
 * using tcp_v4_do_rcv()=>...=>tcp_cong_avoid() to process a incoming pkt.
 *
 * Note: Before using this module, 
 *       you should first change the default congestion algorithm to be reno.
 *
 * Copyright (C) 2014, Perth Charles <zhongbincharles@gmail.com>
 */


/*
 * Test result:
 *    tcp_cong_avoid() is always called without locking the "struct sock".
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/socket.h>
#include <linux/tcp.h>
#include <linux/kprobes.h>
#include <linux/module.h>

#include <net/tcp.h>
#include <net/sock.h>

MODULE_AUTHOR("Perth Charles <zhongbincharles@gmail.com>");
MODULE_DESCRIPTION("Checking sock_owned_by_user() when calling tcp_reno_cong_avoid()");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.3");

/*
 * Hook inserted to be called before tcp_cong_avoid.
 */
static int jtcp_reno(struct sock *sk, u32 ack, u32 in_flight)
{

    static u32 cnt = 0;
    if ((cnt++) % 100 == 0) {
         printk(KERN_EMERG "tcp_reno_cong_avoid() is called when sock_owned_by_user(sk) = %d\n", sock_owned_by_user(sk));
    }

    jprobe_return();
    return 0;
}

static struct jprobe jtcp_cong = {
    .kp = {
        .symbol_name = "tcp_reno_cong_avoid",
    },
    .entry = jtcp_reno,
};

static __init int tcpchklock_init(void)
{
    int ret = -ENOMEM;

    ret = register_jprobe(&jtcp_cong);
    if (ret) {
        return ret;
    }

    pr_info("tcpchk_lock registered\n");
    return 0;
}
module_init(tcpchklock_init);

static __exit void tcpchklock_exit(void)
{
    unregister_jprobe(&jtcp_cong);
    pr_info("tcpchklock unregistered\n");
}
module_exit(tcpchklock_exit);

/*
 * TCP dummy congestion control
 *   Cwnd is never tuned in congestion avoidance phase.
 * 
 * Copyright (C) 2014, Perth Charles <zhongbincharles@gmail.com>
 */

#include <linux/module.h>
#include <net/tcp.h>

static void tcp_dummy_cong_avoid(struct sock *sk, u32 ack, u32 in_flight)
{
    /* Dummy :) */
}

static struct tcp_congestion_ops tcp_dummy __read_mostly = {
	.ssthresh	= tcp_reno_ssthresh,
	.cong_avoid	= tcp_dummy_cong_avoid,
	.min_cwnd	= tcp_reno_min_cwnd,

	.owner		= THIS_MODULE,
	.name		= "dummy",
};

static int __init tcp_dummy_register(void)
{
	tcp_register_congestion_control(&tcp_dummy);
	return 0;
}

static void __exit tcp_dummy_unregister(void)
{
	tcp_unregister_congestion_control(&tcp_dummy);
}

module_init(tcp_dummy_register);
module_exit(tcp_dummy_unregister);

MODULE_AUTHOR("Perth Charles");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("TCP Dummy");

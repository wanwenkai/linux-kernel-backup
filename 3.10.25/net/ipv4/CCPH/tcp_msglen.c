/*
 * tcpsendlen - Collect the information of the TCP sending length with kprobes.
 *     /proc/net/tcpmsglen for tcp_sendmsg()
 *     /proc/net/tcppagelen for tcp_sendpage()
 *
 * Copyright (C) 2014, Perth Charles <zhongbincharles@gmail.com>
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/kprobes.h>
#include <linux/socket.h>
#include <linux/tcp.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/module.h>
#include <linux/ktime.h>
#include <linux/time.h>
#include <linux/aio.h>
#include <linux/types.h>
#include <linux/compiler.h>
#include <net/net_namespace.h>

#include <net/tcp.h>
#include <net/sock.h>
#include <net/tcp_states.h>

#include "tcp_flowmsg.h"

MODULE_AUTHOR("Perth Charles <zhongbincharles@gmail.com>");
MODULE_DESCRIPTION("TCP sending length information collector");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.3");

/* only control some specific flows, e.g., HTTP with port=80 */
static int port __read_mostly = 0;
MODULE_PARM_DESC(port, "Port to match (0=all)");
module_param(port, int, 0);

static unsigned int bufsize __read_mostly = 4096;
MODULE_PARM_DESC(bufsize, "Log buffer size in flow message");
module_param(bufsize, uint, 0);

static const char procname[] = "tcpmsglen";

static struct ccph_buf tcp_msglen;

/*
 * Hook inserted to be called before each sendmsg().
 *
 * Note: 
 *   If socket is blocking I/O, then length = sum-of-each-iovec;
 *   else length = min(sum-of-each-iovec, free-in-sndbuf).
 * TODO: free space in sndbuf is not exactly the space to store application data, 
 *       a more accurate method need to be implement.
 */
static int jtcp_sendmsg(struct kiocb *iocb, struct sock *sk, struct msghdr *msg,
                        size_t size)
{
    int flags, iovlen;
    int len = 0;
    struct iovec *iov;
    const struct inet_sock *inet = inet_sk(sk);

    if (port == 0 || ntohs(inet->inet_dport) == port ||
        ntohs(inet->inet_sport) == port) {

        flags = msg->msg_flags;
        if (unlikely(flags & MSG_FASTOPEN)) {
            pr_info("tcp_sendmsg is sending with flag MSG_FASTOPEN.\n");
        }
    
        iovlen = msg->msg_iovlen;
        iov = msg->msg_iov;

        while (--iovlen >= 0) {
            len += iov->iov_len;
            iov++;
        }

        /* TODO: Delete this debug info after some tests. */
        if (len != size) {
            pr_info("tcp_sendmsg: size is not equal to the len requested.\n");
        }

        /* Nonblocking I/O */
        if ((flags & MSG_DONTWAIT) && sk_stream_memory_free(sk)) {
            len = sk_stream_wspace(sk) < len ? sk_stream_wspace(sk) : len;
        }

        /* add a new message into tcp_msglen.buf */
        spin_lock(&tcp_msglen.lock);
        
        if (ccph_buf_avail(&tcp_msglen, bufsize) > 1) {
             struct flow_msg *cur = ccph_buf_head(&tcp_msglen);

            cur->tstamp    = ktime_get();
            cur->sock_net  = (u64)sock_net(sk);
            cur->dev_index = (u64)sk->sk_bound_dev_if;
            cur->addr_pair = (u64)inet->inet_addrpair;
            cur->port_pair = (u32)inet->inet_portpair;
            /* doesn't differentiate blocking and nonblocking here, yet */
            cur->flag      = SENDMSG;  
            cur->value     = len;

            ccph_buf_add(&tcp_msglen, bufsize);
        } else {
            pr_info("jtcp_sendmsg: buf fills\n");
        }
        
        spin_unlock(&tcp_msglen.lock);
        wake_up(&tcp_msglen.wait);
    }

    jprobe_return();
    return 0;
}

static struct jprobe jtcp_msglen = {
    .kp = {
        .symbol_name = "tcp_sendmsg",
    },
    .entry = jtcp_sendmsg,
};

static int tcpmsglen_open(struct inode *inode, struct file *file)
{
    ccph_buf_init(&tcp_msglen);

    return 0;
}

static ssize_t tcpmsglen_read(struct file *file, char __user *buf,
                              size_t len, loff_t *ppos)
{
    int error = 0;
    size_t cnt = 0;

    if (!buf || len < sizeof(struct flow_msg)) {
        return -EINVAL;
    }

    while (cnt < len) {
        char tbuf[256];
        int width;
        error = wait_event_interruptible(tcp_msglen.wait, ccph_buf_used(&tcp_msglen, bufsize) > 0);
        if (error) {
            break;
        }

        spin_lock_bh(&tcp_msglen.lock);
        if (tcp_msglen.head == tcp_msglen.tail) {
            /* multiple readers race ? */
            spin_unlock_bh(&tcp_msglen.lock);
            continue;
        }

        width = flowmsg_readone(&tcp_msglen, tbuf, sizeof(tbuf));
        if (cnt + width < len) {
            ccph_buf_del(&tcp_msglen, bufsize);
        }
        spin_unlock_bh(&tcp_msglen.lock);

        if (cnt + width >= len) {
            break;
        }

        if (copy_to_user(buf + cnt, tbuf, width)) {
            return -EFAULT;
        }
        cnt += width;
    }

    return cnt == 0 ? error : cnt;
}

static const struct file_operations tcpmsglen_fops = {
    .owner  = THIS_MODULE,
    .open   = tcpmsglen_open,
    .read   = tcpmsglen_read,
    .llseek = noop_llseek,
};

static __init int tcpmsglen_init(void)
{
    int ret = -ENOMEM;

    init_waitqueue_head(&tcp_msglen.wait);
    spin_lock_init(&tcp_msglen.lock);

    if (bufsize == 0) {
        return -EINVAL;
    }

    bufsize = roundup_pow_of_two(bufsize);
    tcp_msglen.buf = kcalloc(bufsize, sizeof(struct flow_msg), GFP_KERNEL);
    if (!tcp_msglen.buf) {
        goto err0;
    }

    if (!proc_create(procname, S_IRUSR, init_net.proc_net, &tcpmsglen_fops)) {
        goto err1;
    }
    
    ret = register_jprobe(&jtcp_msglen);
    if (ret) {
        goto err1;
    }

    pr_info("tcp_msglen registered (port=%d) bufsize=%u\n", port, bufsize);
    return 0;

err1:
    remove_proc_entry(procname, init_net.proc_net);

err0:
    kfree(tcp_msglen.buf);
    return ret;
}
module_init(tcpmsglen_init);

static __exit void tcpmsglen_exit(void)
{
    remove_proc_entry(procname, init_net.proc_net);
    unregister_jprobe(&jtcp_msglen);
    kfree(tcp_msglen.buf);
    pr_info("tcp_msglen unregistered\n");
}
module_exit(tcpmsglen_exit);

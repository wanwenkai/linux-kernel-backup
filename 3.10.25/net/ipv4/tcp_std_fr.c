/*
 * Fast recovery in RFC standards
 * 
 * This is based on the retransmission algorithm described in RFC3517
 */

#include <linux/module.h>
#include <net/tcp.h>


static void tcp_stdfr_cwnd_reduction(struct sock *sk, int newly_acked_sacked,
                                  int fast_rexmit)
{
    // For standard fast recovery algorithm, there is no need to change cwnd in CWR mode
}

void tcp_stdfr_init_cwnd_reduction(struct sock *sk, const bool set_ssthresh)
{
    struct tcp_sock *tp = tcp_sk(sk);

    tp->high_seq = tp->snd_nxt;
    tp->tlp_high_seq = 0;
    tp->snd_cwnd_cnt = 0;
    tp->prior_cwnd = tp->snd_cwnd;
    tp->prr_delivered = 0;
    tp->prr_out = 0;

    // standard fast recovery: set cwnd = ssthresh = FlightSize/2
    tp->snd_cwnd = tp->snd_cwnd > 4U ? (tp->snd_cwnd >> 1U) : 2U;
    tp->snd_ssthresh = tp->snd_cwnd;

    if (tp->ecn_flags & TCP_ECN_OK) {
        tp->ecn_flags |= TCP_ECN_QUEUE_CWR;
    }
    //printk(KERN_ALERT "RETRANS_MODULE: init cwnd reduction STD_FR\n");
}

static struct tcp_retrans_ops tcp_std_fr = {
    .name                = "STD_FR",
    .owner               = THIS_MODULE,
    .init_cwnd_reduction = tcp_stdfr_init_cwnd_reduction,
    .cwnd_reduction      = tcp_stdfr_cwnd_reduction,
    .end_cwnd_reduction   = tcp_prr_end_cwnd_reduction,
};

static int __init std_fastrecovery_register(void)
{
    return tcp_register_retransmission_algorithm(&tcp_std_fr);
}

static void __exit std_fastrecovery_unregister(void)
{
    tcp_unregister_retransmission_algorithm(&tcp_std_fr);
}

module_init(std_fastrecovery_register);
module_exit(std_fastrecovery_unregister);

MODULE_AUTHOR("Perth Charles");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Standard Fast Recovery");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x5a78030c, __VMLINUX_SYMBOL_STR(ip_tunnel_get_stats64) },
	{ 0x9e329de, __VMLINUX_SYMBOL_STR(ip_tunnel_change_mtu) },
	{ 0xbc3fe5b8, __VMLINUX_SYMBOL_STR(ip_tunnel_uninit) },
	{ 0x3c235a32, __VMLINUX_SYMBOL_STR(ip_tunnel_dellink) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0x85b63bb9, __VMLINUX_SYMBOL_STR(rtnl_link_register) },
	{ 0x1a70d15b, __VMLINUX_SYMBOL_STR(xfrm4_tunnel_register) },
	{ 0x4a4ef2b9, __VMLINUX_SYMBOL_STR(register_pernet_device) },
	{ 0xcd0db5f1, __VMLINUX_SYMBOL_STR(ip_tunnel_init) },
	{ 0x33d53d62, __VMLINUX_SYMBOL_STR(consume_skb) },
	{ 0xb7e8b1c2, __VMLINUX_SYMBOL_STR(ip_tunnel_xmit) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x5d53fd26, __VMLINUX_SYMBOL_STR(ip_tunnel_ioctl) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x7b334f7e, __VMLINUX_SYMBOL_STR(ip_tunnel_setup) },
	{ 0xc6b8f29d, __VMLINUX_SYMBOL_STR(ip_tunnel_newlink) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xd65431b3, __VMLINUX_SYMBOL_STR(ip_tunnel_changelink) },
	{ 0xfa0f6d09, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0x1a6fb88f, __VMLINUX_SYMBOL_STR(ip_tunnel_rcv) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x5401e02, __VMLINUX_SYMBOL_STR(__xfrm_policy_check) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x9dc9ac81, __VMLINUX_SYMBOL_STR(ipv4_redirect) },
	{ 0xdc234f7f, __VMLINUX_SYMBOL_STR(ipv4_update_pmtu) },
	{ 0x2c841cb9, __VMLINUX_SYMBOL_STR(ip_tunnel_lookup) },
	{ 0x11d4fc19, __VMLINUX_SYMBOL_STR(ip_tunnel_init_net) },
	{ 0xeb8c0977, __VMLINUX_SYMBOL_STR(ip_tunnel_delete_net) },
	{ 0x49f7daaa, __VMLINUX_SYMBOL_STR(unregister_pernet_device) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x23f2fcf0, __VMLINUX_SYMBOL_STR(xfrm4_tunnel_deregister) },
	{ 0x9169abcd, __VMLINUX_SYMBOL_STR(rtnl_link_unregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=ip_tunnel,tunnel4";


MODULE_INFO(srcversion, "863CBE480692EE6073F0C16");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x609f1c7e, __VMLINUX_SYMBOL_STR(synchronize_net) },
	{ 0xa7c3e250, __VMLINUX_SYMBOL_STR(nf_conntrack_in) },
	{ 0x624fd50, __VMLINUX_SYMBOL_STR(__nf_ct_refresh_acct) },
	{ 0xe02819f3, __VMLINUX_SYMBOL_STR(nf_ct_l4proto_pernet_register) },
	{ 0x5a92c21c, __VMLINUX_SYMBOL_STR(nf_ct_get_tuplepr) },
	{ 0x6eb85693, __VMLINUX_SYMBOL_STR(nf_defrag_ipv6_enable) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0x448eac3e, __VMLINUX_SYMBOL_STR(kmemdup) },
	{ 0x54a7bdf5, __VMLINUX_SYMBOL_STR(nf_ct_deliver_cached_events) },
	{ 0xea054b22, __VMLINUX_SYMBOL_STR(nla_policy_len) },
	{ 0xd10788db, __VMLINUX_SYMBOL_STR(nf_log_packet) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0xb9d1f1bc, __VMLINUX_SYMBOL_STR(nf_ct_l4proto_unregister) },
	{ 0xaef6cd12, __VMLINUX_SYMBOL_STR(unregister_pernet_subsys) },
	{ 0xdb586915, __VMLINUX_SYMBOL_STR(nf_ct_invert_tuple) },
	{ 0x33d1a2e1, __VMLINUX_SYMBOL_STR(__nf_conntrack_confirm) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x8bedac1a, __VMLINUX_SYMBOL_STR(nf_ct_l4proto_pernet_unregister) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xfa0f6d09, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0xf39b35d2, __VMLINUX_SYMBOL_STR(nf_ct_l4proto_register) },
	{ 0x54830a14, __VMLINUX_SYMBOL_STR(nf_ct_l3proto_unregister) },
	{ 0xdbeba7fe, __VMLINUX_SYMBOL_STR(nf_unregister_hooks) },
	{ 0xe8b7be4e, __VMLINUX_SYMBOL_STR(ipv6_skip_exthdr) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x6e224a7a, __VMLINUX_SYMBOL_STR(need_conntrack) },
	{ 0x81c1ee8b, __VMLINUX_SYMBOL_STR(nf_unregister_sockopt) },
	{ 0x6307490a, __VMLINUX_SYMBOL_STR(nf_conntrack_l4proto_tcp6) },
	{ 0xca8210d5, __VMLINUX_SYMBOL_STR(nf_ct_l3proto_pernet_register) },
	{ 0xad4fe501, __VMLINUX_SYMBOL_STR(register_pernet_subsys) },
	{ 0x2a18c74, __VMLINUX_SYMBOL_STR(nf_conntrack_destroy) },
	{ 0xf6ebc03b, __VMLINUX_SYMBOL_STR(net_ratelimit) },
	{ 0x8c803fed, __VMLINUX_SYMBOL_STR(nf_conntrack_untracked) },
	{ 0x11c6422c, __VMLINUX_SYMBOL_STR(nf_register_hooks) },
	{ 0x20479200, __VMLINUX_SYMBOL_STR(nf_nat_seq_adjust_hook) },
	{ 0x7628f3c7, __VMLINUX_SYMBOL_STR(this_cpu_off) },
	{ 0x72335d36, __VMLINUX_SYMBOL_STR(nf_ct_l3proto_pernet_unregister) },
	{ 0x6b8402af, __VMLINUX_SYMBOL_STR(nf_register_sockopt) },
	{ 0x465dc539, __VMLINUX_SYMBOL_STR(nf_conntrack_l4proto_udp6) },
	{ 0x98b6dac, __VMLINUX_SYMBOL_STR(proc_dointvec_jiffies) },
	{ 0x418132dc, __VMLINUX_SYMBOL_STR(skb_copy_bits) },
	{ 0xfb690089, __VMLINUX_SYMBOL_STR(nf_ip6_checksum) },
	{ 0xd542439, __VMLINUX_SYMBOL_STR(__ipv6_addr_type) },
	{ 0x4193963, __VMLINUX_SYMBOL_STR(nf_ct_l3proto_register) },
	{ 0xcaa5172f, __VMLINUX_SYMBOL_STR(nf_conntrack_find_get) },
	{ 0xa8de0646, __VMLINUX_SYMBOL_STR(__nf_ct_l4proto_find) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=nf_conntrack,nf_defrag_ipv6,ipv6";


MODULE_INFO(srcversion, "02B9C8C7ED6FF055C2AE943");

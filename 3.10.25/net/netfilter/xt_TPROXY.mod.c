#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x24069915, __VMLINUX_SYMBOL_STR(xt_register_targets) },
	{ 0x6eb85693, __VMLINUX_SYMBOL_STR(nf_defrag_ipv6_enable) },
	{ 0x6b6c3d10, __VMLINUX_SYMBOL_STR(nf_defrag_ipv4_enable) },
	{ 0x315fc4d7, __VMLINUX_SYMBOL_STR(__inet_lookup_listener) },
	{ 0x309e39d2, __VMLINUX_SYMBOL_STR(udp4_lib_lookup) },
	{ 0xac8fa7e7, __VMLINUX_SYMBOL_STR(__inet_lookup_established) },
	{ 0x9dd42b1b, __VMLINUX_SYMBOL_STR(nf_tproxy_assign_sock) },
	{ 0xd14466e, __VMLINUX_SYMBOL_STR(inet_twsk_deschedule) },
	{ 0x22f7fce0, __VMLINUX_SYMBOL_STR(tcp_death_row) },
	{ 0x5dbb1bee, __VMLINUX_SYMBOL_STR(inet6_lookup_listener) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x6d36c306, __VMLINUX_SYMBOL_STR(udp6_lib_lookup) },
	{ 0x66edb96d, __VMLINUX_SYMBOL_STR(__inet6_lookup_established) },
	{ 0x7ec9a70a, __VMLINUX_SYMBOL_STR(tcp_hashinfo) },
	{ 0x418132dc, __VMLINUX_SYMBOL_STR(skb_copy_bits) },
	{ 0xdf126b62, __VMLINUX_SYMBOL_STR(ipv6_find_hdr) },
	{ 0x9f554caa, __VMLINUX_SYMBOL_STR(inet_twsk_put) },
	{ 0x9025f7fc, __VMLINUX_SYMBOL_STR(sk_free) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xcd145fee, __VMLINUX_SYMBOL_STR(xt_unregister_targets) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=x_tables,nf_defrag_ipv6,nf_defrag_ipv4,nf_tproxy_core,ipv6";


MODULE_INFO(srcversion, "E89BDBAEE2D5699D564DF9F");

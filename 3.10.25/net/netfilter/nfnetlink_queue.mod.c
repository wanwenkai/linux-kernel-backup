#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x92eee323, __VMLINUX_SYMBOL_STR(__skb_gso_segment) },
	{ 0x60a13e90, __VMLINUX_SYMBOL_STR(rcu_barrier) },
	{ 0xfa599bb2, __VMLINUX_SYMBOL_STR(netlink_register_notifier) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0xd2da1048, __VMLINUX_SYMBOL_STR(register_netdevice_notifier) },
	{ 0x90ce81d5, __VMLINUX_SYMBOL_STR(remove_proc_entry) },
	{ 0x448eac3e, __VMLINUX_SYMBOL_STR(kmemdup) },
	{ 0xfc42108a, __VMLINUX_SYMBOL_STR(nf_reinject) },
	{ 0xdf54a8f7, __VMLINUX_SYMBOL_STR(netlink_unregister_notifier) },
	{ 0x9c39470d, __VMLINUX_SYMBOL_STR(seq_read) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x9d0d6206, __VMLINUX_SYMBOL_STR(unregister_netdevice_notifier) },
	{ 0x794ad8c1, __VMLINUX_SYMBOL_STR(nfnetlink_subsys_unregister) },
	{ 0xaef6cd12, __VMLINUX_SYMBOL_STR(unregister_pernet_subsys) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0xfa0f6d09, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0xd7ea7094, __VMLINUX_SYMBOL_STR(nf_unregister_queue_handler) },
	{ 0x9f230289, __VMLINUX_SYMBOL_STR(nfnetlink_unicast) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x4c3db7d4, __VMLINUX_SYMBOL_STR(seq_release_net) },
	{ 0x88dc702c, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0x8d48a0fe, __VMLINUX_SYMBOL_STR(__get_page_tail) },
	{ 0xc1437d38, __VMLINUX_SYMBOL_STR(module_put) },
	{ 0x86a62306, __VMLINUX_SYMBOL_STR(skb_copy_expand) },
	{ 0xfe19a1d1, __VMLINUX_SYMBOL_STR(__nla_reserve) },
	{ 0x4d2feec6, __VMLINUX_SYMBOL_STR(nf_register_queue_handler) },
	{ 0xf2dfc005, __VMLINUX_SYMBOL_STR(nf_queue_entry_get_refs) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xadf4c0c3, __VMLINUX_SYMBOL_STR(skb_checksum_help) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x595bb37c, __VMLINUX_SYMBOL_STR(nfnetlink_alloc_skb) },
	{ 0xad4fe501, __VMLINUX_SYMBOL_STR(register_pernet_subsys) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xb116afdd, __VMLINUX_SYMBOL_STR(nf_queue_entry_release_refs) },
	{ 0xf6ebc03b, __VMLINUX_SYMBOL_STR(net_ratelimit) },
	{ 0x7f76e4a5, __VMLINUX_SYMBOL_STR(proc_create_data) },
	{ 0xd2bcdb9a, __VMLINUX_SYMBOL_STR(seq_lseek) },
	{ 0xc248acde, __VMLINUX_SYMBOL_STR(nfnetlink_subsys_register) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x33bf8734, __VMLINUX_SYMBOL_STR(seq_open_net) },
	{ 0xa75312bc, __VMLINUX_SYMBOL_STR(call_rcu_sched) },
	{ 0xa5b2bbdc, __VMLINUX_SYMBOL_STR(___pskb_trim) },
	{ 0x1f65a5c5, __VMLINUX_SYMBOL_STR(skb_make_writable) },
	{ 0x45f4292a, __VMLINUX_SYMBOL_STR(skb_put) },
	{ 0x917984da, __VMLINUX_SYMBOL_STR(__nlmsg_put) },
	{ 0x418132dc, __VMLINUX_SYMBOL_STR(skb_copy_bits) },
	{ 0xedccbde5, __VMLINUX_SYMBOL_STR(try_module_get) },
	{ 0x4cdb3178, __VMLINUX_SYMBOL_STR(ns_to_timeval) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=nfnetlink";


MODULE_INFO(srcversion, "9CB7628A0932BD485740C4F");

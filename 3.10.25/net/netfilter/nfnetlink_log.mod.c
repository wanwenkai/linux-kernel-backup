#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xfd37fe3, __VMLINUX_SYMBOL_STR(nf_log_unregister) },
	{ 0xcca27eeb, __VMLINUX_SYMBOL_STR(del_timer) },
	{ 0xfa599bb2, __VMLINUX_SYMBOL_STR(netlink_register_notifier) },
	{ 0x841b4d92, __VMLINUX_SYMBOL_STR(nf_log_bind_pf) },
	{ 0x79aa04a2, __VMLINUX_SYMBOL_STR(get_random_bytes) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0x90ce81d5, __VMLINUX_SYMBOL_STR(remove_proc_entry) },
	{ 0xdf54a8f7, __VMLINUX_SYMBOL_STR(netlink_unregister_notifier) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x4ca7ae56, __VMLINUX_SYMBOL_STR(__put_net) },
	{ 0x9c39470d, __VMLINUX_SYMBOL_STR(seq_read) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x794ad8c1, __VMLINUX_SYMBOL_STR(nfnetlink_subsys_unregister) },
	{ 0xaef6cd12, __VMLINUX_SYMBOL_STR(unregister_pernet_subsys) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xfa0f6d09, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0x9f230289, __VMLINUX_SYMBOL_STR(nfnetlink_unicast) },
	{ 0x4c3db7d4, __VMLINUX_SYMBOL_STR(seq_release_net) },
	{ 0xd9605d4c, __VMLINUX_SYMBOL_STR(add_timer) },
	{ 0x88dc702c, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0xc1437d38, __VMLINUX_SYMBOL_STR(module_put) },
	{ 0x3ff62317, __VMLINUX_SYMBOL_STR(local_bh_disable) },
	{ 0x7171121c, __VMLINUX_SYMBOL_STR(overflowgid) },
	{ 0xaa841fa3, __VMLINUX_SYMBOL_STR(nf_log_unbind_pf) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x799aca4, __VMLINUX_SYMBOL_STR(local_bh_enable) },
	{ 0x595bb37c, __VMLINUX_SYMBOL_STR(nfnetlink_alloc_skb) },
	{ 0xad4fe501, __VMLINUX_SYMBOL_STR(register_pernet_subsys) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x59e2743e, __VMLINUX_SYMBOL_STR(call_rcu_bh) },
	{ 0xf6ebc03b, __VMLINUX_SYMBOL_STR(net_ratelimit) },
	{ 0x7f76e4a5, __VMLINUX_SYMBOL_STR(proc_create_data) },
	{ 0xd2bcdb9a, __VMLINUX_SYMBOL_STR(seq_lseek) },
	{ 0xa5a99b49, __VMLINUX_SYMBOL_STR(_raw_read_lock_bh) },
	{ 0xc248acde, __VMLINUX_SYMBOL_STR(nfnetlink_subsys_register) },
	{ 0xbded85d0, __VMLINUX_SYMBOL_STR(_raw_read_unlock_bh) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x33bf8734, __VMLINUX_SYMBOL_STR(seq_open_net) },
	{ 0x8b618d08, __VMLINUX_SYMBOL_STR(overflowuid) },
	{ 0xca8a9895, __VMLINUX_SYMBOL_STR(nf_log_register) },
	{ 0x45f4292a, __VMLINUX_SYMBOL_STR(skb_put) },
	{ 0x917984da, __VMLINUX_SYMBOL_STR(__nlmsg_put) },
	{ 0x418132dc, __VMLINUX_SYMBOL_STR(skb_copy_bits) },
	{ 0xedccbde5, __VMLINUX_SYMBOL_STR(try_module_get) },
	{ 0x4cdb3178, __VMLINUX_SYMBOL_STR(ns_to_timeval) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=nfnetlink";


MODULE_INFO(srcversion, "EA2F1E6D16FFBE3F091A41E");

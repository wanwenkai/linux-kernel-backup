#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x11c6422c, __VMLINUX_SYMBOL_STR(nf_register_hooks) },
	{ 0xad4fe501, __VMLINUX_SYMBOL_STR(register_pernet_subsys) },
	{ 0x202189a3, __VMLINUX_SYMBOL_STR(ebt_do_table) },
	{ 0xc1dbf5a7, __VMLINUX_SYMBOL_STR(ebt_register_table) },
	{ 0x4b1fc46a, __VMLINUX_SYMBOL_STR(ebt_unregister_table) },
	{ 0xaef6cd12, __VMLINUX_SYMBOL_STR(unregister_pernet_subsys) },
	{ 0xdbeba7fe, __VMLINUX_SYMBOL_STR(nf_unregister_hooks) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=ebtables";


MODULE_INFO(srcversion, "FE9E58F930B3374E4DB9255");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x54554948, __VMLINUX_SYMBOL_STR(kobject_put) },
	{ 0xb294dd6, __VMLINUX_SYMBOL_STR(kmem_cache_destroy) },
	{ 0x31cfd965, __VMLINUX_SYMBOL_STR(simple_pin_fs) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x9b388444, __VMLINUX_SYMBOL_STR(get_zeroed_page) },
	{ 0xf5893abf, __VMLINUX_SYMBOL_STR(up_read) },
	{ 0x619cb7dd, __VMLINUX_SYMBOL_STR(simple_read_from_buffer) },
	{ 0x387b0000, __VMLINUX_SYMBOL_STR(generic_file_llseek) },
	{ 0x188a3dfb, __VMLINUX_SYMBOL_STR(timespec_trunc) },
	{ 0xdd71138, __VMLINUX_SYMBOL_STR(simple_write_end) },
	{ 0xe2ff60a2, __VMLINUX_SYMBOL_STR(simple_release_fs) },
	{ 0x34184afe, __VMLINUX_SYMBOL_STR(current_kernel_time) },
	{ 0x98509e0, __VMLINUX_SYMBOL_STR(generic_delete_inode) },
	{ 0x83dd255b, __VMLINUX_SYMBOL_STR(dput) },
	{ 0xa4e434de, __VMLINUX_SYMBOL_STR(inc_nlink) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x8993b6e3, __VMLINUX_SYMBOL_STR(mount_single) },
	{ 0x8d1d2e94, __VMLINUX_SYMBOL_STR(generic_read_dir) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x57a6ccd0, __VMLINUX_SYMBOL_STR(down_read) },
	{ 0xc10ca2e1, __VMLINUX_SYMBOL_STR(kobject_create_and_add) },
	{ 0x7651ed5c, __VMLINUX_SYMBOL_STR(d_delete) },
	{ 0x1c5c0a, __VMLINUX_SYMBOL_STR(kern_path) },
	{ 0xc9e7ba8, __VMLINUX_SYMBOL_STR(kill_litter_super) },
	{ 0xfa53c933, __VMLINUX_SYMBOL_STR(simple_write_begin) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xf78b7eb4, __VMLINUX_SYMBOL_STR(d_rehash) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x9166fada, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0x31ea0f, __VMLINUX_SYMBOL_STR(bdi_init) },
	{ 0xc9409b91, __VMLINUX_SYMBOL_STR(kmem_cache_free) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xbafa3c61, __VMLINUX_SYMBOL_STR(simple_readpage) },
	{ 0xc1437d38, __VMLINUX_SYMBOL_STR(module_put) },
	{ 0xc6cbbc89, __VMLINUX_SYMBOL_STR(capable) },
	{ 0x51bc3a46, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xfad63108, __VMLINUX_SYMBOL_STR(simple_unlink) },
	{ 0x2e81cc98, __VMLINUX_SYMBOL_STR(d_alloc) },
	{ 0x3c8355e8, __VMLINUX_SYMBOL_STR(simple_setattr) },
	{ 0x93fca811, __VMLINUX_SYMBOL_STR(__get_free_pages) },
	{ 0x3ec16c3b, __VMLINUX_SYMBOL_STR(in_group_p) },
	{ 0xa06f2c63, __VMLINUX_SYMBOL_STR(d_drop) },
	{ 0x6f20960a, __VMLINUX_SYMBOL_STR(full_name_hash) },
	{ 0x49676622, __VMLINUX_SYMBOL_STR(path_put) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x67cc86e8, __VMLINUX_SYMBOL_STR(kmem_cache_create) },
	{ 0x455b1dc2, __VMLINUX_SYMBOL_STR(register_filesystem) },
	{ 0x99195078, __VMLINUX_SYMBOL_STR(vsnprintf) },
	{ 0x4302d0eb, __VMLINUX_SYMBOL_STR(free_pages) },
	{ 0xe953b21f, __VMLINUX_SYMBOL_STR(get_next_ino) },
	{ 0xcc2e8c93, __VMLINUX_SYMBOL_STR(kernel_kobj) },
	{ 0x78342ae0, __VMLINUX_SYMBOL_STR(iput) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x52928a53, __VMLINUX_SYMBOL_STR(iunique) },
	{ 0x41f1b73f, __VMLINUX_SYMBOL_STR(generic_readlink) },
	{ 0x8c12322f, __VMLINUX_SYMBOL_STR(d_make_root) },
	{ 0xff015dd5, __VMLINUX_SYMBOL_STR(simple_statfs) },
	{ 0x73e40495, __VMLINUX_SYMBOL_STR(bdi_destroy) },
	{ 0xa009b3e7, __VMLINUX_SYMBOL_STR(unregister_filesystem) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x92c0167b, __VMLINUX_SYMBOL_STR(new_inode) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0x1bce8f24, __VMLINUX_SYMBOL_STR(d_instantiate) },
	{ 0xedccbde5, __VMLINUX_SYMBOL_STR(try_module_get) },
	{ 0x29812de9, __VMLINUX_SYMBOL_STR(simple_rmdir) },
	{ 0xa4a37877, __VMLINUX_SYMBOL_STR(__d_drop) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "E7E74769F90D2D98E9DAC6F");

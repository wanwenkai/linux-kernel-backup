#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x346ec079, __VMLINUX_SYMBOL_STR(mb_cache_entry_find_next) },
	{ 0xf6134041, __VMLINUX_SYMBOL_STR(blkdev_issue_discard) },
	{ 0xb294dd6, __VMLINUX_SYMBOL_STR(kmem_cache_destroy) },
	{ 0x9e18159, __VMLINUX_SYMBOL_STR(page_zero_new_buffers) },
	{ 0x1671f2ae, __VMLINUX_SYMBOL_STR(dquot_alloc) },
	{ 0xc72c65fe, __VMLINUX_SYMBOL_STR(iget_failed) },
	{ 0xf378a639, __VMLINUX_SYMBOL_STR(dquot_destroy) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x61b7f275, __VMLINUX_SYMBOL_STR(perf_tp_event) },
	{ 0x405c1144, __VMLINUX_SYMBOL_STR(get_seconds) },
	{ 0x7ae34508, __VMLINUX_SYMBOL_STR(drop_nlink) },
	{ 0x6b1b67d3, __VMLINUX_SYMBOL_STR(__bdevname) },
	{ 0x60a86999, __VMLINUX_SYMBOL_STR(sb_min_blocksize) },
	{ 0x840e7d, __VMLINUX_SYMBOL_STR(generic_getxattr) },
	{ 0xf5893abf, __VMLINUX_SYMBOL_STR(up_read) },
	{ 0xdd9cb220, __VMLINUX_SYMBOL_STR(journal_restart) },
	{ 0xbbbdbcbc, __VMLINUX_SYMBOL_STR(__bread) },
	{ 0x4c4fef19, __VMLINUX_SYMBOL_STR(kernel_stack) },
	{ 0x53c05586, __VMLINUX_SYMBOL_STR(journal_abort) },
	{ 0x8357be4b, __VMLINUX_SYMBOL_STR(journal_check_available_features) },
	{ 0xd82c6497, __VMLINUX_SYMBOL_STR(make_bad_inode) },
	{ 0x387b0000, __VMLINUX_SYMBOL_STR(generic_file_llseek) },
	{ 0x99fd1a19, __VMLINUX_SYMBOL_STR(journal_release_buffer) },
	{ 0xf1c550e3, __VMLINUX_SYMBOL_STR(__mark_inode_dirty) },
	{ 0x6f4c812f, __VMLINUX_SYMBOL_STR(__percpu_counter_add) },
	{ 0xe74fed31, __VMLINUX_SYMBOL_STR(dquot_writeback_dquots) },
	{ 0xc91f4061, __VMLINUX_SYMBOL_STR(__set_page_dirty_nobuffers) },
	{ 0x349cba85, __VMLINUX_SYMBOL_STR(strchr) },
	{ 0x45044497, __VMLINUX_SYMBOL_STR(percpu_counter_destroy) },
	{ 0x37fdfe20, __VMLINUX_SYMBOL_STR(__page_symlink) },
	{ 0x923d6b00, __VMLINUX_SYMBOL_STR(generic_file_llseek_size) },
	{ 0x60a13e90, __VMLINUX_SYMBOL_STR(rcu_barrier) },
	{ 0x2659486f, __VMLINUX_SYMBOL_STR(dquot_acquire) },
	{ 0xbb8c35ed, __VMLINUX_SYMBOL_STR(filemap_write_and_wait_range) },
	{ 0xee0efd72, __VMLINUX_SYMBOL_STR(bdev_read_only) },
	{ 0x251cbdee, __VMLINUX_SYMBOL_STR(posix_acl_to_xattr) },
	{ 0x9469482, __VMLINUX_SYMBOL_STR(kfree_call_rcu) },
	{ 0xc5876268, __VMLINUX_SYMBOL_STR(generic_fh_to_parent) },
	{ 0x79aa04a2, __VMLINUX_SYMBOL_STR(get_random_bytes) },
	{ 0x34184afe, __VMLINUX_SYMBOL_STR(current_kernel_time) },
	{ 0xc7e798b4, __VMLINUX_SYMBOL_STR(dquot_file_open) },
	{ 0xd15c4825, __VMLINUX_SYMBOL_STR(block_is_partially_uptodate) },
	{ 0x99f78b48, __VMLINUX_SYMBOL_STR(dquot_mark_dquot_dirty) },
	{ 0x3cc17d46, __VMLINUX_SYMBOL_STR(log_wait_commit) },
	{ 0xd00630ea, __VMLINUX_SYMBOL_STR(seq_puts) },
	{ 0x7ac0237e, __VMLINUX_SYMBOL_STR(is_bad_inode) },
	{ 0x9270b4e2, __VMLINUX_SYMBOL_STR(blkdev_issue_flush) },
	{ 0x91825c21, __VMLINUX_SYMBOL_STR(dquot_quota_on_mount) },
	{ 0xe42e6a1f, __VMLINUX_SYMBOL_STR(journal_start) },
	{ 0x8abda123, __VMLINUX_SYMBOL_STR(journal_init_inode) },
	{ 0x25820c64, __VMLINUX_SYMBOL_STR(fs_overflowuid) },
	{ 0xacf4d843, __VMLINUX_SYMBOL_STR(match_strdup) },
	{ 0xcb2a7756, __VMLINUX_SYMBOL_STR(mb_cache_entry_get) },
	{ 0x4266402f, __VMLINUX_SYMBOL_STR(filemap_write_and_wait) },
	{ 0x84a8f36e, __VMLINUX_SYMBOL_STR(__lock_buffer) },
	{ 0x20000329, __VMLINUX_SYMBOL_STR(simple_strtoul) },
	{ 0x6def2db2, __VMLINUX_SYMBOL_STR(half_md4_transform) },
	{ 0xad317f36, __VMLINUX_SYMBOL_STR(generic_file_aio_read) },
	{ 0xb847c87e, __VMLINUX_SYMBOL_STR(mb_cache_entry_release) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0xe5a4cccc, __VMLINUX_SYMBOL_STR(mb_cache_entry_alloc) },
	{ 0xbb551ce0, __VMLINUX_SYMBOL_STR(dquot_quota_off) },
	{ 0x6729d3df, __VMLINUX_SYMBOL_STR(__get_user_4) },
	{ 0x44e9a829, __VMLINUX_SYMBOL_STR(match_token) },
	{ 0xee361410, __VMLINUX_SYMBOL_STR(buffer_migrate_page) },
	{ 0xa4e434de, __VMLINUX_SYMBOL_STR(inc_nlink) },
	{ 0xe6dd04af, __VMLINUX_SYMBOL_STR(init_user_ns) },
	{ 0xee0a055c, __VMLINUX_SYMBOL_STR(__percpu_counter_sum) },
	{ 0x9621849f, __VMLINUX_SYMBOL_STR(ring_buffer_event_data) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x190db1aa, __VMLINUX_SYMBOL_STR(mount_bdev) },
	{ 0x85df9b6c, __VMLINUX_SYMBOL_STR(strsep) },
	{ 0x1024e064, __VMLINUX_SYMBOL_STR(journal_stop) },
	{ 0x9071485c, __VMLINUX_SYMBOL_STR(__dquot_free_space) },
	{ 0x8d1d2e94, __VMLINUX_SYMBOL_STR(generic_read_dir) },
	{ 0xdaac886f, __VMLINUX_SYMBOL_STR(filter_current_check_discard) },
	{ 0x1f3f183b, __VMLINUX_SYMBOL_STR(unlock_buffer) },
	{ 0x7a51ef69, __VMLINUX_SYMBOL_STR(generic_setxattr) },
	{ 0x374c5c42, __VMLINUX_SYMBOL_STR(__dquot_alloc_space) },
	{ 0x842205e8, __VMLINUX_SYMBOL_STR(redirty_page_for_writepage) },
	{ 0xedcb381b, __VMLINUX_SYMBOL_STR(generic_file_aio_write) },
	{ 0x307c2fd0, __VMLINUX_SYMBOL_STR(generic_check_addressable) },
	{ 0xc499ae1e, __VMLINUX_SYMBOL_STR(kstrdup) },
	{ 0xb4ff7a2c, __VMLINUX_SYMBOL_STR(journal_clear_err) },
	{ 0xacc3dc46, __VMLINUX_SYMBOL_STR(dquot_get_dqblk) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x9b56eb61, __VMLINUX_SYMBOL_STR(truncate_setsize) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x57a6ccd0, __VMLINUX_SYMBOL_STR(down_read) },
	{ 0x3bba2de8, __VMLINUX_SYMBOL_STR(journal_destroy) },
	{ 0xece784c2, __VMLINUX_SYMBOL_STR(rb_first) },
	{ 0x11f2663c, __VMLINUX_SYMBOL_STR(invalidate_bdev) },
	{ 0x3cf6f29, __VMLINUX_SYMBOL_STR(journal_get_create_access) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x82a50c9b, __VMLINUX_SYMBOL_STR(mb_cache_entry_find_first) },
	{ 0xa2cbbe8, __VMLINUX_SYMBOL_STR(inode_owner_or_capable) },
	{ 0x6ef0d557, __VMLINUX_SYMBOL_STR(journal_get_undo_access) },
	{ 0xf6e14473, __VMLINUX_SYMBOL_STR(mpage_readpages) },
	{ 0x447a33ef, __VMLINUX_SYMBOL_STR(journal_get_write_access) },
	{ 0xfd6ef57d, __VMLINUX_SYMBOL_STR(trace_event_buffer_lock_reserve) },
	{ 0x3c0a7e36, __VMLINUX_SYMBOL_STR(trace_define_field) },
	{ 0x4d54d942, __VMLINUX_SYMBOL_STR(mpage_readpage) },
	{ 0x404e35d6, __VMLINUX_SYMBOL_STR(dquot_alloc_inode) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x3e751a12, __VMLINUX_SYMBOL_STR(journal_update_format) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xe15f42bb, __VMLINUX_SYMBOL_STR(_raw_spin_trylock) },
	{ 0xf45a4e52, __VMLINUX_SYMBOL_STR(posix_acl_chmod) },
	{ 0x6faf7207, __VMLINUX_SYMBOL_STR(d_obtain_alias) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xb06761e1, __VMLINUX_SYMBOL_STR(journal_try_to_free_buffers) },
	{ 0xb5c5b29d, __VMLINUX_SYMBOL_STR(find_or_create_page) },
	{ 0x7c1372e8, __VMLINUX_SYMBOL_STR(panic) },
	{ 0x479c3c86, __VMLINUX_SYMBOL_STR(find_next_zero_bit) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x4d9b652b, __VMLINUX_SYMBOL_STR(rb_erase) },
	{ 0x5d5fbe97, __VMLINUX_SYMBOL_STR(mb_cache_destroy) },
	{ 0xb1a850de, __VMLINUX_SYMBOL_STR(journal_load) },
	{ 0xe740d1e9, __VMLINUX_SYMBOL_STR(journal_trans_will_send_data_barrier) },
	{ 0x9507231f, __VMLINUX_SYMBOL_STR(security_inode_init_security) },
	{ 0xc0373e69, __VMLINUX_SYMBOL_STR(journal_flush) },
	{ 0x5a921311, __VMLINUX_SYMBOL_STR(strncmp) },
	{ 0xb54088a3, __VMLINUX_SYMBOL_STR(posix_acl_alloc) },
	{ 0xc9409b91, __VMLINUX_SYMBOL_STR(kmem_cache_free) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x736c541f, __VMLINUX_SYMBOL_STR(set_nlink) },
	{ 0xee87d815, __VMLINUX_SYMBOL_STR(__wait_on_buffer) },
	{ 0x93d5a015, __VMLINUX_SYMBOL_STR(setattr_copy) },
	{ 0x6808fc0b, __VMLINUX_SYMBOL_STR(dquot_resume) },
	{ 0x75057c31, __VMLINUX_SYMBOL_STR(posix_acl_equiv_mode) },
	{ 0x2184f416, __VMLINUX_SYMBOL_STR(insert_inode_locked) },
	{ 0x941857c7, __VMLINUX_SYMBOL_STR(sync_dirty_buffer) },
	{ 0x5240ee7, __VMLINUX_SYMBOL_STR(percpu_counter_batch) },
	{ 0x2d416365, __VMLINUX_SYMBOL_STR(blkdev_get_by_dev) },
	{ 0x4e3567f7, __VMLINUX_SYMBOL_STR(match_int) },
	{ 0x67f43102, __VMLINUX_SYMBOL_STR(unlock_page) },
	{ 0x3b4ceb4a, __VMLINUX_SYMBOL_STR(up_write) },
	{ 0xe6e3b875, __VMLINUX_SYMBOL_STR(down_write) },
	{ 0x428f24f9, __VMLINUX_SYMBOL_STR(posix_acl_create) },
	{ 0xda87dc34, __VMLINUX_SYMBOL_STR(__brelse) },
	{ 0xafbb2e57, __VMLINUX_SYMBOL_STR(journal_start_commit) },
	{ 0xc7707c2d, __VMLINUX_SYMBOL_STR(generic_removexattr) },
	{ 0xe98f3cd1, __VMLINUX_SYMBOL_STR(dquot_set_dqblk) },
	{ 0x42d95661, __VMLINUX_SYMBOL_STR(journal_lock_updates) },
	{ 0x765ac851, __VMLINUX_SYMBOL_STR(inode_init_once) },
	{ 0xb4296868, __VMLINUX_SYMBOL_STR(bh_submit_read) },
	{ 0xf941442, __VMLINUX_SYMBOL_STR(ftrace_event_reg) },
	{ 0x65bcc394, __VMLINUX_SYMBOL_STR(journal_revoke) },
	{ 0x14ad961b, __VMLINUX_SYMBOL_STR(page_follow_link_light) },
	{ 0x167c5967, __VMLINUX_SYMBOL_STR(print_hex_dump) },
	{ 0x5b2862f7, __VMLINUX_SYMBOL_STR(mnt_drop_write_file) },
	{ 0xc6cbbc89, __VMLINUX_SYMBOL_STR(capable) },
	{ 0x1cf7068c, __VMLINUX_SYMBOL_STR(journal_unlock_updates) },
	{ 0x7171121c, __VMLINUX_SYMBOL_STR(overflowgid) },
	{ 0x51bc3a46, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xf1c1e695, __VMLINUX_SYMBOL_STR(blkdev_put) },
	{ 0x4821e3fe, __VMLINUX_SYMBOL_STR(dquot_quota_on) },
	{ 0xb2fd5ceb, __VMLINUX_SYMBOL_STR(__put_user_4) },
	{ 0x86cb53c2, __VMLINUX_SYMBOL_STR(journal_create) },
	{ 0x23fb3ba4, __VMLINUX_SYMBOL_STR(dquot_initialize) },
	{ 0x8d7cdfa8, __VMLINUX_SYMBOL_STR(generic_block_fiemap) },
	{ 0x31794b12, __VMLINUX_SYMBOL_STR(generic_file_mmap) },
	{ 0xe92fcf53, __VMLINUX_SYMBOL_STR(mb_cache_entry_free) },
	{ 0x48da1181, __VMLINUX_SYMBOL_STR(block_write_full_page) },
	{ 0x94c90b31, __VMLINUX_SYMBOL_STR(bdevname) },
	{ 0xc2aee12b, __VMLINUX_SYMBOL_STR(sync_blockdev) },
	{ 0xc7f7fc5b, __VMLINUX_SYMBOL_STR(block_write_end) },
	{ 0x535fbba7, __VMLINUX_SYMBOL_STR(create_empty_buffers) },
	{ 0x456f5002, __VMLINUX_SYMBOL_STR(try_to_free_buffers) },
	{ 0x7ad41bf9, __VMLINUX_SYMBOL_STR(posix_acl_valid) },
	{ 0xf82ec573, __VMLINUX_SYMBOL_STR(rb_prev) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xa12f5bb9, __VMLINUX_SYMBOL_STR(posix_acl_from_xattr) },
	{ 0x563eed2, __VMLINUX_SYMBOL_STR(dquot_quota_sync) },
	{ 0x3d9ee9f0, __VMLINUX_SYMBOL_STR(clear_page) },
	{ 0x57fb46e1, __VMLINUX_SYMBOL_STR(do_sync_read) },
	{ 0x6e94ef8f, __VMLINUX_SYMBOL_STR(unlock_new_inode) },
	{ 0x6b3f4972, __VMLINUX_SYMBOL_STR(journal_invalidatepage) },
	{ 0x8279df70, __VMLINUX_SYMBOL_STR(mnt_want_write_file) },
	{ 0x3ec16c3b, __VMLINUX_SYMBOL_STR(in_group_p) },
	{ 0xb541366a, __VMLINUX_SYMBOL_STR(kill_block_super) },
	{ 0x6b2dc060, __VMLINUX_SYMBOL_STR(dump_stack) },
	{ 0x1b427524, __VMLINUX_SYMBOL_STR(log_start_commit) },
	{ 0x5e95b1cd, __VMLINUX_SYMBOL_STR(current_umask) },
	{ 0x97aad9cf, __VMLINUX_SYMBOL_STR(page_cache_sync_readahead) },
	{ 0x6e297a62, __VMLINUX_SYMBOL_STR(__percpu_counter_init) },
	{ 0x846d7060, __VMLINUX_SYMBOL_STR(inode_change_ok) },
	{ 0xb2623da3, __VMLINUX_SYMBOL_STR(submit_bh) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xd1bf92f3, __VMLINUX_SYMBOL_STR(dquot_drop) },
	{ 0x15bc8387, __VMLINUX_SYMBOL_STR(mb_cache_entry_insert) },
	{ 0xa5526619, __VMLINUX_SYMBOL_STR(rb_insert_color) },
	{ 0x67cc86e8, __VMLINUX_SYMBOL_STR(kmem_cache_create) },
	{ 0xce917bb0, __VMLINUX_SYMBOL_STR(dquot_get_dqinfo) },
	{ 0x98a5c6ab, __VMLINUX_SYMBOL_STR(dquot_transfer) },
	{ 0x455b1dc2, __VMLINUX_SYMBOL_STR(register_filesystem) },
	{ 0x2b20440d, __VMLINUX_SYMBOL_STR(journal_extend) },
	{ 0x78342ae0, __VMLINUX_SYMBOL_STR(iput) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x9dd700f0, __VMLINUX_SYMBOL_STR(inode_dio_wait) },
	{ 0xf96c7e34, __VMLINUX_SYMBOL_STR(do_sync_write) },
	{ 0x1a81608a, __VMLINUX_SYMBOL_STR(generic_file_splice_write) },
	{ 0x83f125cb, __VMLINUX_SYMBOL_STR(ihold) },
	{ 0x1edda8be, __VMLINUX_SYMBOL_STR(trace_event_raw_init) },
	{ 0xa75312bc, __VMLINUX_SYMBOL_STR(call_rcu_sched) },
	{ 0x828f9949, __VMLINUX_SYMBOL_STR(generic_error_remove_page) },
	{ 0xb63c5dd1, __VMLINUX_SYMBOL_STR(d_splice_alias) },
	{ 0xeb580408, __VMLINUX_SYMBOL_STR(journal_errno) },
	{ 0xbf54e1e7, __VMLINUX_SYMBOL_STR(end_buffer_read_sync) },
	{ 0xe6602e9f, __VMLINUX_SYMBOL_STR(perf_trace_buf_prepare) },
	{ 0x7938690a, __VMLINUX_SYMBOL_STR(mb_cache_shrink) },
	{ 0x9236e33c, __VMLINUX_SYMBOL_STR(sb_set_blocksize) },
	{ 0x41f1b73f, __VMLINUX_SYMBOL_STR(generic_readlink) },
	{ 0x7628f3c7, __VMLINUX_SYMBOL_STR(this_cpu_off) },
	{ 0x2e710893, __VMLINUX_SYMBOL_STR(put_page) },
	{ 0x8c12322f, __VMLINUX_SYMBOL_STR(d_make_root) },
	{ 0xf98f5f92, __VMLINUX_SYMBOL_STR(__blockdev_direct_IO) },
	{ 0xdc0ec901, __VMLINUX_SYMBOL_STR(__block_write_begin) },
	{ 0x32e3d8df, __VMLINUX_SYMBOL_STR(__find_get_block) },
	{ 0x8b618d08, __VMLINUX_SYMBOL_STR(overflowuid) },
	{ 0xca9360b5, __VMLINUX_SYMBOL_STR(rb_next) },
	{ 0x315e618f, __VMLINUX_SYMBOL_STR(mark_buffer_dirty) },
	{ 0xa009b3e7, __VMLINUX_SYMBOL_STR(unregister_filesystem) },
	{ 0x5e9c3496, __VMLINUX_SYMBOL_STR(init_special_inode) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x92c0167b, __VMLINUX_SYMBOL_STR(new_inode) },
	{ 0x1e3a88fb, __VMLINUX_SYMBOL_STR(trace_seq_printf) },
	{ 0xb0e602eb, __VMLINUX_SYMBOL_STR(memmove) },
	{ 0x8cc3fc32, __VMLINUX_SYMBOL_STR(generic_file_splice_read) },
	{ 0x25d9d27f, __VMLINUX_SYMBOL_STR(__getblk) },
	{ 0x5bbc6b22, __VMLINUX_SYMBOL_STR(journal_blocks_per_page) },
	{ 0xa60806d5, __VMLINUX_SYMBOL_STR(set_blocksize) },
	{ 0xd491ca09, __VMLINUX_SYMBOL_STR(dquot_free_inode) },
	{ 0x3dba25e7, __VMLINUX_SYMBOL_STR(dquot_release) },
	{ 0x7bb7207a, __VMLINUX_SYMBOL_STR(dquot_disable) },
	{ 0x19bf9cf, __VMLINUX_SYMBOL_STR(journal_wipe) },
	{ 0x65e11a51, __VMLINUX_SYMBOL_STR(generic_fh_to_dentry) },
	{ 0x4db15ece, __VMLINUX_SYMBOL_STR(grab_cache_page_write_begin) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0xc978661d, __VMLINUX_SYMBOL_STR(journal_forget) },
	{ 0x62aa1ea9, __VMLINUX_SYMBOL_STR(dquot_commit_info) },
	{ 0x9ed4e00a, __VMLINUX_SYMBOL_STR(dquot_set_dqinfo) },
	{ 0xcf33d584, __VMLINUX_SYMBOL_STR(journal_dirty_metadata) },
	{ 0xe8c92535, __VMLINUX_SYMBOL_STR(journal_init_dev) },
	{ 0x411627e6, __VMLINUX_SYMBOL_STR(clear_inode) },
	{ 0x9c33b612, __VMLINUX_SYMBOL_STR(mb_cache_create) },
	{ 0x9a3596b5, __VMLINUX_SYMBOL_STR(page_put_link) },
	{ 0x1bce8f24, __VMLINUX_SYMBOL_STR(d_instantiate) },
	{ 0xcdabc909, __VMLINUX_SYMBOL_STR(ftrace_raw_output_prep) },
	{ 0x2a6e6109, __VMLINUX_SYMBOL_STR(__init_rwsem) },
	{ 0x2e13b258, __VMLINUX_SYMBOL_STR(journal_force_commit_nested) },
	{ 0xa6b643af, __VMLINUX_SYMBOL_STR(journal_dirty_data) },
	{ 0x498d293a, __VMLINUX_SYMBOL_STR(trace_buffer_unlock_commit) },
	{ 0x1f721021, __VMLINUX_SYMBOL_STR(filemap_flush) },
	{ 0x55dd9f9d, __VMLINUX_SYMBOL_STR(generic_block_bmap) },
	{ 0x9ac23dd2, __VMLINUX_SYMBOL_STR(clear_nlink) },
	{ 0xeedc5133, __VMLINUX_SYMBOL_STR(iget_locked) },
	{ 0xf812cff6, __VMLINUX_SYMBOL_STR(memscan) },
	{ 0xd36c760d, __VMLINUX_SYMBOL_STR(inode_init_owner) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
	{ 0xfbcbd5c7, __VMLINUX_SYMBOL_STR(bh_uptodate_or_lock) },
	{ 0xc585088f, __VMLINUX_SYMBOL_STR(journal_force_commit) },
	{ 0x24afe55d, __VMLINUX_SYMBOL_STR(truncate_inode_pages) },
	{ 0x35f51627, __VMLINUX_SYMBOL_STR(dquot_commit) },
	{ 0xdf929370, __VMLINUX_SYMBOL_STR(fs_overflowgid) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=jbd";


MODULE_INFO(srcversion, "ADC187B0999759BF933D7CF");

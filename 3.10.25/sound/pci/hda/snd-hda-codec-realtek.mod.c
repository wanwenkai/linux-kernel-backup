#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x3445a13, __VMLINUX_SYMBOL_STR(snd_hda_gen_free) },
	{ 0x277b95a7, __VMLINUX_SYMBOL_STR(snd_hda_codec_get_pin_target) },
	{ 0xd2f7a010, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_put) },
	{ 0x1bf65c9c, __VMLINUX_SYMBOL_STR(snd_hda_gen_parse_auto_config) },
	{ 0x900f7006, __VMLINUX_SYMBOL_STR(snd_hda_apply_fixup) },
	{ 0xa829bf61, __VMLINUX_SYMBOL_STR(snd_hda_add_verbs) },
	{ 0x4555e99c, __VMLINUX_SYMBOL_STR(snd_hda_pick_fixup) },
	{ 0xb6e3d717, __VMLINUX_SYMBOL_STR(snd_hda_mixer_bind_ctls_put) },
	{ 0x52028d39, __VMLINUX_SYMBOL_STR(snd_pci_quirk_lookup) },
	{ 0x5ab12290, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_tlv) },
	{ 0xb7d17e61, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_pcms) },
	{ 0x676f5ac, __VMLINUX_SYMBOL_STR(snd_hda_get_input_pin_attr) },
	{ 0x95217b0b, __VMLINUX_SYMBOL_STR(snd_hda_override_conn_list) },
	{ 0xce155efb, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect) },
	{ 0x588dd561, __VMLINUX_SYMBOL_STR(snd_hda_jack_unsol_event) },
	{ 0xc499ae1e, __VMLINUX_SYMBOL_STR(kstrdup) },
	{ 0x289ee3e4, __VMLINUX_SYMBOL_STR(snd_hda_gen_update_outputs) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x187002fc, __VMLINUX_SYMBOL_STR(snd_hda_override_pin_caps) },
	{ 0xf872af8d, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect_enable_callback) },
	{ 0x2cca1760, __VMLINUX_SYMBOL_STR(snd_hda_query_pin_caps) },
	{ 0x918ebfe0, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_get) },
	{ 0x1253059c, __VMLINUX_SYMBOL_STR(_snd_hda_set_pin_ctl) },
	{ 0xf94531a1, __VMLINUX_SYMBOL_STR(snd_hda_gen_hp_automute) },
	{ 0x75a0ff0f, __VMLINUX_SYMBOL_STR(snd_hda_gen_check_power_status) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x20c55ae0, __VMLINUX_SYMBOL_STR(sscanf) },
	{ 0x6add5c9a, __VMLINUX_SYMBOL_STR(dmi_find_device) },
	{ 0x2385cdde, __VMLINUX_SYMBOL_STR(snd_hda_add_codec_preset) },
	{ 0xf59ac8e7, __VMLINUX_SYMBOL_STR(snd_hda_mixer_bind_ctls_get) },
	{ 0xbbd534a7, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_analog_prepare) },
	{ 0x1fb2cb0e, __VMLINUX_SYMBOL_STR(snd_hda_mixer_bind_ctls_info) },
	{ 0xf15b3fe1, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_analog_cleanup) },
	{ 0x8c064de3, __VMLINUX_SYMBOL_STR(snd_hda_codec_write) },
	{ 0x5698ae47, __VMLINUX_SYMBOL_STR(snd_hda_gen_init) },
	{ 0x8fe7eb13, __VMLINUX_SYMBOL_STR(snd_hda_parse_pin_defcfg) },
	{ 0x5a3afa00, __VMLINUX_SYMBOL_STR(snd_hda_jack_set_gating_jack) },
	{ 0x10311d9c, __VMLINUX_SYMBOL_STR(snd_hda_override_amp_caps) },
	{ 0x3888ac21, __VMLINUX_SYMBOL_STR(snd_ctl_boolean_mono_info) },
	{ 0x3b295720, __VMLINUX_SYMBOL_STR(snd_hda_find_mixer_ctl) },
	{ 0xacd73f87, __VMLINUX_SYMBOL_STR(snd_hda_sequence_write) },
	{ 0x4d59dc0d, __VMLINUX_SYMBOL_STR(snd_hda_ctl_add) },
	{ 0x5bc09ffd, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_analog_open) },
	{ 0xc5887676, __VMLINUX_SYMBOL_STR(snd_hda_bind_sw) },
	{ 0x143cf13a, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_put_beep) },
	{ 0xfa4ea467, __VMLINUX_SYMBOL_STR(snd_ctl_new1) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x44a99c44, __VMLINUX_SYMBOL_STR(snd_hda_codec_flush_cache) },
	{ 0xe60d0c51, __VMLINUX_SYMBOL_STR(snd_hda_codec_resume_amp) },
	{ 0xf85ccffe, __VMLINUX_SYMBOL_STR(snd_hda_gen_add_kctl) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xd8883812, __VMLINUX_SYMBOL_STR(snd_hda_get_nid_path) },
	{ 0x79b32a4b, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_get_beep) },
	{ 0x9c27f124, __VMLINUX_SYMBOL_STR(snd_hda_codec_get_pincfg) },
	{ 0x487f7d71, __VMLINUX_SYMBOL_STR(snd_hda_apply_pincfgs) },
	{ 0xd37f25a, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_info) },
	{ 0x551252dd, __VMLINUX_SYMBOL_STR(snd_hda_gen_spec_init) },
	{ 0x848528bb, __VMLINUX_SYMBOL_STR(snd_hda_codec_resume_cache) },
	{ 0x7ba6bc09, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_info) },
	{ 0xebafbd63, __VMLINUX_SYMBOL_STR(snd_hda_delete_codec_preset) },
	{ 0xea5594e3, __VMLINUX_SYMBOL_STR(query_amp_caps) },
	{ 0x29da3d99, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_controls) },
	{ 0x31da3d1f, __VMLINUX_SYMBOL_STR(snd_hda_codec_read) },
	{ 0xdab68a8b, __VMLINUX_SYMBOL_STR(snd_hda_codec_amp_read) },
	{ 0xa9d4c058, __VMLINUX_SYMBOL_STR(snd_hda_add_new_ctls) },
	{ 0x2479273c, __VMLINUX_SYMBOL_STR(snd_hda_shutup_pins) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-hda-codec,snd";


MODULE_INFO(srcversion, "6E91272DFCE3FB68855EDD3");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x3445a13, __VMLINUX_SYMBOL_STR(snd_hda_gen_free) },
	{ 0x32854fd1, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_prepare) },
	{ 0x375cacd5, __VMLINUX_SYMBOL_STR(snd_hda_input_mux_info) },
	{ 0xd2f7a010, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_put) },
	{ 0x1bf65c9c, __VMLINUX_SYMBOL_STR(snd_hda_gen_parse_auto_config) },
	{ 0x900f7006, __VMLINUX_SYMBOL_STR(snd_hda_apply_fixup) },
	{ 0x4555e99c, __VMLINUX_SYMBOL_STR(snd_hda_pick_fixup) },
	{ 0x28ca6784, __VMLINUX_SYMBOL_STR(snd_hda_codec_amp_stereo) },
	{ 0xb6e3d717, __VMLINUX_SYMBOL_STR(snd_hda_mixer_bind_ctls_put) },
	{ 0xf506c307, __VMLINUX_SYMBOL_STR(__snd_hda_add_vmaster) },
	{ 0x141e6778, __VMLINUX_SYMBOL_STR(snd_hda_input_mux_put) },
	{ 0x5ab12290, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_tlv) },
	{ 0xb7d17e61, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_pcms) },
	{ 0xce155efb, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect) },
	{ 0x588dd561, __VMLINUX_SYMBOL_STR(snd_hda_jack_unsol_event) },
	{ 0xa466ee43, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_put) },
	{ 0x2cca1760, __VMLINUX_SYMBOL_STR(snd_hda_query_pin_caps) },
	{ 0x918ebfe0, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_get) },
	{ 0x1253059c, __VMLINUX_SYMBOL_STR(_snd_hda_set_pin_ctl) },
	{ 0x9b9039f3, __VMLINUX_SYMBOL_STR(snd_hda_attach_beep_device) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x75a0ff0f, __VMLINUX_SYMBOL_STR(snd_hda_gen_check_power_status) },
	{ 0xe71ff1b3, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_get) },
	{ 0x90b8118c, __VMLINUX_SYMBOL_STR(snd_hda_codec_set_power_to_all) },
	{ 0x2385cdde, __VMLINUX_SYMBOL_STR(snd_hda_add_codec_preset) },
	{ 0xf59ac8e7, __VMLINUX_SYMBOL_STR(snd_hda_mixer_bind_ctls_get) },
	{ 0xbbd534a7, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_analog_prepare) },
	{ 0x1fb2cb0e, __VMLINUX_SYMBOL_STR(snd_hda_mixer_bind_ctls_info) },
	{ 0xf15b3fe1, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_analog_cleanup) },
	{ 0x8c064de3, __VMLINUX_SYMBOL_STR(snd_hda_codec_write) },
	{ 0x21335210, __VMLINUX_SYMBOL_STR(snd_hda_create_spdif_in_ctls) },
	{ 0x5698ae47, __VMLINUX_SYMBOL_STR(snd_hda_gen_init) },
	{ 0x8fe7eb13, __VMLINUX_SYMBOL_STR(snd_hda_parse_pin_defcfg) },
	{ 0x10311d9c, __VMLINUX_SYMBOL_STR(snd_hda_override_amp_caps) },
	{ 0x3888ac21, __VMLINUX_SYMBOL_STR(snd_ctl_boolean_mono_info) },
	{ 0x3b295720, __VMLINUX_SYMBOL_STR(snd_hda_find_mixer_ctl) },
	{ 0xacd73f87, __VMLINUX_SYMBOL_STR(snd_hda_sequence_write) },
	{ 0xb7802a44, __VMLINUX_SYMBOL_STR(snd_hda_create_spdif_share_sw) },
	{ 0x4d59dc0d, __VMLINUX_SYMBOL_STR(snd_hda_ctl_add) },
	{ 0x5bc09ffd, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_analog_open) },
	{ 0xf9403390, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_close) },
	{ 0x787276eb, __VMLINUX_SYMBOL_STR(snd_hda_create_dig_out_ctls) },
	{ 0xc5887676, __VMLINUX_SYMBOL_STR(snd_hda_bind_sw) },
	{ 0x369b2a3b, __VMLINUX_SYMBOL_STR(snd_hda_codec_write_cache) },
	{ 0x143cf13a, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_put_beep) },
	{ 0xfa4ea467, __VMLINUX_SYMBOL_STR(snd_ctl_new1) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd2a77d91, __VMLINUX_SYMBOL_STR(snd_hda_check_board_config) },
	{ 0x9274913, __VMLINUX_SYMBOL_STR(__snd_hda_codec_cleanup_stream) },
	{ 0xbe9ad4bc, __VMLINUX_SYMBOL_STR(snd_hda_detach_beep_device) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x8c48fada, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_open) },
	{ 0x79b32a4b, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_get_beep) },
	{ 0x9c27f124, __VMLINUX_SYMBOL_STR(snd_hda_codec_get_pincfg) },
	{ 0x182ac953, __VMLINUX_SYMBOL_STR(snd_hda_mixer_bind_tlv) },
	{ 0xd37f25a, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_info) },
	{ 0x5528903c, __VMLINUX_SYMBOL_STR(snd_hda_codec_setup_stream) },
	{ 0x551252dd, __VMLINUX_SYMBOL_STR(snd_hda_gen_spec_init) },
	{ 0x7ba6bc09, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_info) },
	{ 0xebafbd63, __VMLINUX_SYMBOL_STR(snd_hda_delete_codec_preset) },
	{ 0xea5594e3, __VMLINUX_SYMBOL_STR(query_amp_caps) },
	{ 0x8c2dc817, __VMLINUX_SYMBOL_STR(snd_hda_set_vmaster_tlv) },
	{ 0x29e891c9, __VMLINUX_SYMBOL_STR(snd_hda_bind_vol) },
	{ 0x29da3d99, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_controls) },
	{ 0x31da3d1f, __VMLINUX_SYMBOL_STR(snd_hda_codec_read) },
	{ 0xb8c2853, __VMLINUX_SYMBOL_STR(snd_hda_get_connections) },
	{ 0xa9d4c058, __VMLINUX_SYMBOL_STR(snd_hda_add_new_ctls) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-hda-codec,snd";


MODULE_INFO(srcversion, "B8ED0342FCF483F14F2FF96");

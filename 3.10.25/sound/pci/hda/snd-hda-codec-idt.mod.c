#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x3445a13, __VMLINUX_SYMBOL_STR(snd_hda_gen_free) },
	{ 0x375cacd5, __VMLINUX_SYMBOL_STR(snd_hda_input_mux_info) },
	{ 0xd2f7a010, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_put) },
	{ 0xd940ac1a, __VMLINUX_SYMBOL_STR(snd_hda_codec_eapd_power_filter) },
	{ 0x1bf65c9c, __VMLINUX_SYMBOL_STR(snd_hda_gen_parse_auto_config) },
	{ 0x900f7006, __VMLINUX_SYMBOL_STR(snd_hda_apply_fixup) },
	{ 0xa829bf61, __VMLINUX_SYMBOL_STR(snd_hda_add_verbs) },
	{ 0x4555e99c, __VMLINUX_SYMBOL_STR(snd_hda_pick_fixup) },
	{ 0x141e6778, __VMLINUX_SYMBOL_STR(snd_hda_input_mux_put) },
	{ 0x5ab12290, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_tlv) },
	{ 0xb7d17e61, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_pcms) },
	{ 0x676f5ac, __VMLINUX_SYMBOL_STR(snd_hda_get_input_pin_attr) },
	{ 0xce155efb, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect) },
	{ 0x588dd561, __VMLINUX_SYMBOL_STR(snd_hda_jack_unsol_event) },
	{ 0x1e88a7c2, __VMLINUX_SYMBOL_STR(snd_hda_codec_set_pincfg) },
	{ 0xa466ee43, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_put) },
	{ 0x289ee3e4, __VMLINUX_SYMBOL_STR(snd_hda_gen_update_outputs) },
	{ 0xf872af8d, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect_enable_callback) },
	{ 0x918ebfe0, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_get) },
	{ 0x1253059c, __VMLINUX_SYMBOL_STR(_snd_hda_set_pin_ctl) },
	{ 0xf94531a1, __VMLINUX_SYMBOL_STR(snd_hda_gen_hp_automute) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x20c55ae0, __VMLINUX_SYMBOL_STR(sscanf) },
	{ 0x6add5c9a, __VMLINUX_SYMBOL_STR(dmi_find_device) },
	{ 0xe71ff1b3, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_get) },
	{ 0x90b8118c, __VMLINUX_SYMBOL_STR(snd_hda_codec_set_power_to_all) },
	{ 0x2385cdde, __VMLINUX_SYMBOL_STR(snd_hda_add_codec_preset) },
	{ 0x8c064de3, __VMLINUX_SYMBOL_STR(snd_hda_codec_write) },
	{ 0x1e6d26a8, __VMLINUX_SYMBOL_STR(strstr) },
	{ 0x80a00229, __VMLINUX_SYMBOL_STR(is_jack_detectable) },
	{ 0x94659ed8, __VMLINUX_SYMBOL_STR(snd_hda_sequence_write_cache) },
	{ 0x5698ae47, __VMLINUX_SYMBOL_STR(snd_hda_gen_init) },
	{ 0x8fe7eb13, __VMLINUX_SYMBOL_STR(snd_hda_parse_pin_defcfg) },
	{ 0x10311d9c, __VMLINUX_SYMBOL_STR(snd_hda_override_amp_caps) },
	{ 0x3888ac21, __VMLINUX_SYMBOL_STR(snd_ctl_boolean_mono_info) },
	{ 0x6e680abe, __VMLINUX_SYMBOL_STR(snd_hda_gen_line_automute) },
	{ 0x4b015768, __VMLINUX_SYMBOL_STR(snd_iprintf) },
	{ 0x6a209953, __VMLINUX_SYMBOL_STR(snd_hda_jack_tbl_get) },
	{ 0x4dab1f60, __VMLINUX_SYMBOL_STR(snd_hda_add_imux_item) },
	{ 0x369b2a3b, __VMLINUX_SYMBOL_STR(snd_hda_codec_write_cache) },
	{ 0x143cf13a, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_put_beep) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3b17a7f9, __VMLINUX_SYMBOL_STR(snd_hda_enable_beep_device) },
	{ 0xe60d0c51, __VMLINUX_SYMBOL_STR(snd_hda_codec_resume_amp) },
	{ 0xf85ccffe, __VMLINUX_SYMBOL_STR(snd_hda_gen_add_kctl) },
	{ 0x79b32a4b, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_get_beep) },
	{ 0x9c27f124, __VMLINUX_SYMBOL_STR(snd_hda_codec_get_pincfg) },
	{ 0x487f7d71, __VMLINUX_SYMBOL_STR(snd_hda_apply_pincfgs) },
	{ 0xd37f25a, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_info) },
	{ 0x551252dd, __VMLINUX_SYMBOL_STR(snd_hda_gen_spec_init) },
	{ 0x848528bb, __VMLINUX_SYMBOL_STR(snd_hda_codec_resume_cache) },
	{ 0x515f54e8, __VMLINUX_SYMBOL_STR(snd_hda_power_save) },
	{ 0x7ba6bc09, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_info) },
	{ 0xebafbd63, __VMLINUX_SYMBOL_STR(snd_hda_delete_codec_preset) },
	{ 0xea5594e3, __VMLINUX_SYMBOL_STR(query_amp_caps) },
	{ 0x8997916, __VMLINUX_SYMBOL_STR(snd_hda_gen_mic_autoswitch) },
	{ 0x29da3d99, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_controls) },
	{ 0x31da3d1f, __VMLINUX_SYMBOL_STR(snd_hda_codec_read) },
	{ 0xb8c2853, __VMLINUX_SYMBOL_STR(snd_hda_get_connections) },
	{ 0x2479273c, __VMLINUX_SYMBOL_STR(snd_hda_shutup_pins) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-hda-codec,snd";


MODULE_INFO(srcversion, "0AA862266A46E9CFDFAA12B");

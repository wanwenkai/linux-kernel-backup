#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xe722ff65, __VMLINUX_SYMBOL_STR(pci_bus_read_config_byte) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x61b7f275, __VMLINUX_SYMBOL_STR(perf_tp_event) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xc4dc87, __VMLINUX_SYMBOL_STR(timecounter_init) },
	{ 0x20ddb9ae, __VMLINUX_SYMBOL_STR(__pm_runtime_idle) },
	{ 0x7136f957, __VMLINUX_SYMBOL_STR(snd_hda_codec_prepare) },
	{ 0x4c4fef19, __VMLINUX_SYMBOL_STR(kernel_stack) },
	{ 0x10c8e1eb, __VMLINUX_SYMBOL_STR(snd_pcm_lib_default_mmap) },
	{ 0x253bdb78, __VMLINUX_SYMBOL_STR(param_get_int) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x767ddb02, __VMLINUX_SYMBOL_STR(set_memory_wc) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0xc0d6c35, __VMLINUX_SYMBOL_STR(snd_pcm_hw_constraint_step) },
	{ 0xbcf5587b, __VMLINUX_SYMBOL_STR(snd_pcm_period_elapsed) },
	{ 0xb4df1b5c, __VMLINUX_SYMBOL_STR(snd_card_create) },
	{ 0xb9f8b0f2, __VMLINUX_SYMBOL_STR(snd_dma_alloc_pages) },
	{ 0x4a8cbbae, __VMLINUX_SYMBOL_STR(dma_set_mask) },
	{ 0x59df21ad, __VMLINUX_SYMBOL_STR(pci_disable_device) },
	{ 0x52028d39, __VMLINUX_SYMBOL_STR(snd_pci_quirk_lookup) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x33339182, __VMLINUX_SYMBOL_STR(pci_release_regions) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0x9621849f, __VMLINUX_SYMBOL_STR(ring_buffer_event_data) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x4fd451f9, __VMLINUX_SYMBOL_STR(__pm_runtime_resume) },
	{ 0x24939c32, __VMLINUX_SYMBOL_STR(pci_bus_write_config_word) },
	{ 0xc4dc9e59, __VMLINUX_SYMBOL_STR(set_pages_array_wc) },
	{ 0x83c8a355, __VMLINUX_SYMBOL_STR(param_set_int) },
	{ 0xdaac886f, __VMLINUX_SYMBOL_STR(filter_current_check_discard) },
	{ 0xec78e02d, __VMLINUX_SYMBOL_STR(snd_hda_build_controls) },
	{ 0xf4c91ed, __VMLINUX_SYMBOL_STR(ns_to_timespec) },
	{ 0x2fc8ae76, __VMLINUX_SYMBOL_STR(snd_pcm_hw_constraint_integer) },
	{ 0x82def7f2, __VMLINUX_SYMBOL_STR(snd_hda_resume) },
	{ 0xd97b7cd3, __VMLINUX_SYMBOL_STR(snd_card_disconnect) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0xcb58edff, __VMLINUX_SYMBOL_STR(snd_pcm_suspend_all) },
	{ 0x40256835, __VMLINUX_SYMBOL_STR(complete_all) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x35b6b772, __VMLINUX_SYMBOL_STR(param_ops_charp) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0x50d1f870, __VMLINUX_SYMBOL_STR(pgprot_writecombine) },
	{ 0xfd6ef57d, __VMLINUX_SYMBOL_STR(trace_event_buffer_lock_reserve) },
	{ 0x3c0a7e36, __VMLINUX_SYMBOL_STR(trace_define_field) },
	{ 0x72a1c621, __VMLINUX_SYMBOL_STR(snd_pcm_sgbuf_ops_page) },
	{ 0x1521b79c, __VMLINUX_SYMBOL_STR(snd_device_new) },
	{ 0xce748240, __VMLINUX_SYMBOL_STR(snd_hda_suspend) },
	{ 0xc2ac79c5, __VMLINUX_SYMBOL_STR(pci_restore_state) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x1a8e40c, __VMLINUX_SYMBOL_STR(snd_pcm_set_ops) },
	{ 0x5e4b3b08, __VMLINUX_SYMBOL_STR(snd_hda_codec_configure) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x8e4c6826, __VMLINUX_SYMBOL_STR(snd_hda_calc_stream_format) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x4bd795e7, __VMLINUX_SYMBOL_STR(snd_pcm_lib_free_pages) },
	{ 0xd6b8e852, __VMLINUX_SYMBOL_STR(request_threaded_irq) },
	{ 0xe086d97e, __VMLINUX_SYMBOL_STR(snd_pcm_lib_ioctl) },
	{ 0x3f53f985, __VMLINUX_SYMBOL_STR(snd_pcm_lib_malloc_pages) },
	{ 0xe523ad75, __VMLINUX_SYMBOL_STR(synchronize_irq) },
	{ 0xbfc407b4, __VMLINUX_SYMBOL_STR(param_ops_bint) },
	{ 0x74954462, __VMLINUX_SYMBOL_STR(timecounter_read) },
	{ 0x8021861a, __VMLINUX_SYMBOL_STR(snd_hda_spdif_out_of_nid) },
	{ 0xac1a55be, __VMLINUX_SYMBOL_STR(unregister_reboot_notifier) },
	{ 0xf941442, __VMLINUX_SYMBOL_STR(ftrace_event_reg) },
	{ 0xf5e3fd9a, __VMLINUX_SYMBOL_STR(set_pages_array_wb) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0x952c5e83, __VMLINUX_SYMBOL_STR(pci_bus_read_config_word) },
	{ 0x71f696dd, __VMLINUX_SYMBOL_STR(snd_hda_queue_unsol_event) },
	{ 0x3517383e, __VMLINUX_SYMBOL_STR(register_reboot_notifier) },
	{ 0xc09a6f21, __VMLINUX_SYMBOL_STR(pci_intx) },
	{ 0x331335d4, __VMLINUX_SYMBOL_STR(snd_hda_bus_reboot_notify) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0x337e4587, __VMLINUX_SYMBOL_STR(snd_dma_free_pages) },
	{ 0x3616c35, __VMLINUX_SYMBOL_STR(snd_hda_codec_new) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
	{ 0x68fe373f, __VMLINUX_SYMBOL_STR(snd_pcm_set_sync) },
	{ 0x904b4a5e, __VMLINUX_SYMBOL_STR(snd_hda_codec_cleanup) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x959a3cab, __VMLINUX_SYMBOL_STR(pci_ioremap_bar) },
	{ 0xca34c226, __VMLINUX_SYMBOL_STR(snd_sgbuf_get_chunk_size) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xb7c63631, __VMLINUX_SYMBOL_STR(snd_hda_build_pcms) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xb555a29a, __VMLINUX_SYMBOL_STR(pci_set_power_state) },
	{ 0x87f70505, __VMLINUX_SYMBOL_STR(pci_bus_write_config_byte) },
	{ 0x1e047854, __VMLINUX_SYMBOL_STR(warn_slowpath_fmt) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x1edda8be, __VMLINUX_SYMBOL_STR(trace_event_raw_init) },
	{ 0x12bd6c9, __VMLINUX_SYMBOL_STR(pci_request_regions) },
	{ 0x4845c423, __VMLINUX_SYMBOL_STR(param_array_ops) },
	{ 0x35e582ea, __VMLINUX_SYMBOL_STR(pci_disable_msi) },
	{ 0xf65f7fc7, __VMLINUX_SYMBOL_STR(dma_supported) },
	{ 0x6128b5fc, __VMLINUX_SYMBOL_STR(__printk_ratelimit) },
	{ 0x415f6c39, __VMLINUX_SYMBOL_STR(snd_pcm_hw_constraint_minmax) },
	{ 0xedc03953, __VMLINUX_SYMBOL_STR(iounmap) },
	{ 0xe6602e9f, __VMLINUX_SYMBOL_STR(perf_trace_buf_prepare) },
	{ 0x7d731237, __VMLINUX_SYMBOL_STR(snd_hda_bus_new) },
	{ 0x20b80f05, __VMLINUX_SYMBOL_STR(pci_dev_run_wake) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x7628f3c7, __VMLINUX_SYMBOL_STR(this_cpu_off) },
	{ 0x91607d95, __VMLINUX_SYMBOL_STR(set_memory_wb) },
	{ 0x5740285a, __VMLINUX_SYMBOL_STR(pci_get_device) },
	{ 0x64c65f8d, __VMLINUX_SYMBOL_STR(snd_pcm_lib_preallocate_pages_for_all) },
	{ 0xf37f2e3, __VMLINUX_SYMBOL_STR(snd_card_free) },
	{ 0xf774757e, __VMLINUX_SYMBOL_STR(snd_card_register) },
	{ 0x12121db8, __VMLINUX_SYMBOL_STR(pci_dev_put) },
	{ 0x2302a906, __VMLINUX_SYMBOL_STR(snd_pcm_limit_hw_rates) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0xa8228593, __VMLINUX_SYMBOL_STR(snd_pcm_new) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x98616114, __VMLINUX_SYMBOL_STR(pci_enable_msi_block) },
	{ 0x1e3a88fb, __VMLINUX_SYMBOL_STR(trace_seq_printf) },
	{ 0x515f54e8, __VMLINUX_SYMBOL_STR(snd_hda_power_save) },
	{ 0x9647a668, __VMLINUX_SYMBOL_STR(pci_enable_device) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xcdabc909, __VMLINUX_SYMBOL_STR(ftrace_raw_output_prep) },
	{ 0x498d293a, __VMLINUX_SYMBOL_STR(trace_buffer_unlock_commit) },
	{ 0xf20dabd8, __VMLINUX_SYMBOL_STR(free_irq) },
	{ 0xea4b8c8a, __VMLINUX_SYMBOL_STR(pci_save_state) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-hda-codec,snd-pcm,snd,snd-page-alloc";

MODULE_ALIAS("pci:v00008086d00001C20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001D20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001E20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008C20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D21sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C21sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000A0Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000C0Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000D0Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003B56sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000811Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000080Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002668sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000027D8sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000269Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000284Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000293Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000293Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003A3Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003A6Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d*sv*sd*bc04sc03i00*");
MODULE_ALIAS("pci:v00001002d0000437Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00004383sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001022d0000780Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000793Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00007919sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000960Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000970Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA00sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA08sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA10sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA18sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA28sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA30sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA38sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA40sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AA48sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00009902sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AAA0sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AAA8sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d0000AAB0sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001106d00003288sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001106d00009170sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001106d00009140sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001039d00007502sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010B9d00005461sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd*sv*sd*bc04sc03i00*");
MODULE_ALIAS("pci:v00006549d00001200sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00006549d00002200sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001102d00000010sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001102d00000012sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001102d00000009sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000017F3d00003010sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000015ADd00001977sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d*sv*sd*bc04sc03i00*");
MODULE_ALIAS("pci:v00001022d*sv*sd*bc04sc03i00*");

MODULE_INFO(srcversion, "A8F6896E9B4188B1ABF1E71");

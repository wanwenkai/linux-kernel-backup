#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x496ea286, __VMLINUX_SYMBOL_STR(crypto_register_template) },
	{ 0xac80e43f, __VMLINUX_SYMBOL_STR(aead_geniv_init) },
	{ 0xb782dac7, __VMLINUX_SYMBOL_STR(aead_geniv_exit) },
	{ 0xec5a9013, __VMLINUX_SYMBOL_STR(aead_geniv_alloc) },
	{ 0xabac6778, __VMLINUX_SYMBOL_STR(skcipher_geniv_exit) },
	{ 0xf053212c, __VMLINUX_SYMBOL_STR(skcipher_geniv_alloc) },
	{ 0x6ff607b6, __VMLINUX_SYMBOL_STR(crypto_get_default_rng) },
	{ 0x124f2056, __VMLINUX_SYMBOL_STR(crypto_get_attr_type) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0xde719d41, __VMLINUX_SYMBOL_STR(crypto_default_rng) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x8810ad5e, __VMLINUX_SYMBOL_STR(crypto_xor) },
	{ 0x764ac193, __VMLINUX_SYMBOL_STR(skcipher_geniv_init) },
	{ 0x668402aa, __VMLINUX_SYMBOL_STR(crypto_put_default_rng) },
	{ 0x91254f17, __VMLINUX_SYMBOL_STR(aead_geniv_free) },
	{ 0x61e0032e, __VMLINUX_SYMBOL_STR(skcipher_geniv_free) },
	{ 0x88825490, __VMLINUX_SYMBOL_STR(crypto_unregister_template) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "025AB53F2AFC056562E0FA9");

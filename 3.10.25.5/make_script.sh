#! /bin/bash
make clean

make all -j24 1> tmp 2> err 
make modules -j24 1>> tmp 2>> err
make modules_install -j24 1>> tmp 2>> err
make install -j24 1>> tmp 2>> err

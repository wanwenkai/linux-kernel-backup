#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xa6deeeaa, __VMLINUX_SYMBOL_STR(nfs_unlink) },
	{ 0x3fc59aeb, __VMLINUX_SYMBOL_STR(nfs_symlink) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x9752e335, __VMLINUX_SYMBOL_STR(nfs_refresh_inode) },
	{ 0x15ee6175, __VMLINUX_SYMBOL_STR(nfs_close_context) },
	{ 0xf7d92c4c, __VMLINUX_SYMBOL_STR(nfs_free_client) },
	{ 0x33c5a1e9, __VMLINUX_SYMBOL_STR(nfs_try_mount) },
	{ 0x790e2b62, __VMLINUX_SYMBOL_STR(nfs_permission) },
	{ 0xcdf9b3f7, __VMLINUX_SYMBOL_STR(nfs_post_op_update_inode_force_wcc) },
	{ 0xc52da868, __VMLINUX_SYMBOL_STR(nfs_link) },
	{ 0xedc8b68a, __VMLINUX_SYMBOL_STR(xdr_inline_pages) },
	{ 0xdfa09e61, __VMLINUX_SYMBOL_STR(nfs_instantiate) },
	{ 0x19fa7350, __VMLINUX_SYMBOL_STR(nfs_sops) },
	{ 0xb6f6f272, __VMLINUX_SYMBOL_STR(nfs_setattr_update_inode) },
	{ 0x963ca5b, __VMLINUX_SYMBOL_STR(unregister_nfs_version) },
	{ 0x8e899550, __VMLINUX_SYMBOL_STR(nfs_rmdir) },
	{ 0xafc53c71, __VMLINUX_SYMBOL_STR(xdr_reserve_space) },
	{ 0x6db875c2, __VMLINUX_SYMBOL_STR(nfs_alloc_fattr) },
	{ 0xeeceb206, __VMLINUX_SYMBOL_STR(nfs_fattr_init) },
	{ 0xe34b17ff, __VMLINUX_SYMBOL_STR(xdr_terminate_string) },
	{ 0xef9aec59, __VMLINUX_SYMBOL_STR(nfs_setattr) },
	{ 0x2152bae1, __VMLINUX_SYMBOL_STR(nfs_fs_type) },
	{ 0xfe1f5880, __VMLINUX_SYMBOL_STR(rpc_call_sync) },
	{ 0x211e8fdb, __VMLINUX_SYMBOL_STR(rpc_call_start) },
	{ 0xa032d1cf, __VMLINUX_SYMBOL_STR(register_nfs_version) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x1b5a9278, __VMLINUX_SYMBOL_STR(nfs_getattr) },
	{ 0x321640cd, __VMLINUX_SYMBOL_STR(nfs_invalidate_atime) },
	{ 0x26884ff7, __VMLINUX_SYMBOL_STR(nfs_alloc_fhandle) },
	{ 0xd32fe7a0, __VMLINUX_SYMBOL_STR(nfs_lookup) },
	{ 0x698e1aab, __VMLINUX_SYMBOL_STR(nfs_rename) },
	{ 0x6f2c69d3, __VMLINUX_SYMBOL_STR(nfs_file_operations) },
	{ 0xd18570fd, __VMLINUX_SYMBOL_STR(nfs_mkdir) },
	{ 0x9d811605, __VMLINUX_SYMBOL_STR(nfs_create) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x3d64c9f2, __VMLINUX_SYMBOL_STR(xdr_read_pages) },
	{ 0xe5919cb1, __VMLINUX_SYMBOL_STR(xdr_encode_opaque) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x5b68171d, __VMLINUX_SYMBOL_STR(nfs_init_client) },
	{ 0x2c00641f, __VMLINUX_SYMBOL_STR(nfs_create_server) },
	{ 0x2ed9301d, __VMLINUX_SYMBOL_STR(nfs_mknod) },
	{ 0xc838aaca, __VMLINUX_SYMBOL_STR(nfs_submount) },
	{ 0x6596f764, __VMLINUX_SYMBOL_STR(nfs_pageio_init_read) },
	{ 0xa36e7b21, __VMLINUX_SYMBOL_STR(xdr_inline_decode) },
	{ 0xe1638dd7, __VMLINUX_SYMBOL_STR(nfs_pageio_init_write) },
	{ 0x9e8e911c, __VMLINUX_SYMBOL_STR(nfs_wb_all) },
	{ 0xa0f7a28d, __VMLINUX_SYMBOL_STR(xdr_write_pages) },
	{ 0x8b612bd3, __VMLINUX_SYMBOL_STR(nfs_dentry_operations) },
	{ 0x801ad6b6, __VMLINUX_SYMBOL_STR(nfs_alloc_client) },
	{ 0x29b4e257, __VMLINUX_SYMBOL_STR(nlmclnt_proc) },
	{ 0x8cce9c40, __VMLINUX_SYMBOL_STR(nfs_clone_server) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=nfs,sunrpc,lockd";


MODULE_INFO(srcversion, "0837EE3B872919B0858C96A");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x3445a13, __VMLINUX_SYMBOL_STR(snd_hda_gen_free) },
	{ 0x1bf65c9c, __VMLINUX_SYMBOL_STR(snd_hda_gen_parse_auto_config) },
	{ 0xb7d17e61, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_pcms) },
	{ 0x588dd561, __VMLINUX_SYMBOL_STR(snd_hda_jack_unsol_event) },
	{ 0x2385cdde, __VMLINUX_SYMBOL_STR(snd_hda_add_codec_preset) },
	{ 0x5698ae47, __VMLINUX_SYMBOL_STR(snd_hda_gen_init) },
	{ 0x8fe7eb13, __VMLINUX_SYMBOL_STR(snd_hda_parse_pin_defcfg) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x551252dd, __VMLINUX_SYMBOL_STR(snd_hda_gen_spec_init) },
	{ 0xebafbd63, __VMLINUX_SYMBOL_STR(snd_hda_delete_codec_preset) },
	{ 0x29da3d99, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_controls) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-hda-codec";


MODULE_INFO(srcversion, "5153A36508E1A9BAA9B1052");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xd2f7a010, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_put) },
	{ 0xbfe609d1, __VMLINUX_SYMBOL_STR(snd_hda_enum_helper_info) },
	{ 0x1bf65c9c, __VMLINUX_SYMBOL_STR(snd_hda_gen_parse_auto_config) },
	{ 0x900f7006, __VMLINUX_SYMBOL_STR(snd_hda_apply_fixup) },
	{ 0x4555e99c, __VMLINUX_SYMBOL_STR(snd_hda_pick_fixup) },
	{ 0x5ab12290, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_tlv) },
	{ 0x5528d259, __VMLINUX_SYMBOL_STR(cancel_delayed_work_sync) },
	{ 0xb7d17e61, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_pcms) },
	{ 0x95217b0b, __VMLINUX_SYMBOL_STR(snd_hda_override_conn_list) },
	{ 0x50beee27, __VMLINUX_SYMBOL_STR(snd_hda_check_amp_list_power) },
	{ 0xce155efb, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect) },
	{ 0x588dd561, __VMLINUX_SYMBOL_STR(snd_hda_jack_unsol_event) },
	{ 0x1e88a7c2, __VMLINUX_SYMBOL_STR(snd_hda_codec_set_pincfg) },
	{ 0xc499ae1e, __VMLINUX_SYMBOL_STR(kstrdup) },
	{ 0xa466ee43, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_put) },
	{ 0xf872af8d, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect_enable_callback) },
	{ 0x918ebfe0, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_get) },
	{ 0xf94531a1, __VMLINUX_SYMBOL_STR(snd_hda_gen_hp_automute) },
	{ 0xe71ff1b3, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_get) },
	{ 0x2385cdde, __VMLINUX_SYMBOL_STR(snd_hda_add_codec_preset) },
	{ 0x8c064de3, __VMLINUX_SYMBOL_STR(snd_hda_codec_write) },
	{ 0x80a00229, __VMLINUX_SYMBOL_STR(is_jack_detectable) },
	{ 0x5698ae47, __VMLINUX_SYMBOL_STR(snd_hda_gen_init) },
	{ 0x8fe7eb13, __VMLINUX_SYMBOL_STR(snd_hda_parse_pin_defcfg) },
	{ 0x10311d9c, __VMLINUX_SYMBOL_STR(snd_hda_override_amp_caps) },
	{ 0x3888ac21, __VMLINUX_SYMBOL_STR(snd_ctl_boolean_mono_info) },
	{ 0x6e680abe, __VMLINUX_SYMBOL_STR(snd_hda_gen_line_automute) },
	{ 0x6a209953, __VMLINUX_SYMBOL_STR(snd_hda_jack_tbl_get) },
	{ 0xacd73f87, __VMLINUX_SYMBOL_STR(snd_hda_sequence_write) },
	{ 0x7ebc6401, __VMLINUX_SYMBOL_STR(snd_hda_gen_spec_free) },
	{ 0x53f01b6b, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x9c27f124, __VMLINUX_SYMBOL_STR(snd_hda_codec_get_pincfg) },
	{ 0xd37f25a, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_info) },
	{ 0x551252dd, __VMLINUX_SYMBOL_STR(snd_hda_gen_spec_init) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x7ba6bc09, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_info) },
	{ 0xebafbd63, __VMLINUX_SYMBOL_STR(snd_hda_delete_codec_preset) },
	{ 0x29da3d99, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_controls) },
	{ 0x31da3d1f, __VMLINUX_SYMBOL_STR(snd_hda_codec_read) },
	{ 0xdab68a8b, __VMLINUX_SYMBOL_STR(snd_hda_codec_amp_read) },
	{ 0xb8c2853, __VMLINUX_SYMBOL_STR(snd_hda_get_connections) },
	{ 0xa9d4c058, __VMLINUX_SYMBOL_STR(snd_hda_add_new_ctls) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-hda-codec,snd";


MODULE_INFO(srcversion, "EC6C8985FD477CA846DD83D");

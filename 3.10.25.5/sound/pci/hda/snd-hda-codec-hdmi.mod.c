#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x24a94b26, __VMLINUX_SYMBOL_STR(snd_info_get_line) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x22e1b56a, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect_enable) },
	{ 0x32854fd1, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_prepare) },
	{ 0x9a8e1877, __VMLINUX_SYMBOL_STR(snd_array_free) },
	{ 0xc0d6c35, __VMLINUX_SYMBOL_STR(snd_pcm_hw_constraint_step) },
	{ 0x163e6f, __VMLINUX_SYMBOL_STR(snd_device_free) },
	{ 0xbe7dd7dc, __VMLINUX_SYMBOL_STR(snd_array_new) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0xeda3a721, __VMLINUX_SYMBOL_STR(snd_print_pcm_bits) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x88d08bf2, __VMLINUX_SYMBOL_STR(snd_hda_query_supported_pcm) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x9e913c1, __VMLINUX_SYMBOL_STR(snd_pcm_alt_chmaps) },
	{ 0x95217b0b, __VMLINUX_SYMBOL_STR(snd_hda_override_conn_list) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x1f4cc36d, __VMLINUX_SYMBOL_STR(snd_hda_spdif_ctls_unassign) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x2cca1760, __VMLINUX_SYMBOL_STR(snd_hda_query_pin_caps) },
	{ 0x8c74599c, __VMLINUX_SYMBOL_STR(snd_hda_pin_sense) },
	{ 0x416f25b6, __VMLINUX_SYMBOL_STR(cancel_delayed_work) },
	{ 0x86e906b2, __VMLINUX_SYMBOL_STR(snd_pcm_hw_constraint_list) },
	{ 0x2e2b40d2, __VMLINUX_SYMBOL_STR(strncat) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x20c55ae0, __VMLINUX_SYMBOL_STR(sscanf) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0x90b8118c, __VMLINUX_SYMBOL_STR(snd_hda_codec_set_power_to_all) },
	{ 0x2385cdde, __VMLINUX_SYMBOL_STR(snd_hda_add_codec_preset) },
	{ 0xa8d71064, __VMLINUX_SYMBOL_STR(snd_ctl_notify) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x5a921311, __VMLINUX_SYMBOL_STR(strncmp) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x8c064de3, __VMLINUX_SYMBOL_STR(snd_hda_codec_write) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xbecacbba, __VMLINUX_SYMBOL_STR(snd_hda_spdif_ctls_assign) },
	{ 0x80a00229, __VMLINUX_SYMBOL_STR(is_jack_detectable) },
	{ 0x42160169, __VMLINUX_SYMBOL_STR(flush_workqueue) },
	{ 0x4b015768, __VMLINUX_SYMBOL_STR(snd_iprintf) },
	{ 0x8021861a, __VMLINUX_SYMBOL_STR(snd_hda_spdif_out_of_nid) },
	{ 0xacd73f87, __VMLINUX_SYMBOL_STR(snd_hda_sequence_write) },
	{ 0x4d59dc0d, __VMLINUX_SYMBOL_STR(snd_hda_ctl_add) },
	{ 0xf9403390, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_close) },
	{ 0x787276eb, __VMLINUX_SYMBOL_STR(snd_hda_create_dig_out_ctls) },
	{ 0xb2fd5ceb, __VMLINUX_SYMBOL_STR(__put_user_4) },
	{ 0x53f01b6b, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x369b2a3b, __VMLINUX_SYMBOL_STR(snd_hda_codec_write_cache) },
	{ 0x2c81cd66, __VMLINUX_SYMBOL_STR(snd_hda_get_sub_nodes) },
	{ 0xfa4ea467, __VMLINUX_SYMBOL_STR(snd_ctl_new1) },
	{ 0x4b2ca7cd, __VMLINUX_SYMBOL_STR(snd_hda_codec_update_widgets) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x9274913, __VMLINUX_SYMBOL_STR(__snd_hda_codec_cleanup_stream) },
	{ 0xa11f8fe6, __VMLINUX_SYMBOL_STR(snd_hda_jack_add_kctl) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x8c48fada, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_open) },
	{ 0xd013c267, __VMLINUX_SYMBOL_STR(snd_hda_jack_tbl_get_from_tag) },
	{ 0xfdba1436, __VMLINUX_SYMBOL_STR(snd_card_proc_new) },
	{ 0x9c27f124, __VMLINUX_SYMBOL_STR(snd_hda_codec_get_pincfg) },
	{ 0x94e8d55e, __VMLINUX_SYMBOL_STR(snd_pcm_add_chmap_ctls) },
	{ 0x5528903c, __VMLINUX_SYMBOL_STR(snd_hda_codec_setup_stream) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0xbd8b282c, __VMLINUX_SYMBOL_STR(snd_hda_jack_set_dirty_all) },
	{ 0x36d14bca, __VMLINUX_SYMBOL_STR(snd_hda_jack_report_sync) },
	{ 0xebafbd63, __VMLINUX_SYMBOL_STR(snd_hda_delete_codec_preset) },
	{ 0x31da3d1f, __VMLINUX_SYMBOL_STR(snd_hda_codec_read) },
	{ 0xb8c2853, __VMLINUX_SYMBOL_STR(snd_hda_get_connections) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd,snd-hda-codec,snd-pcm";


MODULE_INFO(srcversion, "AE7498E3A00FB03D39F67DD");

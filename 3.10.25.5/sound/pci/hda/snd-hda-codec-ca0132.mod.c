#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x22e1b56a, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect_enable) },
	{ 0x32854fd1, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_prepare) },
	{ 0xd2f7a010, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_put) },
	{ 0x28ca6784, __VMLINUX_SYMBOL_STR(snd_hda_codec_amp_stereo) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x5ab12290, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_tlv) },
	{ 0xeed84fd1, __VMLINUX_SYMBOL_STR(snd_hda_jack_add_kctls) },
	{ 0x5528d259, __VMLINUX_SYMBOL_STR(cancel_delayed_work_sync) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xce155efb, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xa466ee43, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_put) },
	{ 0x918ebfe0, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_get) },
	{ 0x1253059c, __VMLINUX_SYMBOL_STR(_snd_hda_set_pin_ctl) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xe71ff1b3, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_get) },
	{ 0x2385cdde, __VMLINUX_SYMBOL_STR(snd_hda_add_codec_preset) },
	{ 0x8c064de3, __VMLINUX_SYMBOL_STR(snd_hda_codec_write) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x21335210, __VMLINUX_SYMBOL_STR(snd_hda_create_spdif_in_ctls) },
	{ 0x8fe7eb13, __VMLINUX_SYMBOL_STR(snd_hda_parse_pin_defcfg) },
	{ 0x10311d9c, __VMLINUX_SYMBOL_STR(snd_hda_override_amp_caps) },
	{ 0xacd73f87, __VMLINUX_SYMBOL_STR(snd_hda_sequence_write) },
	{ 0xb7802a44, __VMLINUX_SYMBOL_STR(snd_hda_create_spdif_share_sw) },
	{ 0x4d59dc0d, __VMLINUX_SYMBOL_STR(snd_hda_ctl_add) },
	{ 0xf9403390, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_close) },
	{ 0x787276eb, __VMLINUX_SYMBOL_STR(snd_hda_create_dig_out_ctls) },
	{ 0x53f01b6b, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0xfa4ea467, __VMLINUX_SYMBOL_STR(snd_ctl_new1) },
	{ 0xc05aa975, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_cleanup) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x8c48fada, __VMLINUX_SYMBOL_STR(snd_hda_multi_out_dig_open) },
	{ 0xd013c267, __VMLINUX_SYMBOL_STR(snd_hda_jack_tbl_get_from_tag) },
	{ 0xd37f25a, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_switch_info) },
	{ 0x515f54e8, __VMLINUX_SYMBOL_STR(snd_hda_power_save) },
	{ 0x7ba6bc09, __VMLINUX_SYMBOL_STR(snd_hda_mixer_amp_volume_info) },
	{ 0x36d14bca, __VMLINUX_SYMBOL_STR(snd_hda_jack_report_sync) },
	{ 0xebafbd63, __VMLINUX_SYMBOL_STR(snd_hda_delete_codec_preset) },
	{ 0x31da3d1f, __VMLINUX_SYMBOL_STR(snd_hda_codec_read) },
	{ 0xa9d4c058, __VMLINUX_SYMBOL_STR(snd_hda_add_new_ctls) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
	{ 0x6b6f378a, __VMLINUX_SYMBOL_STR(snd_hda_codec_amp_update) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-hda-codec,snd";


MODULE_INFO(srcversion, "6E2D421A9F9CDBCE913AF85");

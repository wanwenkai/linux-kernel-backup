#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x537211a4, __VMLINUX_SYMBOL_STR(snd_emux_new) },
	{ 0x3b448a8f, __VMLINUX_SYMBOL_STR(snd_emu10k1_synth_alloc) },
	{ 0xe449ad56, __VMLINUX_SYMBOL_STR(snd_emux_register) },
	{ 0x6df2f854, __VMLINUX_SYMBOL_STR(snd_emu10k1_memblk_map) },
	{ 0xc0f98a0a, __VMLINUX_SYMBOL_STR(snd_emu10k1_voice_alloc) },
	{ 0x8ab71987, __VMLINUX_SYMBOL_STR(snd_emu10k1_synth_bzero) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0xc622fb29, __VMLINUX_SYMBOL_STR(snd_seq_device_unregister_driver) },
	{ 0x955e4e08, __VMLINUX_SYMBOL_STR(snd_emux_free) },
	{ 0x2fcf2624, __VMLINUX_SYMBOL_STR(snd_emu10k1_ptr_write) },
	{ 0x8193842c, __VMLINUX_SYMBOL_STR(snd_emu10k1_synth_free) },
	{ 0xa8f93b3f, __VMLINUX_SYMBOL_STR(snd_seq_device_register_driver) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x909b9d11, __VMLINUX_SYMBOL_STR(snd_emu10k1_voice_free) },
	{ 0x59e63730, __VMLINUX_SYMBOL_STR(snd_emu10k1_synth_copy_from_user) },
	{ 0x9a6eb551, __VMLINUX_SYMBOL_STR(snd_emu10k1_ptr_read) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-emux-synth,snd-emu10k1,snd-seq-device";


MODULE_INFO(srcversion, "BF76A0340FD12EB50B3F57C");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x1b7f0549, __VMLINUX_SYMBOL_STR(rt2x00mac_tx_frames_pending) },
	{ 0xa70d4438, __VMLINUX_SYMBOL_STR(rt2x00mac_get_ringparam) },
	{ 0x9da0490c, __VMLINUX_SYMBOL_STR(rt2x00mac_get_antenna) },
	{ 0x7743033d, __VMLINUX_SYMBOL_STR(rt2x00mac_set_antenna) },
	{ 0xb23d4a80, __VMLINUX_SYMBOL_STR(rt2x00mac_flush) },
	{ 0x93ed78df, __VMLINUX_SYMBOL_STR(rt2x00mac_rfkill_poll) },
	{ 0x31a6c6f5, __VMLINUX_SYMBOL_STR(rt2x00mac_conf_tx) },
	{ 0xcfa9bda3, __VMLINUX_SYMBOL_STR(rt2x00mac_get_stats) },
	{ 0xc0e0d180, __VMLINUX_SYMBOL_STR(rt2x00mac_sw_scan_complete) },
	{ 0x19b89f70, __VMLINUX_SYMBOL_STR(rt2x00mac_sw_scan_start) },
	{ 0x1bb2697e, __VMLINUX_SYMBOL_STR(rt2x00mac_set_key) },
	{ 0x3ed3a308, __VMLINUX_SYMBOL_STR(rt2x00mac_set_tim) },
	{ 0x8c592a6f, __VMLINUX_SYMBOL_STR(rt2x00mac_configure_filter) },
	{ 0x79cf3922, __VMLINUX_SYMBOL_STR(rt2x00mac_bss_info_changed) },
	{ 0xca8743bd, __VMLINUX_SYMBOL_STR(rt2x00mac_config) },
	{ 0x9952514e, __VMLINUX_SYMBOL_STR(rt2x00mac_remove_interface) },
	{ 0x62a85d7b, __VMLINUX_SYMBOL_STR(rt2x00mac_add_interface) },
	{ 0xecb72f54, __VMLINUX_SYMBOL_STR(rt2x00mac_stop) },
	{ 0xbcc4d972, __VMLINUX_SYMBOL_STR(rt2x00mac_start) },
	{ 0x402e60f0, __VMLINUX_SYMBOL_STR(rt2x00mac_tx) },
	{ 0xfa1ecdf8, __VMLINUX_SYMBOL_STR(rt2x00usb_flush_queue) },
	{ 0xde6310b5, __VMLINUX_SYMBOL_STR(rt2x00usb_kick_queue) },
	{ 0xea7fb2c7, __VMLINUX_SYMBOL_STR(rt2x00usb_watchdog) },
	{ 0xa2e03549, __VMLINUX_SYMBOL_STR(rt2x00usb_clear_entry) },
	{ 0x4617964b, __VMLINUX_SYMBOL_STR(rt2x00usb_uninitialize) },
	{ 0x21ebe334, __VMLINUX_SYMBOL_STR(rt2x00usb_initialize) },
	{ 0xb9c251c7, __VMLINUX_SYMBOL_STR(rt2x00usb_resume) },
	{ 0xaeba462a, __VMLINUX_SYMBOL_STR(rt2x00usb_suspend) },
	{ 0x54193b2c, __VMLINUX_SYMBOL_STR(rt2x00usb_disconnect) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xb3d01079, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x79aa04a2, __VMLINUX_SYMBOL_STR(get_random_bytes) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x93e96038, __VMLINUX_SYMBOL_STR(rt2x00usb_disable_radio) },
	{ 0xfd24d9ae, __VMLINUX_SYMBOL_STR(rt2x00usb_vendor_request) },
	{ 0x33d53d62, __VMLINUX_SYMBOL_STR(consume_skb) },
	{ 0xf503b80a, __VMLINUX_SYMBOL_STR(usb_submit_urb) },
	{ 0xe6961bf3, __VMLINUX_SYMBOL_STR(skb_push) },
	{ 0xf00f8562, __VMLINUX_SYMBOL_STR(skb_trim) },
	{ 0x28fe3048, __VMLINUX_SYMBOL_STR(rt2x00usb_vendor_request_buff) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xd61bc8cc, __VMLINUX_SYMBOL_STR(rt2x00usb_vendor_req_buff_lock) },
	{ 0x5fbf5763, __VMLINUX_SYMBOL_STR(rt2x00usb_probe) },
	{ 0xdf01d54, __VMLINUX_SYMBOL_STR(usb_deregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=rt2x00lib,rt2x00usb";

MODULE_ALIAS("usb:v0B05p1706d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0B05p1707d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v050Dp7050d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v050Dp7051d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v13B1p000Dd*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v13B1p0011d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v13B1p001Ad*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v14B2p3C02d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v2001p3C00d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v1044p8001d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v1044p8007d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v06F8pE000d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0411p005Ed*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0411p0066d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0411p0067d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0411p008Bd*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0411p0097d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0DB0p6861d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0DB0p6865d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0DB0p6869d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v148Fp1706d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v148Fp2570d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v148Fp9020d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v079Bp004Bd*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0681p3C06d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0707pEE13d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v114Bp0110d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0769p11F3d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0EB0p9020d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0F88p3012d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v5A57p0260d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "3DFFB4A2A254722B4189F50");

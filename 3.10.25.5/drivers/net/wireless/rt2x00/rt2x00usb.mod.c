#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x65dcba97, __VMLINUX_SYMBOL_STR(skb_pad) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0x51a1d62f, __VMLINUX_SYMBOL_STR(usb_kill_urb) },
	{ 0x5a014da4, __VMLINUX_SYMBOL_STR(rt2x00lib_resume) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x339eaece, __VMLINUX_SYMBOL_STR(rt2x00lib_dmastart) },
	{ 0xe3ecd4a8, __VMLINUX_SYMBOL_STR(rt2x00queue_flush_queue) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xf0119601, __VMLINUX_SYMBOL_STR(usb_control_msg) },
	{ 0xde45567f, __VMLINUX_SYMBOL_STR(rt2x00queue_for_each_entry) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x3f3a4a47, __VMLINUX_SYMBOL_STR(rt2x00lib_remove_dev) },
	{ 0xf503b80a, __VMLINUX_SYMBOL_STR(usb_submit_urb) },
	{ 0x5a7fc92c, __VMLINUX_SYMBOL_STR(usb_get_dev) },
	{ 0x4cfcd605, __VMLINUX_SYMBOL_STR(rt2x00queue_get_entry) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xcc9b9148, __VMLINUX_SYMBOL_STR(usb_reset_device) },
	{ 0x5b3163cf, __VMLINUX_SYMBOL_STR(rt2x00lib_rxdone) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x8c2b3430, __VMLINUX_SYMBOL_STR(usb_put_dev) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x76e2a2ec, __VMLINUX_SYMBOL_STR(ieee80211_alloc_hw) },
	{ 0x22a4778a, __VMLINUX_SYMBOL_STR(hrtimer_init) },
	{ 0x4cb511e7, __VMLINUX_SYMBOL_STR(ieee80211_free_hw) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0xbf79a950, __VMLINUX_SYMBOL_STR(rt2x00lib_txdone_noinfo) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x456f0451, __VMLINUX_SYMBOL_STR(rt2x00lib_dmadone) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x32eaa242, __VMLINUX_SYMBOL_STR(usb_free_urb) },
	{ 0x9b3ad659, __VMLINUX_SYMBOL_STR(rt2x00lib_probe_dev) },
	{ 0xaed0a438, __VMLINUX_SYMBOL_STR(rt2x00lib_suspend) },
	{ 0xfc44da0f, __VMLINUX_SYMBOL_STR(usb_alloc_urb) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=rt2x00lib,mac80211";


MODULE_INFO(srcversion, "2FEBDEB5026D80AF383E3AD");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x34d425ec, __VMLINUX_SYMBOL_STR(genphy_read_status) },
	{ 0x767f0a78, __VMLINUX_SYMBOL_STR(phy_drivers_register) },
	{ 0x253f8f33, __VMLINUX_SYMBOL_STR(genphy_update_link) },
	{ 0xa2838db8, __VMLINUX_SYMBOL_STR(genphy_config_aneg) },
	{ 0x18ec44cc, __VMLINUX_SYMBOL_STR(mdiobus_read) },
	{ 0x8393b6a6, __VMLINUX_SYMBOL_STR(mdiobus_write) },
	{ 0x47027c1b, __VMLINUX_SYMBOL_STR(phy_drivers_unregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libphy";

MODULE_ALIAS("mdio:0000000101000001000011000110????");
MODULE_ALIAS("mdio:0000000101000001000011001001????");
MODULE_ALIAS("mdio:0000000101000001000011001100????");
MODULE_ALIAS("mdio:0000000101000001000011100001????");
MODULE_ALIAS("mdio:0000000101000001000011001011????");
MODULE_ALIAS("mdio:0000000101000001000011001101????");
MODULE_ALIAS("mdio:0000000101000001000011100101????");
MODULE_ALIAS("mdio:0000000101000001000011100011????");
MODULE_ALIAS("mdio:0000000101000001000011101001????");

MODULE_INFO(srcversion, "A4B054857DC2FC8D76574B8");

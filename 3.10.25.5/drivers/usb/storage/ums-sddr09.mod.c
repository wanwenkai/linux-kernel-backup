#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x736a4855, __VMLINUX_SYMBOL_STR(usb_stor_set_xfer_buf) },
	{ 0xbd8fa773, __VMLINUX_SYMBOL_STR(usb_stor_bulk_transfer_buf) },
	{ 0x15779369, __VMLINUX_SYMBOL_STR(usb_stor_access_xfer_buf) },
	{ 0xc8ba7611, __VMLINUX_SYMBOL_STR(usb_stor_post_reset) },
	{ 0x4c17e012, __VMLINUX_SYMBOL_STR(usb_reset_configuration) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xe21d5ea8, __VMLINUX_SYMBOL_STR(usb_stor_disconnect) },
	{ 0xdf01d54, __VMLINUX_SYMBOL_STR(usb_deregister) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x802fe80a, __VMLINUX_SYMBOL_STR(usb_stor_probe2) },
	{ 0x6cadf87b, __VMLINUX_SYMBOL_STR(fill_inquiry_response) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x7ba045c7, __VMLINUX_SYMBOL_STR(usb_stor_pre_reset) },
	{ 0xa861e116, __VMLINUX_SYMBOL_STR(usb_stor_reset_resume) },
	{ 0x6e274688, __VMLINUX_SYMBOL_STR(usb_stor_ctrl_transfer) },
	{ 0x8236dd51, __VMLINUX_SYMBOL_STR(usb_stor_suspend) },
	{ 0x137ac5b8, __VMLINUX_SYMBOL_STR(usb_stor_CB_reset) },
	{ 0xefd625d5, __VMLINUX_SYMBOL_STR(usb_stor_probe1) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x69c3285f, __VMLINUX_SYMBOL_STR(usb_stor_resume) },
	{ 0xb3d01079, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0x9f3dbc2, __VMLINUX_SYMBOL_STR(usb_stor_CB_transport) },
	{ 0xbda8afbe, __VMLINUX_SYMBOL_STR(usb_stor_bulk_transfer_sg) },
	{ 0xcf0438d1, __VMLINUX_SYMBOL_STR(usb_stor_bulk_srb) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=usb-storage";

MODULE_ALIAS("usb:v0436p0005d0100dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v04E6p0003d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v04E6p0005d020[0-8]dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v04E6p0005d01*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v066Bp0105d0100dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0781p0200d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v07AFp0006d0100dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "DE679526C3E3F1664FC70C1");

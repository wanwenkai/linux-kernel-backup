#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xc69dd24f, __VMLINUX_SYMBOL_STR(usb_hcd_pci_pm_ops) },
	{ 0x41d2857, __VMLINUX_SYMBOL_STR(usb_hcd_pci_shutdown) },
	{ 0x3ba0cccf, __VMLINUX_SYMBOL_STR(usb_hcd_pci_remove) },
	{ 0x4c3e4755, __VMLINUX_SYMBOL_STR(usb_hcd_pci_probe) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x19a304ba, __VMLINUX_SYMBOL_STR(usb_disabled) },
	{ 0xc17515d7, __VMLINUX_SYMBOL_STR(usb_hcds_loaded) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0xd4ed8b1a, __VMLINUX_SYMBOL_STR(usb_hc_died) },
	{ 0x937d9af6, __VMLINUX_SYMBOL_STR(pci_get_slot) },
	{ 0xbacde865, __VMLINUX_SYMBOL_STR(device_init_wakeup) },
	{ 0x12121db8, __VMLINUX_SYMBOL_STR(pci_dev_put) },
	{ 0x5740285a, __VMLINUX_SYMBOL_STR(pci_get_device) },
	{ 0x27c1e63f, __VMLINUX_SYMBOL_STR(usb_amd_find_chipset_info) },
	{ 0x8b0e7bc4, __VMLINUX_SYMBOL_STR(pci_match_id) },
	{ 0xc9268b74, __VMLINUX_SYMBOL_STR(usb_hcd_resume_root_hub) },
	{ 0x5a7d3ae7, __VMLINUX_SYMBOL_STR(usb_hcd_poll_rh_status) },
	{ 0xa13c2925, __VMLINUX_SYMBOL_STR(usb_root_hub_lost_power) },
	{ 0x72a3ccf8, __VMLINUX_SYMBOL_STR(dma_pool_create) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0xb5aa7165, __VMLINUX_SYMBOL_STR(dma_pool_destroy) },
	{ 0xb9b9df41, __VMLINUX_SYMBOL_STR(usb_amd_dev_put) },
	{ 0xcca27eeb, __VMLINUX_SYMBOL_STR(del_timer) },
	{ 0xf20dabd8, __VMLINUX_SYMBOL_STR(free_irq) },
	{ 0x8487a2b6, __VMLINUX_SYMBOL_STR(flush_work) },
	{ 0xdc825d6c, __VMLINUX_SYMBOL_STR(usb_amd_quirk_pll_disable) },
	{ 0xec9d06aa, __VMLINUX_SYMBOL_STR(usb_hcd_link_urb_to_ep) },
	{ 0x7e64181d, __VMLINUX_SYMBOL_STR(usb_calc_bus_time) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x771cf835, __VMLINUX_SYMBOL_STR(dma_pool_alloc) },
	{ 0x8170740, __VMLINUX_SYMBOL_STR(usb_hcd_check_unlink_urb) },
	{ 0xd0ee38b8, __VMLINUX_SYMBOL_STR(schedule_timeout_uninterruptible) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0xe08dd8d1, __VMLINUX_SYMBOL_STR(device_set_wakeup_capable) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xc8fd727e, __VMLINUX_SYMBOL_STR(mod_timer) },
	{ 0x91eb9b4, __VMLINUX_SYMBOL_STR(round_jiffies) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xe82af09c, __VMLINUX_SYMBOL_STR(usb_hcd_giveback_urb) },
	{ 0x75c54af2, __VMLINUX_SYMBOL_STR(usb_hcd_unlink_urb_from_ep) },
	{ 0x10ecc52c, __VMLINUX_SYMBOL_STR(usb_amd_quirk_pll_enable) },
	{ 0x24939c32, __VMLINUX_SYMBOL_STR(pci_bus_write_config_word) },
	{ 0x952c5e83, __VMLINUX_SYMBOL_STR(pci_bus_read_config_word) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x2a37d074, __VMLINUX_SYMBOL_STR(dma_pool_free) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v*d*sv*sd*bc0Csc03i10*");
MODULE_ALIAS("pci:v0000104Ad0000CC01sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "60A77080A99D32A39F375A4");

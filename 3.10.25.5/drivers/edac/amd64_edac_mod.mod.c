#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xcf6cfe1f, __VMLINUX_SYMBOL_STR(msrs_free) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x6fb3c8e8, __VMLINUX_SYMBOL_STR(edac_pci_release_generic_ctl) },
	{ 0x950ffff2, __VMLINUX_SYMBOL_STR(cpu_online_mask) },
	{ 0xc715d9e0, __VMLINUX_SYMBOL_STR(boot_cpu_data) },
	{ 0xc0a3d105, __VMLINUX_SYMBOL_STR(find_next_bit) },
	{ 0x3d7c1ed7, __VMLINUX_SYMBOL_STR(msrs_alloc) },
	{ 0xe8367c2d, __VMLINUX_SYMBOL_STR(free_cpumask_var) },
	{ 0xfe7c4287, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0xfd1ec33c, __VMLINUX_SYMBOL_STR(amd_northbridges) },
	{ 0xb51fbd64, __VMLINUX_SYMBOL_STR(edac_op_state) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xcabe18d9, __VMLINUX_SYMBOL_STR(rdmsr_on_cpus) },
	{ 0x5587e353, __VMLINUX_SYMBOL_STR(edac_mc_handle_error) },
	{ 0x14878009, __VMLINUX_SYMBOL_STR(amd_report_gart_errors) },
	{ 0xfdc59a0a, __VMLINUX_SYMBOL_STR(zalloc_cpumask_var) },
	{ 0x21626132, __VMLINUX_SYMBOL_STR(amd_register_ecc_decoder) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x21ca769b, __VMLINUX_SYMBOL_STR(pci_bus_write_config_dword) },
	{ 0x73e112ce, __VMLINUX_SYMBOL_STR(edac_mc_free) },
	{ 0x5ebe3824, __VMLINUX_SYMBOL_STR(wrmsr_on_cpus) },
	{ 0x9f46ced8, __VMLINUX_SYMBOL_STR(__sw_hweight64) },
	{ 0x69259f22, __VMLINUX_SYMBOL_STR(edac_mem_types) },
	{ 0xde0cd16a, __VMLINUX_SYMBOL_STR(edac_mc_alloc) },
	{ 0xcc260e15, __VMLINUX_SYMBOL_STR(edac_pci_create_generic_ctl) },
	{ 0x658c8186, __VMLINUX_SYMBOL_STR(amd_get_nb_id) },
	{ 0x6bf14d96, __VMLINUX_SYMBOL_STR(find_mci_by_dev) },
	{ 0x28557954, __VMLINUX_SYMBOL_STR(pci_bus_read_config_dword) },
	{ 0xf780536b, __VMLINUX_SYMBOL_STR(edac_mc_add_mc) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x32047ad5, __VMLINUX_SYMBOL_STR(__per_cpu_offset) },
	{ 0x1e047854, __VMLINUX_SYMBOL_STR(warn_slowpath_fmt) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xd919806a, __VMLINUX_SYMBOL_STR(amd_cache_northbridges) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x737912b3, __VMLINUX_SYMBOL_STR(amd_unregister_ecc_decoder) },
	{ 0x74c134b9, __VMLINUX_SYMBOL_STR(__sw_hweight32) },
	{ 0x5740285a, __VMLINUX_SYMBOL_STR(pci_get_device) },
	{ 0x302e505c, __VMLINUX_SYMBOL_STR(edac_mc_find) },
	{ 0x3df445bf, __VMLINUX_SYMBOL_STR(edac_mc_del_mc) },
	{ 0x12121db8, __VMLINUX_SYMBOL_STR(pci_dev_put) },
	{ 0x9647a668, __VMLINUX_SYMBOL_STR(pci_enable_device) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=edac_core";

MODULE_ALIAS("pci:v00001022d00001102sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001022d00001202sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001022d00001602sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001022d00001532sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "90DE4EB7213F191AF577165");

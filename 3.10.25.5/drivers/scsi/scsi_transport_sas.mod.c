#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x62d06521, __VMLINUX_SYMBOL_STR(blk_init_queue) },
	{ 0xc34599ee, __VMLINUX_SYMBOL_STR(transport_class_register) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0x8524670e, __VMLINUX_SYMBOL_STR(scsi_remove_target) },
	{ 0x2e133e3e, __VMLINUX_SYMBOL_STR(blk_cleanup_queue) },
	{ 0x2162100b, __VMLINUX_SYMBOL_STR(dev_printk) },
	{ 0x4d9023ce, __VMLINUX_SYMBOL_STR(transport_destroy_device) },
	{ 0x8cbca513, __VMLINUX_SYMBOL_STR(attribute_container_unregister) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x810f7ea6, __VMLINUX_SYMBOL_STR(scsi_scan_target) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x36caf629, __VMLINUX_SYMBOL_STR(scsi_is_host_device) },
	{ 0x620ec38d, __VMLINUX_SYMBOL_STR(device_del) },
	{ 0xd1a07b53, __VMLINUX_SYMBOL_STR(transport_add_device) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xeeb8e41b, __VMLINUX_SYMBOL_STR(bsg_register_queue) },
	{ 0xf49d27b6, __VMLINUX_SYMBOL_STR(transport_configure_device) },
	{ 0x5a921311, __VMLINUX_SYMBOL_STR(strncmp) },
	{ 0x3d8da4a0, __VMLINUX_SYMBOL_STR(attribute_container_register) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x7515e069, __VMLINUX_SYMBOL_STR(sysfs_remove_link) },
	{ 0x20d3df20, __VMLINUX_SYMBOL_STR(device_add) },
	{ 0x5a3b2f59, __VMLINUX_SYMBOL_STR(transport_class_unregister) },
	{ 0x9475bb71, __VMLINUX_SYMBOL_STR(sysfs_create_link) },
	{ 0x17a54f1c, __VMLINUX_SYMBOL_STR(put_device) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0x2aad9969, __VMLINUX_SYMBOL_STR(transport_setup_device) },
	{ 0xd3e433bd, __VMLINUX_SYMBOL_STR(bsg_unregister_queue) },
	{ 0x8e71afe4, __VMLINUX_SYMBOL_STR(blk_fetch_request) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xb0415876, __VMLINUX_SYMBOL_STR(get_device) },
	{ 0xf498dc8c, __VMLINUX_SYMBOL_STR(device_for_each_child) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x3ddcdebf, __VMLINUX_SYMBOL_STR(blk_end_request_all) },
	{ 0xb28c531c, __VMLINUX_SYMBOL_STR(device_initialize) },
	{ 0x63a10b66, __VMLINUX_SYMBOL_STR(scsi_get_vpd_page) },
	{ 0x75da42c8, __VMLINUX_SYMBOL_STR(transport_remove_device) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x291f89b7, __VMLINUX_SYMBOL_STR(dev_set_name) },
	{ 0xb3303502, __VMLINUX_SYMBOL_STR(scsi_mode_sense) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=scsi_mod";


MODULE_INFO(srcversion, "D60904DBE495B32DC8BB8AE");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x1d8c2cae, __VMLINUX_SYMBOL_STR(cpufreq_freq_attr_scaling_available_freqs) },
	{ 0x20694570, __VMLINUX_SYMBOL_STR(cpufreq_register_driver) },
	{ 0xbec30d05, __VMLINUX_SYMBOL_STR(x86_match_cpu) },
	{ 0xafdf88c5, __VMLINUX_SYMBOL_STR(cpufreq_frequency_table_verify) },
	{ 0xe8367c2d, __VMLINUX_SYMBOL_STR(free_cpumask_var) },
	{ 0xc0a3d105, __VMLINUX_SYMBOL_STR(find_next_bit) },
	{ 0xe997667b, __VMLINUX_SYMBOL_STR(wrmsr_on_cpu) },
	{ 0x3400c54f, __VMLINUX_SYMBOL_STR(cpufreq_notify_transition) },
	{ 0xfe7c4287, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0x74ea1265, __VMLINUX_SYMBOL_STR(cpumask_next_and) },
	{ 0x950ffff2, __VMLINUX_SYMBOL_STR(cpu_online_mask) },
	{ 0xf16add4, __VMLINUX_SYMBOL_STR(cpufreq_frequency_table_target) },
	{ 0xfdc59a0a, __VMLINUX_SYMBOL_STR(zalloc_cpumask_var) },
	{ 0xb1cfad22, __VMLINUX_SYMBOL_STR(rdmsr_on_cpu) },
	{ 0x7ae1ae8e, __VMLINUX_SYMBOL_STR(cpufreq_frequency_table_put_attr) },
	{ 0xa4a10505, __VMLINUX_SYMBOL_STR(cpufreq_unregister_driver) },
	{ 0x846f59a, __VMLINUX_SYMBOL_STR(cpu_info) },
	{ 0x32047ad5, __VMLINUX_SYMBOL_STR(__per_cpu_offset) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=freq_table";


MODULE_INFO(srcversion, "A5621497D8359B1FDDACC3F");

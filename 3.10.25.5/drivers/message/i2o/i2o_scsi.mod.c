#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x5a3ccad2, __VMLINUX_SYMBOL_STR(i2o_driver_register) },
	{ 0x1031b7e9, __VMLINUX_SYMBOL_STR(scsi_dma_unmap) },
	{ 0x7e743260, __VMLINUX_SYMBOL_STR(i2o_cntxt_list_get) },
	{ 0x9475bb71, __VMLINUX_SYMBOL_STR(sysfs_create_link) },
	{ 0x2396bbfe, __VMLINUX_SYMBOL_STR(__scsi_add_device) },
	{ 0x40fc66ed, __VMLINUX_SYMBOL_STR(i2o_iop_find_device) },
	{ 0x9bffd2dd, __VMLINUX_SYMBOL_STR(__scsi_iterate_devices) },
	{ 0x83338307, __VMLINUX_SYMBOL_STR(scsi_device_put) },
	{ 0x230885ed, __VMLINUX_SYMBOL_STR(scsi_remove_device) },
	{ 0x7515e069, __VMLINUX_SYMBOL_STR(sysfs_remove_link) },
	{ 0x51973243, __VMLINUX_SYMBOL_STR(scsi_add_host_with_dma) },
	{ 0x6f91d329, __VMLINUX_SYMBOL_STR(i2o_sg_tablesize) },
	{ 0xaddb8893, __VMLINUX_SYMBOL_STR(scsi_host_alloc) },
	{ 0xab3c0014, __VMLINUX_SYMBOL_STR(i2o_parm_field_get) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x65eaba52, __VMLINUX_SYMBOL_STR(i2o_dma_map_sg) },
	{ 0xe3deabb9, __VMLINUX_SYMBOL_STR(i2o_cntxt_list_add) },
	{ 0x4aad52d7, __VMLINUX_SYMBOL_STR(mempool_free) },
	{ 0xe9dff136, __VMLINUX_SYMBOL_STR(mempool_alloc) },
	{ 0x6c137fc1, __VMLINUX_SYMBOL_STR(scsi_cmd_get_serial) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x56b98fee, __VMLINUX_SYMBOL_STR(i2o_msg_post_wait_mem) },
	{ 0x5529ea8f, __VMLINUX_SYMBOL_STR(i2o_cntxt_list_get_ptr) },
	{ 0x55dd99b5, __VMLINUX_SYMBOL_STR(i2o_msg_get_wait) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x28fd2cd3, __VMLINUX_SYMBOL_STR(scsi_host_put) },
	{ 0x5af82fa6, __VMLINUX_SYMBOL_STR(scsi_remove_host) },
	{ 0xae40d1cd, __VMLINUX_SYMBOL_STR(i2o_driver_unregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=i2o_core,scsi_mod";


MODULE_INFO(srcversion, "871319C864972E79FA29E79");

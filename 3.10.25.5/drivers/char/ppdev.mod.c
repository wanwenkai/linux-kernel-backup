#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x12a4e61c, __VMLINUX_SYMBOL_STR(no_llseek) },
	{ 0xce7b64e6, __VMLINUX_SYMBOL_STR(parport_register_driver) },
	{ 0xc0acce4f, __VMLINUX_SYMBOL_STR(__class_create) },
	{ 0xa80b653d, __VMLINUX_SYMBOL_STR(__register_chrdev) },
	{ 0xa9f758c6, __VMLINUX_SYMBOL_STR(parport_read) },
	{ 0x61a93f9, __VMLINUX_SYMBOL_STR(parport_write) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xd2f12c37, __VMLINUX_SYMBOL_STR(parport_set_timeout) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xaff9bd3e, __VMLINUX_SYMBOL_STR(parport_put_port) },
	{ 0x76f54795, __VMLINUX_SYMBOL_STR(parport_register_device) },
	{ 0x24656330, __VMLINUX_SYMBOL_STR(parport_find_number) },
	{ 0x733c3b54, __VMLINUX_SYMBOL_STR(kasprintf) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xdf71c46d, __VMLINUX_SYMBOL_STR(parport_unregister_device) },
	{ 0xfc4e522b, __VMLINUX_SYMBOL_STR(parport_release) },
	{ 0x6e4b6355, __VMLINUX_SYMBOL_STR(parport_negotiate) },
	{ 0x6b2b8e2a, __VMLINUX_SYMBOL_STR(parport_claim_or_block) },
	{ 0x6bc3fbc0, __VMLINUX_SYMBOL_STR(__unregister_chrdev) },
	{ 0xde9239f6, __VMLINUX_SYMBOL_STR(class_destroy) },
	{ 0x4946b0f2, __VMLINUX_SYMBOL_STR(parport_unregister_driver) },
	{ 0x7da04a25, __VMLINUX_SYMBOL_STR(device_create) },
	{ 0x44a9d788, __VMLINUX_SYMBOL_STR(device_destroy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=parport";


MODULE_INFO(srcversion, "33F068D1282A179477800A4");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x9e91ce13, __VMLINUX_SYMBOL_STR(tpm_store_cancel) },
	{ 0xea6452ba, __VMLINUX_SYMBOL_STR(tpm_show_caps) },
	{ 0x8d021dc, __VMLINUX_SYMBOL_STR(tpm_show_pcrs) },
	{ 0xf38c9761, __VMLINUX_SYMBOL_STR(tpm_show_pubek) },
	{ 0xa22f4941, __VMLINUX_SYMBOL_STR(tpm_release) },
	{ 0xf76edb99, __VMLINUX_SYMBOL_STR(tpm_open) },
	{ 0x565fea31, __VMLINUX_SYMBOL_STR(tpm_write) },
	{ 0xa6dc04d5, __VMLINUX_SYMBOL_STR(tpm_read) },
	{ 0x12a4e61c, __VMLINUX_SYMBOL_STR(no_llseek) },
	{ 0x2355ce79, __VMLINUX_SYMBOL_STR(tpm_pm_resume) },
	{ 0x4b6a2018, __VMLINUX_SYMBOL_STR(tpm_pm_suspend) },
	{ 0x594bf15b, __VMLINUX_SYMBOL_STR(ioport_map) },
	{ 0xfb3043c1, __VMLINUX_SYMBOL_STR(tpm_register_hardware) },
	{ 0x6b43df53, __VMLINUX_SYMBOL_STR(platform_device_register_full) },
	{ 0x1fedf0f4, __VMLINUX_SYMBOL_STR(__request_region) },
	{ 0x3c8746c6, __VMLINUX_SYMBOL_STR(platform_driver_register) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x727c4f3, __VMLINUX_SYMBOL_STR(iowrite8) },
	{ 0xf10de535, __VMLINUX_SYMBOL_STR(ioread8) },
	{ 0xa013ba3d, __VMLINUX_SYMBOL_STR(platform_device_unregister) },
	{ 0xadec1761, __VMLINUX_SYMBOL_STR(tpm_remove_hardware) },
	{ 0x7c61340c, __VMLINUX_SYMBOL_STR(__release_region) },
	{ 0xff7559e4, __VMLINUX_SYMBOL_STR(ioport_resource) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x3f34327f, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=tpm";


MODULE_INFO(srcversion, "542D2B8A8356B911B0A2F29");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x9df29988, __VMLINUX_SYMBOL_STR(tpm_show_timeouts) },
	{ 0xf65bea01, __VMLINUX_SYMBOL_STR(tpm_show_durations) },
	{ 0x9e91ce13, __VMLINUX_SYMBOL_STR(tpm_store_cancel) },
	{ 0x32ad38d3, __VMLINUX_SYMBOL_STR(tpm_show_caps_1_2) },
	{ 0xe3e3095c, __VMLINUX_SYMBOL_STR(tpm_show_temp_deactivated) },
	{ 0x383b397b, __VMLINUX_SYMBOL_STR(tpm_show_owned) },
	{ 0x7cef95ec, __VMLINUX_SYMBOL_STR(tpm_show_active) },
	{ 0xb6b19f6f, __VMLINUX_SYMBOL_STR(tpm_show_enabled) },
	{ 0x8d021dc, __VMLINUX_SYMBOL_STR(tpm_show_pcrs) },
	{ 0xf38c9761, __VMLINUX_SYMBOL_STR(tpm_show_pubek) },
	{ 0xa22f4941, __VMLINUX_SYMBOL_STR(tpm_release) },
	{ 0xf76edb99, __VMLINUX_SYMBOL_STR(tpm_open) },
	{ 0x565fea31, __VMLINUX_SYMBOL_STR(tpm_write) },
	{ 0xa6dc04d5, __VMLINUX_SYMBOL_STR(tpm_read) },
	{ 0x12a4e61c, __VMLINUX_SYMBOL_STR(no_llseek) },
	{ 0x4d405db8, __VMLINUX_SYMBOL_STR(param_ops_string) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0x6b43df53, __VMLINUX_SYMBOL_STR(platform_device_register_full) },
	{ 0x3c8746c6, __VMLINUX_SYMBOL_STR(platform_driver_register) },
	{ 0x421a79de, __VMLINUX_SYMBOL_STR(pnp_register_driver) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x88f22cfe, __VMLINUX_SYMBOL_STR(pnpacpi_protocol) },
	{ 0x526444db, __VMLINUX_SYMBOL_STR(pnp_get_resource) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x3b870e30, __VMLINUX_SYMBOL_STR(tpm_gen_interrupt) },
	{ 0xd6b8e852, __VMLINUX_SYMBOL_STR(request_threaded_irq) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x8940c52b, __VMLINUX_SYMBOL_STR(tpm_get_timeouts) },
	{ 0xc5534d64, __VMLINUX_SYMBOL_STR(ioread16) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x42c8de35, __VMLINUX_SYMBOL_STR(ioremap_nocache) },
	{ 0xfb3043c1, __VMLINUX_SYMBOL_STR(tpm_register_hardware) },
	{ 0x5efa04e6, __VMLINUX_SYMBOL_STR(tpm_calc_ordinal_duration) },
	{ 0x1c58f886, __VMLINUX_SYMBOL_STR(freezing_slow_path) },
	{ 0x7ab88a45, __VMLINUX_SYMBOL_STR(system_freezing_cnt) },
	{ 0xfa66f77c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0xd62c833f, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0x5c8b5ce8, __VMLINUX_SYMBOL_STR(prepare_to_wait) },
	{ 0xc8b57c27, __VMLINUX_SYMBOL_STR(autoremove_wake_function) },
	{ 0x4c4fef19, __VMLINUX_SYMBOL_STR(kernel_stack) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xf3bbea1d, __VMLINUX_SYMBOL_STR(wait_for_tpm_stat) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x3f34327f, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xa013ba3d, __VMLINUX_SYMBOL_STR(platform_device_unregister) },
	{ 0xffac5935, __VMLINUX_SYMBOL_STR(pnp_unregister_driver) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xedc03953, __VMLINUX_SYMBOL_STR(iounmap) },
	{ 0xf20dabd8, __VMLINUX_SYMBOL_STR(free_irq) },
	{ 0xadec1761, __VMLINUX_SYMBOL_STR(tpm_remove_hardware) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xf10de535, __VMLINUX_SYMBOL_STR(ioread8) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xf2fb7965, __VMLINUX_SYMBOL_STR(tpm_dev_vendor_release) },
	{ 0x66528486, __VMLINUX_SYMBOL_STR(tpm_do_selftest) },
	{ 0x2355ce79, __VMLINUX_SYMBOL_STR(tpm_pm_resume) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x727c4f3, __VMLINUX_SYMBOL_STR(iowrite8) },
	{ 0x4b6a2018, __VMLINUX_SYMBOL_STR(tpm_pm_suspend) },
	{ 0x436c2179, __VMLINUX_SYMBOL_STR(iowrite32) },
	{ 0xe484e35f, __VMLINUX_SYMBOL_STR(ioread32) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=tpm";

MODULE_ALIAS("pnp:dPNP0C31*");
MODULE_ALIAS("acpi*:PNP0C31:*");
MODULE_ALIAS("pnp:dATM1200*");
MODULE_ALIAS("acpi*:ATM1200:*");
MODULE_ALIAS("pnp:dIFX0102*");
MODULE_ALIAS("acpi*:IFX0102:*");
MODULE_ALIAS("pnp:dBCM0101*");
MODULE_ALIAS("acpi*:BCM0101:*");
MODULE_ALIAS("pnp:dBCM0102*");
MODULE_ALIAS("acpi*:BCM0102:*");
MODULE_ALIAS("pnp:dNSC1200*");
MODULE_ALIAS("acpi*:NSC1200:*");
MODULE_ALIAS("pnp:dICO0102*");
MODULE_ALIAS("acpi*:ICO0102:*");
MODULE_ALIAS("pnp:d*");
MODULE_ALIAS("acpi*::*");

MODULE_INFO(srcversion, "E95F72188F52FB29E2D0E81");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x777d3d25, __VMLINUX_SYMBOL_STR(hwmon_device_register) },
	{ 0x4b269ab6, __VMLINUX_SYMBOL_STR(led_classdev_register) },
	{ 0x43a53735, __VMLINUX_SYMBOL_STR(__alloc_workqueue_key) },
	{ 0xfc780aed, __VMLINUX_SYMBOL_STR(input_register_polled_device) },
	{ 0xa6630be3, __VMLINUX_SYMBOL_STR(input_set_abs_params) },
	{ 0x90a9ba67, __VMLINUX_SYMBOL_STR(input_allocate_polled_device) },
	{ 0x6b43df53, __VMLINUX_SYMBOL_STR(platform_device_register_full) },
	{ 0x3c8746c6, __VMLINUX_SYMBOL_STR(platform_driver_register) },
	{ 0x1fedf0f4, __VMLINUX_SYMBOL_STR(__request_region) },
	{ 0xd4835ef8, __VMLINUX_SYMBOL_STR(dmi_check_system) },
	{ 0xf4efcb02, __VMLINUX_SYMBOL_STR(input_event) },
	{ 0x111bc7a0, __VMLINUX_SYMBOL_STR(sysfs_create_file) },
	{ 0x7c61340c, __VMLINUX_SYMBOL_STR(__release_region) },
	{ 0xff7559e4, __VMLINUX_SYMBOL_STR(ioport_resource) },
	{ 0x3f34327f, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xa013ba3d, __VMLINUX_SYMBOL_STR(platform_device_unregister) },
	{ 0xb3361ad1, __VMLINUX_SYMBOL_STR(hwmon_device_unregister) },
	{ 0x2faf417a, __VMLINUX_SYMBOL_STR(input_free_polled_device) },
	{ 0xb8e5020f, __VMLINUX_SYMBOL_STR(input_unregister_polled_device) },
	{ 0x52b1c573, __VMLINUX_SYMBOL_STR(sysfs_remove_file) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x8c03d20c, __VMLINUX_SYMBOL_STR(destroy_workqueue) },
	{ 0xc57dc556, __VMLINUX_SYMBOL_STR(led_classdev_unregister) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x60ea2d6, __VMLINUX_SYMBOL_STR(kstrtoull) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x9e7d6bd0, __VMLINUX_SYMBOL_STR(__udelay) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=hwmon,input-polldev";

MODULE_ALIAS("dmi*:pn*MacBookAir*:rvn*Apple*:");
MODULE_ALIAS("dmi*:pn*MacBookPro*:rvn*Apple*:");
MODULE_ALIAS("dmi*:pn*MacBook*:rvn*Apple*:");
MODULE_ALIAS("dmi*:pn*Macmini*:rvn*Apple*:");
MODULE_ALIAS("dmi*:pn*MacPro*:rvn*Apple*:");
MODULE_ALIAS("dmi*:pn*iMac*:rvn*Apple*:");

MODULE_INFO(srcversion, "EA4620064779D292F9D780C");

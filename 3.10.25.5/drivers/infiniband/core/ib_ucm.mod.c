#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x57996ff6, __VMLINUX_SYMBOL_STR(cdev_del) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x96b854c8, __VMLINUX_SYMBOL_STR(ib_send_cm_rej) },
	{ 0x8628bd65, __VMLINUX_SYMBOL_STR(cdev_init) },
	{ 0xd8e484f0, __VMLINUX_SYMBOL_STR(register_chrdev_region) },
	{ 0xc8b57c27, __VMLINUX_SYMBOL_STR(autoremove_wake_function) },
	{ 0x3fde1c2e, __VMLINUX_SYMBOL_STR(ib_send_cm_req) },
	{ 0x113e3b15, __VMLINUX_SYMBOL_STR(ib_send_cm_dreq) },
	{ 0x115066a6, __VMLINUX_SYMBOL_STR(ib_send_cm_rtu) },
	{ 0x12a4e61c, __VMLINUX_SYMBOL_STR(no_llseek) },
	{ 0x8f52a40d, __VMLINUX_SYMBOL_STR(kobject_set_name) },
	{ 0x448eac3e, __VMLINUX_SYMBOL_STR(kmemdup) },
	{ 0x18382f6a, __VMLINUX_SYMBOL_STR(ib_copy_path_rec_to_user) },
	{ 0x2f847bc, __VMLINUX_SYMBOL_STR(ib_copy_path_rec_from_user) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x7485e15e, __VMLINUX_SYMBOL_STR(unregister_chrdev_region) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xc399f253, __VMLINUX_SYMBOL_STR(nonseekable_open) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x4eef7554, __VMLINUX_SYMBOL_STR(ib_send_cm_mra) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x6d0aba34, __VMLINUX_SYMBOL_STR(wait_for_completion) },
	{ 0x5fd46b07, __VMLINUX_SYMBOL_STR(ib_send_cm_drep) },
	{ 0x931ee0e0, __VMLINUX_SYMBOL_STR(ib_cm_init_qp_attr) },
	{ 0x45c92723, __VMLINUX_SYMBOL_STR(idr_destroy) },
	{ 0xc61c7f2e, __VMLINUX_SYMBOL_STR(device_register) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x6affeb4c, __VMLINUX_SYMBOL_STR(ib_get_client_data) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x184f3575, __VMLINUX_SYMBOL_STR(ib_copy_qp_attr_to_user) },
	{ 0x721ea6ac, __VMLINUX_SYMBOL_STR(ib_send_cm_rep) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xa46b36fa, __VMLINUX_SYMBOL_STR(class_remove_file) },
	{ 0x4fd5cfb9, __VMLINUX_SYMBOL_STR(idr_alloc) },
	{ 0xef32eb8b, __VMLINUX_SYMBOL_STR(class_create_file) },
	{ 0x2615eddf, __VMLINUX_SYMBOL_STR(ib_set_client_data) },
	{ 0x9e023cd9, __VMLINUX_SYMBOL_STR(ib_create_cm_id) },
	{ 0xf11543ff, __VMLINUX_SYMBOL_STR(find_first_zero_bit) },
	{ 0xcaef1b77, __VMLINUX_SYMBOL_STR(idr_remove) },
	{ 0x7e08f3f1, __VMLINUX_SYMBOL_STR(device_create_file) },
	{ 0x2fd946ba, __VMLINUX_SYMBOL_STR(cdev_add) },
	{ 0x754d9d38, __VMLINUX_SYMBOL_STR(ib_register_client) },
	{ 0x6f36c782, __VMLINUX_SYMBOL_STR(idr_find_slowpath) },
	{ 0x5b24efb7, __VMLINUX_SYMBOL_STR(ib_cm_notify) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x96ce6c46, __VMLINUX_SYMBOL_STR(rdma_node_get_transport) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xeed44943, __VMLINUX_SYMBOL_STR(ib_destroy_cm_id) },
	{ 0x79a12fe5, __VMLINUX_SYMBOL_STR(ib_send_cm_apr) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x4b0f2893, __VMLINUX_SYMBOL_STR(ib_cm_listen) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x5c8b5ce8, __VMLINUX_SYMBOL_STR(prepare_to_wait) },
	{ 0xfa66f77c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0xb489f380, __VMLINUX_SYMBOL_STR(device_unregister) },
	{ 0x9291cd3b, __VMLINUX_SYMBOL_STR(memdup_user) },
	{ 0x4b06d2e7, __VMLINUX_SYMBOL_STR(complete) },
	{ 0x60f9b791, __VMLINUX_SYMBOL_STR(ib_send_cm_lap) },
	{ 0x5fdaddea, __VMLINUX_SYMBOL_STR(cm_class) },
	{ 0x291f89b7, __VMLINUX_SYMBOL_STR(dev_set_name) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0x5a94e280, __VMLINUX_SYMBOL_STR(ib_send_cm_sidr_rep) },
	{ 0xb4d5b009, __VMLINUX_SYMBOL_STR(show_class_attr_string) },
	{ 0x7125c47b, __VMLINUX_SYMBOL_STR(ib_unregister_client) },
	{ 0x29537c9e, __VMLINUX_SYMBOL_STR(alloc_chrdev_region) },
	{ 0x61c55c83, __VMLINUX_SYMBOL_STR(ib_send_cm_sidr_req) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=ib_cm,ib_uverbs,ib_core";


MODULE_INFO(srcversion, "D06FF0DF9A280FBB7C58DA3");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0xc1eb0c37, __VMLINUX_SYMBOL_STR(single_release) },
	{ 0x9c39470d, __VMLINUX_SYMBOL_STR(seq_read) },
	{ 0xd2bcdb9a, __VMLINUX_SYMBOL_STR(seq_lseek) },
	{ 0x4845c423, __VMLINUX_SYMBOL_STR(param_array_ops) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x2e133e3e, __VMLINUX_SYMBOL_STR(blk_cleanup_queue) },
	{ 0x90ce81d5, __VMLINUX_SYMBOL_STR(remove_proc_entry) },
	{ 0xcca27eeb, __VMLINUX_SYMBOL_STR(del_timer) },
	{ 0xedc03953, __VMLINUX_SYMBOL_STR(iounmap) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x42c8de35, __VMLINUX_SYMBOL_STR(ioremap_nocache) },
	{ 0x1fedf0f4, __VMLINUX_SYMBOL_STR(__request_region) },
	{ 0x28557954, __VMLINUX_SYMBOL_STR(pci_bus_read_config_dword) },
	{ 0xe722ff65, __VMLINUX_SYMBOL_STR(pci_bus_read_config_byte) },
	{ 0x952c5e83, __VMLINUX_SYMBOL_STR(pci_bus_read_config_word) },
	{ 0x4a8cbbae, __VMLINUX_SYMBOL_STR(dma_set_mask) },
	{ 0x9647a668, __VMLINUX_SYMBOL_STR(pci_enable_device) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0x7c61340c, __VMLINUX_SYMBOL_STR(__release_region) },
	{ 0xff7559e4, __VMLINUX_SYMBOL_STR(ioport_resource) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0xb5a459dc, __VMLINUX_SYMBOL_STR(unregister_blkdev) },
	{ 0xf20dabd8, __VMLINUX_SYMBOL_STR(free_irq) },
	{ 0x430a89d7, __VMLINUX_SYMBOL_STR(put_disk) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x6a6feb36, __VMLINUX_SYMBOL_STR(blk_queue_max_segments) },
	{ 0x664d950c, __VMLINUX_SYMBOL_STR(blk_queue_bounce_limit) },
	{ 0x7f76e4a5, __VMLINUX_SYMBOL_STR(proc_create_data) },
	{ 0x435e0b50, __VMLINUX_SYMBOL_STR(proc_mkdir) },
	{ 0x62d06521, __VMLINUX_SYMBOL_STR(blk_init_queue) },
	{ 0xbf06b565, __VMLINUX_SYMBOL_STR(alloc_disk) },
	{ 0xd6b8e852, __VMLINUX_SYMBOL_STR(request_threaded_irq) },
	{ 0x71a50dbc, __VMLINUX_SYMBOL_STR(register_blkdev) },
	{ 0xb8e7ce2c, __VMLINUX_SYMBOL_STR(__put_user_8) },
	{ 0xc813450, __VMLINUX_SYMBOL_STR(add_disk) },
	{ 0x5ce6d1ea, __VMLINUX_SYMBOL_STR(blk_queue_logical_block_size) },
	{ 0x777d1ee3, __VMLINUX_SYMBOL_STR(del_gendisk) },
	{ 0xb2fd5ceb, __VMLINUX_SYMBOL_STR(__put_user_4) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x9291cd3b, __VMLINUX_SYMBOL_STR(memdup_user) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x92c962b, __VMLINUX_SYMBOL_STR(__blk_end_request_all) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x51e8a6e0, __VMLINUX_SYMBOL_STR(blk_rq_map_sg) },
	{ 0xc897c382, __VMLINUX_SYMBOL_STR(sg_init_table) },
	{ 0xe94b430c, __VMLINUX_SYMBOL_STR(blk_start_request) },
	{ 0xfa801eb, __VMLINUX_SYMBOL_STR(blk_peek_request) },
	{ 0xd00630ea, __VMLINUX_SYMBOL_STR(seq_puts) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0xaa8b4be9, __VMLINUX_SYMBOL_STR(single_open) },
	{ 0x7932f22c, __VMLINUX_SYMBOL_STR(PDE_DATA) },
	{ 0xc6cbbc89, __VMLINUX_SYMBOL_STR(capable) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xd9605d4c, __VMLINUX_SYMBOL_STR(add_timer) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xf11543ff, __VMLINUX_SYMBOL_STR(find_first_zero_bit) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v00001011d00000046sv00000E11sd00004058bc*sc*i*");
MODULE_ALIAS("pci:v00001011d00000046sv00000E11sd00004051bc*sc*i*");
MODULE_ALIAS("pci:v00001011d00000046sv00000E11sd00004050bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000010sv00000E11sd00004048bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000010sv00000E11sd00004040bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d0000AE10sv00000E11sd00004034bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d0000AE10sv00000E11sd00004033bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d0000AE10sv00000E11sd00004032bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d0000AE10sv00000E11sd00004031bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d0000AE10sv00000E11sd00004030bc*sc*i*");

MODULE_INFO(srcversion, "3D7C70D7D2EB5954AFBBE35");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x4f68e5c9, __VMLINUX_SYMBOL_STR(do_gettimeofday) },
	{ 0x4b06d2e7, __VMLINUX_SYMBOL_STR(complete) },
	{ 0xc813450, __VMLINUX_SYMBOL_STR(add_disk) },
	{ 0xa1d355c7, __VMLINUX_SYMBOL_STR(blk_execute_rq_nowait) },
	{ 0x80e3b5d1, __VMLINUX_SYMBOL_STR(blk_get_request) },
	{ 0x8e71afe4, __VMLINUX_SYMBOL_STR(blk_fetch_request) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9166fada, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xd6b8e852, __VMLINUX_SYMBOL_STR(request_threaded_irq) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0x6d0aba34, __VMLINUX_SYMBOL_STR(wait_for_completion) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x2510af1c, __VMLINUX_SYMBOL_STR(blk_queue_segment_boundary) },
	{ 0x6a6feb36, __VMLINUX_SYMBOL_STR(blk_queue_max_segments) },
	{ 0xbf06b565, __VMLINUX_SYMBOL_STR(alloc_disk) },
	{ 0x71a50dbc, __VMLINUX_SYMBOL_STR(register_blkdev) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x62d06521, __VMLINUX_SYMBOL_STR(blk_init_queue) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0x42c8de35, __VMLINUX_SYMBOL_STR(ioremap_nocache) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x4a8cbbae, __VMLINUX_SYMBOL_STR(dma_set_mask) },
	{ 0x12bd6c9, __VMLINUX_SYMBOL_STR(pci_request_regions) },
	{ 0x9647a668, __VMLINUX_SYMBOL_STR(pci_enable_device) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xc03543e9, __VMLINUX_SYMBOL_STR(blk_requeue_request) },
	{ 0xbf899fdb, __VMLINUX_SYMBOL_STR(blk_start_queue) },
	{ 0x92c962b, __VMLINUX_SYMBOL_STR(__blk_end_request_all) },
	{ 0x3fec048f, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0x51e8a6e0, __VMLINUX_SYMBOL_STR(blk_rq_map_sg) },
	{ 0xe94b430c, __VMLINUX_SYMBOL_STR(blk_start_request) },
	{ 0xa2cd8a7a, __VMLINUX_SYMBOL_STR(blk_stop_queue) },
	{ 0xfa801eb, __VMLINUX_SYMBOL_STR(blk_peek_request) },
	{ 0xc897c382, __VMLINUX_SYMBOL_STR(sg_init_table) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0x59df21ad, __VMLINUX_SYMBOL_STR(pci_disable_device) },
	{ 0x33339182, __VMLINUX_SYMBOL_STR(pci_release_regions) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xedc03953, __VMLINUX_SYMBOL_STR(iounmap) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0xb5a459dc, __VMLINUX_SYMBOL_STR(unregister_blkdev) },
	{ 0xf20dabd8, __VMLINUX_SYMBOL_STR(free_irq) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x430a89d7, __VMLINUX_SYMBOL_STR(put_disk) },
	{ 0x2e133e3e, __VMLINUX_SYMBOL_STR(blk_cleanup_queue) },
	{ 0x777d1ee3, __VMLINUX_SYMBOL_STR(del_gendisk) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v0000105Ad00008000sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v0000105Ad00008002sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "18C2E39FBFD3292C0E93F24");

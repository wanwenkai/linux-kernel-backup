#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0xce7b64e6, __VMLINUX_SYMBOL_STR(parport_register_driver) },
	{ 0x96b60e74, __VMLINUX_SYMBOL_STR(i2c_handle_smbus_alert) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x188311b8, __VMLINUX_SYMBOL_STR(i2c_setup_smbus_alert) },
	{ 0xb9a4fe5d, __VMLINUX_SYMBOL_STR(i2c_bit_add_bus) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x6b2b8e2a, __VMLINUX_SYMBOL_STR(parport_claim_or_block) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x76f54795, __VMLINUX_SYMBOL_STR(parport_register_device) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xdf71c46d, __VMLINUX_SYMBOL_STR(parport_unregister_device) },
	{ 0xfc4e522b, __VMLINUX_SYMBOL_STR(parport_release) },
	{ 0xa1580468, __VMLINUX_SYMBOL_STR(i2c_del_adapter) },
	{ 0xf2aed3bf, __VMLINUX_SYMBOL_STR(i2c_unregister_device) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x4946b0f2, __VMLINUX_SYMBOL_STR(parport_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=parport,i2c-smbus,i2c-algo-bit,i2c-core";


MODULE_INFO(srcversion, "BF34ED89128AD547419A437");

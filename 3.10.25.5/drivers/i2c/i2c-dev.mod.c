#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x12a4e61c, __VMLINUX_SYMBOL_STR(no_llseek) },
	{ 0xb03da211, __VMLINUX_SYMBOL_STR(bus_register_notifier) },
	{ 0xc0acce4f, __VMLINUX_SYMBOL_STR(__class_create) },
	{ 0xa80b653d, __VMLINUX_SYMBOL_STR(__register_chrdev) },
	{ 0x9761ec2f, __VMLINUX_SYMBOL_STR(i2c_master_recv) },
	{ 0xeaac78ae, __VMLINUX_SYMBOL_STR(i2c_master_send) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0xb8e7ce2c, __VMLINUX_SYMBOL_STR(__put_user_8) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x786ad9a5, __VMLINUX_SYMBOL_STR(i2c_smbus_xfer) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x76965a8e, __VMLINUX_SYMBOL_STR(i2c_transfer) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x9291cd3b, __VMLINUX_SYMBOL_STR(memdup_user) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x40f91668, __VMLINUX_SYMBOL_STR(i2c_verify_client) },
	{ 0xf498dc8c, __VMLINUX_SYMBOL_STR(device_for_each_child) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x196cd84b, __VMLINUX_SYMBOL_STR(i2c_get_adapter) },
	{ 0xd7aa58db, __VMLINUX_SYMBOL_STR(i2c_put_adapter) },
	{ 0x843a078e, __VMLINUX_SYMBOL_STR(device_remove_file) },
	{ 0x6bc3fbc0, __VMLINUX_SYMBOL_STR(__unregister_chrdev) },
	{ 0xde9239f6, __VMLINUX_SYMBOL_STR(class_destroy) },
	{ 0x9cf76468, __VMLINUX_SYMBOL_STR(i2c_for_each_dev) },
	{ 0x8da25994, __VMLINUX_SYMBOL_STR(bus_unregister_notifier) },
	{ 0xcdecafdf, __VMLINUX_SYMBOL_STR(i2c_bus_type) },
	{ 0x44a9d788, __VMLINUX_SYMBOL_STR(device_destroy) },
	{ 0x7e08f3f1, __VMLINUX_SYMBOL_STR(device_create_file) },
	{ 0x7da04a25, __VMLINUX_SYMBOL_STR(device_create) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x948f1f00, __VMLINUX_SYMBOL_STR(i2c_adapter_type) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=i2c-core";


MODULE_INFO(srcversion, "E4407F81C76D5EF063331A9");

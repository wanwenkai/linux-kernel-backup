#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xa097c93c, __VMLINUX_SYMBOL_STR(drm_agp_release) },
	{ 0xfb4c2d23, __VMLINUX_SYMBOL_STR(drm_agp_acquire) },
	{ 0x3b01e6ea, __VMLINUX_SYMBOL_STR(drm_release) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x54930663, __VMLINUX_SYMBOL_STR(drm_compat_ioctl) },
	{ 0x4c4fef19, __VMLINUX_SYMBOL_STR(kernel_stack) },
	{ 0x765a81ae, __VMLINUX_SYMBOL_STR(drm_pci_exit) },
	{ 0xb5dcab5b, __VMLINUX_SYMBOL_STR(remove_wait_queue) },
	{ 0xfce4fd07, __VMLINUX_SYMBOL_STR(drm_mmap) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x5666bbfc, __VMLINUX_SYMBOL_STR(drm_core_ioremap) },
	{ 0xa7092745, __VMLINUX_SYMBOL_STR(drm_vblank_init) },
	{ 0x265b40f6, __VMLINUX_SYMBOL_STR(drm_addbufs_pci) },
	{ 0x6b43df53, __VMLINUX_SYMBOL_STR(platform_device_register_full) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xa49e38c, __VMLINUX_SYMBOL_STR(drm_agp_free) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0xffd5a395, __VMLINUX_SYMBOL_STR(default_wake_function) },
	{ 0xa85e3d28, __VMLINUX_SYMBOL_STR(drm_handle_vblank) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xacde1e28, __VMLINUX_SYMBOL_STR(drm_addmap) },
	{ 0xfcf65499, __VMLINUX_SYMBOL_STR(noop_llseek) },
	{ 0xa013ba3d, __VMLINUX_SYMBOL_STR(platform_device_unregister) },
	{ 0x9745d638, __VMLINUX_SYMBOL_STR(drm_err) },
	{ 0x5e8aa95b, __VMLINUX_SYMBOL_STR(drm_core_ioremapfree) },
	{ 0xdb818e70, __VMLINUX_SYMBOL_STR(drm_agp_bind) },
	{ 0xa765d721, __VMLINUX_SYMBOL_STR(pci_find_capability) },
	{ 0xaf72c522, __VMLINUX_SYMBOL_STR(drm_getsarea) },
	{ 0xcebb348f, __VMLINUX_SYMBOL_STR(drm_ioctl) },
	{ 0xadaa9846, __VMLINUX_SYMBOL_STR(drm_agp_unbind) },
	{ 0xd62c833f, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0x41f480f2, __VMLINUX_SYMBOL_STR(drm_irq_uninstall) },
	{ 0x2f264162, __VMLINUX_SYMBOL_STR(drm_agp_alloc) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x5860aad4, __VMLINUX_SYMBOL_STR(add_wait_queue) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xbffde8ec, __VMLINUX_SYMBOL_STR(compat_alloc_user_space) },
	{ 0x2e526f67, __VMLINUX_SYMBOL_STR(request_firmware) },
	{ 0x56f033ec, __VMLINUX_SYMBOL_STR(drm_agp_enable) },
	{ 0x94a951de, __VMLINUX_SYMBOL_STR(drm_pci_init) },
	{ 0x9851846d, __VMLINUX_SYMBOL_STR(drm_agp_info) },
	{ 0x498dcb5e, __VMLINUX_SYMBOL_STR(drm_ut_debug_printk) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0x75c3f17b, __VMLINUX_SYMBOL_STR(release_firmware) },
	{ 0x4450ffe3, __VMLINUX_SYMBOL_STR(drm_poll) },
	{ 0x54486059, __VMLINUX_SYMBOL_STR(drm_addbufs_agp) },
	{ 0xd1278c9, __VMLINUX_SYMBOL_STR(drm_fasync) },
	{ 0xd5de7b88, __VMLINUX_SYMBOL_STR(drm_open) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=drm";


MODULE_INFO(srcversion, "4AD79D499C51BF87EE9D17A");

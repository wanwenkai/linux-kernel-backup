cmd_lib/raid6/recov_avx2.o := gcc -Wp,-MD,lib/raid6/.recov_avx2.o.d  -nostdinc -isystem /usr/lib/gcc/x86_64-redhat-linux/4.4.7/include -I/home/wanwenkai/kernels/3.10.25.5/arch/x86/include -Iarch/x86/include/generated  -Iinclude -I/home/wanwenkai/kernels/3.10.25.5/arch/x86/include/uapi -Iarch/x86/include/generated/uapi -I/home/wanwenkai/kernels/3.10.25.5/include/uapi -Iinclude/generated/uapi -include /home/wanwenkai/kernels/3.10.25.5/include/linux/kconfig.h -D__KERNEL__ -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -Werror-implicit-function-declaration -Wno-format-security -fno-delete-null-pointer-checks -Os -m64 -mno-mmx -mno-sse -mtune=generic -mno-red-zone -mcmodel=kernel -funit-at-a-time -maccumulate-outgoing-args -fstack-protector -DCONFIG_AS_CFI=1 -DCONFIG_AS_CFI_SIGNAL_FRAME=1 -DCONFIG_AS_CFI_SECTIONS=1 -DCONFIG_AS_FXSAVEQ=1 -DCONFIG_AS_AVX=1 -pipe -Wno-sign-compare -fno-asynchronous-unwind-tables -mno-sse -mno-mmx -mno-sse2 -mno-3dnow -mno-avx -Wframe-larger-than=2048 -Wno-unused-but-set-variable -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fconserve-stack -DCC_HAVE_ASM_GOTO  -DMODULE  -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(recov_avx2)"  -D"KBUILD_MODNAME=KBUILD_STR(raid6_pq)" -c -o lib/raid6/.tmp_recov_avx2.o lib/raid6/recov_avx2.c

source_lib/raid6/recov_avx2.o := lib/raid6/recov_avx2.c

deps_lib/raid6/recov_avx2.o := \
    $(wildcard include/config/as/avx2.h) \
    $(wildcard include/config/x86/64.h) \

lib/raid6/recov_avx2.o: $(deps_lib/raid6/recov_avx2.o)

$(deps_lib/raid6/recov_avx2.o):

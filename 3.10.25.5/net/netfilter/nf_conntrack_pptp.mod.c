#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xf01193bd, __VMLINUX_SYMBOL_STR(nf_ct_unexpect_related) },
	{ 0xb11ec43e, __VMLINUX_SYMBOL_STR(nf_conntrack_helper_register) },
	{ 0xcca27eeb, __VMLINUX_SYMBOL_STR(del_timer) },
	{ 0xa161a975, __VMLINUX_SYMBOL_STR(nf_ct_gre_keymap_add) },
	{ 0x16230b99, __VMLINUX_SYMBOL_STR(nf_conntrack_helper_unregister) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0xe3125c5a, __VMLINUX_SYMBOL_STR(nf_ct_expect_init) },
	{ 0x2bf56ebc, __VMLINUX_SYMBOL_STR(nf_ct_gre_keymap_flush) },
	{ 0xaef6cd12, __VMLINUX_SYMBOL_STR(unregister_pernet_subsys) },
	{ 0xf8df7e56, __VMLINUX_SYMBOL_STR(nf_ct_expect_find_get) },
	{ 0xbac4d71b, __VMLINUX_SYMBOL_STR(nf_ct_expect_put) },
	{ 0xca61afe, __VMLINUX_SYMBOL_STR(nf_ct_expect_alloc) },
	{ 0x19a9c05b, __VMLINUX_SYMBOL_STR(nf_ct_expect_related_report) },
	{ 0x5341f6d0, __VMLINUX_SYMBOL_STR(nf_ct_gre_keymap_destroy) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xad4fe501, __VMLINUX_SYMBOL_STR(register_pernet_subsys) },
	{ 0x2a18c74, __VMLINUX_SYMBOL_STR(nf_conntrack_destroy) },
	{ 0x418132dc, __VMLINUX_SYMBOL_STR(skb_copy_bits) },
	{ 0xcaa5172f, __VMLINUX_SYMBOL_STR(nf_conntrack_find_get) },
	{ 0x90ff6c9f, __VMLINUX_SYMBOL_STR(nf_ct_invert_tuplepr) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=nf_conntrack,nf_conntrack_proto_gre";


MODULE_INFO(srcversion, "F28373639441072B3F83205");

#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x13aa0ad8, __VMLINUX_SYMBOL_STR(ipcomp_output) },
	{ 0x794b0499, __VMLINUX_SYMBOL_STR(ipcomp_input) },
	{ 0xef7fb819, __VMLINUX_SYMBOL_STR(ipcomp_destroy) },
	{ 0x7527d6cb, __VMLINUX_SYMBOL_STR(xfrm4_rcv) },
	{ 0x2cfd14ef, __VMLINUX_SYMBOL_STR(inet_add_protocol) },
	{ 0xd7c99d0c, __VMLINUX_SYMBOL_STR(xfrm_register_type) },
	{ 0x9dc9ac81, __VMLINUX_SYMBOL_STR(ipv4_redirect) },
	{ 0xdc234f7f, __VMLINUX_SYMBOL_STR(ipv4_update_pmtu) },
	{ 0xe5122890, __VMLINUX_SYMBOL_STR(flow_cache_genid) },
	{ 0x3b0e3032, __VMLINUX_SYMBOL_STR(__xfrm_state_destroy) },
	{ 0xe19a7788, __VMLINUX_SYMBOL_STR(xfrm_state_insert) },
	{ 0xff0aeba4, __VMLINUX_SYMBOL_STR(xfrm_init_state) },
	{ 0x174a9e27, __VMLINUX_SYMBOL_STR(xfrm_state_alloc) },
	{ 0xd28d488, __VMLINUX_SYMBOL_STR(xfrm_state_lookup) },
	{ 0x3dfd709c, __VMLINUX_SYMBOL_STR(ipcomp_init_state) },
	{ 0xcca6a892, __VMLINUX_SYMBOL_STR(xfrm_unregister_type) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x46889ef9, __VMLINUX_SYMBOL_STR(inet_del_protocol) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=xfrm_ipcomp";


MODULE_INFO(srcversion, "DA31DEB8C7F62D337430D71");

/*
 * tcpflowinfo - Collect the information of the TCP flows with kprobes.
 *
 * Copyright (C) 2014, Perthcharles <zhongbincharles@gmail.com>
 */


#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/kprobes.h>
#include <linux/socket.h>
#include <linux/tcp.h>
#include <slab.h>
#include <linux/proc_fs.h>
#include <linux/module.h>
#include <linux/ktime.h>
#include <linux/time.h>
#include <net/net_namespace.h>

#include <net/tcp.h>
#include <net/sock.h>
#include <net/tcp_states.h>

#include "tcp_flowmsg.h"

MODULE_AUTHOR("Perth Charles <zhongbincharles@gmail.com>");
MODULE_DESCRIPTION("TCP established information collector");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.2");

/* only control some specific flows, e.g., HTTP with port=8080 */
static int port __read_mostly = 0;
MODULE_PARM_DESC(port, "Port to match (0=all)");
module_param(port, int, 0);

static unsigned int bufsize __read_mostly = 4096;
MODULE_PARM_DESC(bufsize, "Log buffer size in flows");
module_param(bufsize, uint, 0);

static const char procname[] = "tcpflowinfo";

static struct ccph_buf tcp_flow;

/*
 * Hook inserted to be called before each established TCP flow.
 */
static int jtcp_set_state(struct sock *sk, int state)
{
    const struct inet_sock *inet = inet_sk(sk);

    if ((state == TCP_ESTABLISHED || state == TCP_CLOSE) &&
        (port == 0 || ntohs(inet->inet_dport) == port ||
         ntohs(inet->inet_sport) == port)) {
        spin_lock(&tcp_flow.lock);
        
        /* If buf fills, just silently drop */
        if (ccph_buf_avail(&tcp_flow, bufsize) > 1) {
            struct flow_msg *cur = tcp_flow.buf + tcp_flow.head;

            cur->tstamp    = ktime_get();
            cur->sock_net  = (u64)sock_net(sk);
            cur->dev_index = (u64)sk->sk_bound_dev_if;
            cur->addr_pair = (u64)inet->inet_addrpair;
            cur->port_pair = (u32)inet->inet_portpair;
            cur->flag      = (state == TCP_ESTABLISHED) ? FLOW_ADD : FLOW_DEL;
            cur->value     = 0; // unused, so far

            tcp_flow.head = (tcp_flow.head + 1) & (bufsize - 1);
        }

        spin_unlock(&tcp_flow.lock);
        wake_up(&tcp_flow.wait);
    } 

    jprobe_return();
    return 0;
}

static struct jprobe jtcp_flow = {
    .kp = {
        .symbol_name = "tcp_set_state",
    },
    .entry = jtcp_set_state,
};

static int tcpflow_open(struct inode *inode, struct file *file)
{
    ccph_buf_init(&tcp_flow);

    return 0;
}

/* TODO: change to binary read after a few tests */
static ssize_t tcpflow_read(struct file *file, char __user *buf,
                            size_t len, loff_t *ppos)
{
    int error = 0;
    size_t cnt = 0;

    if (!buf) {
        return -EINVAL;
    }

    while (cnt < len) {
        char tbuf[256];
        int width;
        
        error = wait_event_interruptible(tcp_flow.wait, ccph_buf_used(&tcp_flow, bufsize) > 0);
        if (error) {
            break;
        }
        
        spin_lock_bh(&tcp_flow.lock);
        if (tcp_flow.head == tcp_flow.tail) {
            /* multiple readers race ? */
            spin_unlock_bh(&tcp_flow.lock);
            continue;
        }
        
        width = flowmsg_readone(tbuf, sizeof(tbuf));
        if (cnt + width < len) {
            tcp_flow.tail = (tcp_flow.tail + 1) & (bufsize - 1);
        }
        spin_unlock_bh(&tcp_flow.lock);

        if (cnt + width >= len) {
            break;
        }

        if (copy_to_user(buf + cnt, tbuf, width)) {
            return -EFAULT;
        }
        cnt += width;
    }

    return cnt == 0 ? error : cnt;
}

static const struct file_operations tcpflow_fops = {
    .owner   = THIS_MODULE,
    .open    = tcpflow_open,
    .read    = tcpflow_read,
    .llseek  = noop_llseek,
};

static __init int tcpflow_init(void)
{
    int ret = -ENOMEM;

    init_waitqueue_head(&tcp_flow.wait);
    spin_lock_init(&tcp_flow.lock);

    if (bufsize == 0) {
         return -EINVAL;
    }

    bufsize = roundup_pow_of_two(bufsize);
    tcp_flow.buf = kcalloc(bufsize, sizeof(struct flow_msg), GFP_KERNEL);
    if (!tcp_flow.buf) {
        goto err0;
    }

    if (!proc_create(procname, S_IRUSR, init_net.proc_net, &tcpflow_fops)) {
        goto err0;
    }

    ret = register_jprobe(&jtcp_flow);
    if (ret) {
        goto err1;
    }

    pr_info("tcp_flow_info registered (port=%d) bufsize=%u\n", port, bufsize);
    return 0;

err1:
    remove_proc_entry(procname, init_net.proc_net);

err0:
    kfree(tcp_flow.buf);
    return ret;
}
module_init(tcpflow_init);

static __exit void tcpflow_exit(void)
{
    remove_proc_entry(procname, init_net.proc_net);
    unregister_jprobe(&jtcp_flow);
    kfree(tcp_flow.buf);
    pr_info("tcp_flow_info unregistered\n");
}
module_exit(tcpflow_exit);

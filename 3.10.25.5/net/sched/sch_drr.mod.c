#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x1f600fe8, __VMLINUX_SYMBOL_STR(register_qdisc) },
	{ 0x894e74ac, __VMLINUX_SYMBOL_STR(__qdisc_calculate_pkt_len) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x75674642, __VMLINUX_SYMBOL_STR(tc_classify) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0x117093be, __VMLINUX_SYMBOL_STR(qdisc_class_hash_init) },
	{ 0xf53d4c26, __VMLINUX_SYMBOL_STR(qdisc_class_hash_destroy) },
	{ 0xb9d66706, __VMLINUX_SYMBOL_STR(tcf_destroy_chain) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xac6855b0, __VMLINUX_SYMBOL_STR(gen_kill_estimator) },
	{ 0xbac17824, __VMLINUX_SYMBOL_STR(qdisc_class_hash_grow) },
	{ 0xa04a01bd, __VMLINUX_SYMBOL_STR(qdisc_class_hash_insert) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x5e6548b0, __VMLINUX_SYMBOL_STR(qdisc_destroy) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x42b0325e, __VMLINUX_SYMBOL_STR(gen_replace_estimator) },
	{ 0x4f391d0e, __VMLINUX_SYMBOL_STR(nla_parse) },
	{ 0x931f32b9, __VMLINUX_SYMBOL_STR(noop_qdisc) },
	{ 0x45433441, __VMLINUX_SYMBOL_STR(qdisc_create_dflt) },
	{ 0x6926d68a, __VMLINUX_SYMBOL_STR(pfifo_qdisc_ops) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0x6bdcfd99, __VMLINUX_SYMBOL_STR(qdisc_class_hash_remove) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0x6b2dc060, __VMLINUX_SYMBOL_STR(dump_stack) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x85670f1d, __VMLINUX_SYMBOL_STR(rtnl_is_locked) },
	{ 0xd4329d39, __VMLINUX_SYMBOL_STR(qdisc_tree_decrease_qlen) },
	{ 0xf8b29057, __VMLINUX_SYMBOL_STR(qdisc_reset) },
	{ 0xf00f8562, __VMLINUX_SYMBOL_STR(skb_trim) },
	{ 0xfa0f6d09, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0xec96529d, __VMLINUX_SYMBOL_STR(gnet_stats_copy_app) },
	{ 0x6fbd4d88, __VMLINUX_SYMBOL_STR(gnet_stats_copy_queue) },
	{ 0x1b0be076, __VMLINUX_SYMBOL_STR(gnet_stats_copy_rate_est) },
	{ 0xe1493adc, __VMLINUX_SYMBOL_STR(gnet_stats_copy_basic) },
	{ 0xb7b4fbc7, __VMLINUX_SYMBOL_STR(unregister_qdisc) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "33346C7C1D603E032812DE7");

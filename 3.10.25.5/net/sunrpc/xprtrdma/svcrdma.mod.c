#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xd0298b8b, __VMLINUX_SYMBOL_STR(proc_dointvec_minmax) },
	{ 0xfb994d5a, __VMLINUX_SYMBOL_STR(alloc_pages_current) },
	{ 0xb294dd6, __VMLINUX_SYMBOL_STR(kmem_cache_destroy) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0x628121e9, __VMLINUX_SYMBOL_STR(register_sysctl_table) },
	{ 0xd0ee38b8, __VMLINUX_SYMBOL_STR(schedule_timeout_uninterruptible) },
	{ 0x3bfe8d2c, __VMLINUX_SYMBOL_STR(ib_dealloc_pd) },
	{ 0x220471d2, __VMLINUX_SYMBOL_STR(ib_alloc_fast_reg_mr) },
	{ 0x43a53735, __VMLINUX_SYMBOL_STR(__alloc_workqueue_key) },
	{ 0xc8b57c27, __VMLINUX_SYMBOL_STR(autoremove_wake_function) },
	{ 0x4a7ababe, __VMLINUX_SYMBOL_STR(svc_xprt_enqueue) },
	{ 0x24f8b031, __VMLINUX_SYMBOL_STR(svc_reg_xprt_class) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0xa72c5e19, __VMLINUX_SYMBOL_STR(ib_destroy_qp) },
	{ 0xbf70163c, __VMLINUX_SYMBOL_STR(rdma_accept) },
	{ 0x28cab4b4, __VMLINUX_SYMBOL_STR(ib_free_fast_reg_page_list) },
	{ 0x6ddbd44, __VMLINUX_SYMBOL_STR(rdma_destroy_id) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x7ffa5ca, __VMLINUX_SYMBOL_STR(svc_unreg_xprt_class) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x9c59f41c, __VMLINUX_SYMBOL_STR(svc_xprt_init) },
	{ 0x4a2fc29b, __VMLINUX_SYMBOL_STR(ib_alloc_pd) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0xa3db70d, __VMLINUX_SYMBOL_STR(ib_get_dma_mr) },
	{ 0x25db2868, __VMLINUX_SYMBOL_STR(ib_query_device) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xaad4cf69, __VMLINUX_SYMBOL_STR(rdma_listen) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0xfaef0ed, __VMLINUX_SYMBOL_STR(__tasklet_schedule) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xc9409b91, __VMLINUX_SYMBOL_STR(kmem_cache_free) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x8c03d20c, __VMLINUX_SYMBOL_STR(destroy_workqueue) },
	{ 0x1acbf1c7, __VMLINUX_SYMBOL_STR(svc_xprt_copy_addrs) },
	{ 0x88dc702c, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0x67eca25a, __VMLINUX_SYMBOL_STR(rdma_create_id) },
	{ 0xfbc89c16, __VMLINUX_SYMBOL_STR(ib_destroy_cq) },
	{ 0xb971585e, __VMLINUX_SYMBOL_STR(rdma_create_qp) },
	{ 0xa68500bb, __VMLINUX_SYMBOL_STR(svc_xprt_put) },
	{ 0x761558ed, __VMLINUX_SYMBOL_STR(rdma_bind_addr) },
	{ 0x51bc3a46, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xac3d20e2, __VMLINUX_SYMBOL_STR(unregister_sysctl_table) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x578ccefb, __VMLINUX_SYMBOL_STR(rdma_disconnect) },
	{ 0x96ce6c46, __VMLINUX_SYMBOL_STR(rdma_node_get_transport) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x67cc86e8, __VMLINUX_SYMBOL_STR(kmem_cache_create) },
	{ 0xad5377f9, __VMLINUX_SYMBOL_STR(ib_dereg_mr) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x5c8b5ce8, __VMLINUX_SYMBOL_STR(prepare_to_wait) },
	{ 0x2e710893, __VMLINUX_SYMBOL_STR(put_page) },
	{ 0xf87b07, __VMLINUX_SYMBOL_STR(ib_alloc_fast_reg_page_list) },
	{ 0xfa66f77c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x87dcd430, __VMLINUX_SYMBOL_STR(ib_create_cq) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=ib_core,sunrpc,rdma_cm";


MODULE_INFO(srcversion, "5B763C1E8472E2FC6019F41");
